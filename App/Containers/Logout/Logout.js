import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  SafeAreaView
} from "react-native";
import { Header, Icon } from "react-native-elements";
import { Auth } from "aws-amplify";

class Logout extends Component {
  static navigationOptions = ({ navigation }) => ({
    drawerLabel: "Logout",
    drawerIcon: ({ tintColor }) => (
      <Icon
        name="user"
        color={tintColor}
        type="font-awesome"
        style={{ fontSize: 24, opacity: 1 }}
      />
    )
  });

  componentDidMount() {
    Auth.signOut().then(this.props.navigation.navigate("MainScreen"))
  }
  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <View style={styles.container}>
          <View
            style={{
              height: "5%",
              justifyContent: "space-between",
              flexDirection: "row",
              alignItems: "center",
              padding: 10
            }}
          >
            <TouchableOpacity
              onPress={() => this.props.navigation.openDrawer()}
            >
              <Icon name="menu" color="#FCB61A" size={30} />
            </TouchableOpacity>
            <Text
              style={{
                fontFamily: "Signika-Regular",
                fontSize: 16,
                lineHeight: 20,
                textAlign: "center"
              }}
            >
              Logout
            </Text>
            <View style={{ flexDirection: "row" }}>
              <TouchableOpacity>
                <Icon name="search" color="#FCB61A" size={20} />
              </TouchableOpacity>
            </View>
          </View>
          <View
            style={{
              flex: 1,
              alignItems: "center",
              justifyContent: "center",
              color: "black"
            }}
          >
            <Text>Logout</Text>
          </View>
        </View>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});
export default Logout;
