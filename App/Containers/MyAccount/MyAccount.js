import React from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableHighlight,
  TouchableOpacity,
  Alert,
  TextInput,
  TouchableWithoutFeedback,
  Image,
  ActivityIndicator,
  ScrollView,
  SafeAreaView,
  FlatList
} from "react-native";
import { Auth } from "aws-amplify";
import { Icon, Avatar, Slider, Card, Button } from "react-native-elements";
import { NavigationEvents } from "react-navigation";
import ImagePicker from "react-native-image-picker";
import axios from "axios";

class MyAccount extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showProgress: false,
      showEducation: false,
      showAddEducation: false,
      degree: "",
      university: "",
      classOrGrade: "",
      passingYear: "",
      users: {},
      error: null,
      certificate: null,
      totalScore: null,
      educationLoading: false,
      educationEditIndex: null,
      imageSource: null,
      showEditEducation: false,
      currentEditEducation: null,
      coursesProgress: null
    };
  }

  static navigationOptions = ({ navigation }) => ({
    drawerLabel: "My Account",
    drawerIcon: ({ tintColor }) => (
      <Icon
        name="user"
        color={tintColor}
        type="font-awesome"
        style={{ fontSize: 24, opacity: 1 }}
      />
    )
  });

  componentDidMount() {
    this.getEducations();
    this.getProgress();
  }

  getProgress = () => {
    Auth.currentAuthenticatedUser().then(dt => {
      let token = dt
        .getSignInUserSession()
        .getAccessToken()
        .getJwtToken();

      fetch(
        `http://newbee-prod-1105238445.ap-south-1.elb.amazonaws.com/api/user-api/user/search/findByEmail?email=${
          dt.attributes.email
        }`,
        {
          method: "GET",
          headers: {
            Accept: "application/json",
            Authorization: `Bearer ${token}`,
            "Content-Type": "application/json"
          }
        }
      )
        .then(profile => profile.json())
        .then(data1 => {
          let uid = data1._embedded.users[0].id;
          axios
            .get(
              `http://newbee-prod-1105238445.ap-south-1.elb.amazonaws.com/api/course-api/progress/search/findByUserId?userId=${uid}`,
              {
                headers: {
                  Authorization: `Bearer ${token}`
                }
              }
            )
            .then(resp123 => {
              this.setState({
                coursesProgress: resp123.data._embedded.coursesProgress,
                loading: false,
                name: data1._embedded.users[0].firstName
              });
            })
            .catch(e => console.log(e));
        });
    });
  };

  isValid = () => {
    if (
      !this.state.degree ||
      !this.state.university ||
      !this.state.classOrGrade ||
      !this.state.passingYear
    ) {
      this.setState({ error: "You must provide all fields" });
      return false;
    }
    this.setState({ error: "", showAddEducation: false });
    return true;
  };
  getEducations = () => {
    this.setState({ educationLoading: true });
    Auth.currentAuthenticatedUser().then(dt => {
      let token = dt
        .getSignInUserSession()
        .getAccessToken()
        .getJwtToken();
      fetch(
        `http://newbee-prod-1105238445.ap-south-1.elb.amazonaws.com/api/user-api/user/search/findByEmail?email=${
          dt.attributes.email
        }`,
        {
          method: "GET",
          headers: {
            Accept: "application/json",
            Authorization: `Bearer ${token}`,
            "Content-Type": "application/json"
          }
        }
      )
        .then(response => response.json())
        .then(data => {
          axios
            .get(`http://newbee-prod-1105238445.ap-south-1.elb.amazonaws.com/api/course-api/certificate/all/${data._embedded.users[0].id}`, {
              headers: {
                Authorization: `Bearer ${token}`,
              },
            })
            .then((resp123) => {
              this.setState({
                certificate: resp123
                
            }, () => {
              fetch(
                `http://newbee-prod-1105238445.ap-south-1.elb.amazonaws.com/api/course-api/quiz/score/final/${
                  data._embedded.users[0].id
                }`,
                {
                  method: "GET",
                  headers: {
                    Accept: "application/json",
                    Authorization: `Bearer ${token}`,
                    "Content-Type": "application/json"
                  }
                }
              )
                .then(response1 => response1.json())
                .then(data2 => {
                  this.setState({ users: data._embedded.users, totalScore: data2 });
                  this.setState({ educationLoading: false });
                });
            })
            })
        })
        .catch(e => console.log(e));
    });
  };

  addEducation = param => {
    if (!this.isValid()) {
      return;
    }

    let educations = [];

    let newArray = [];
    newArray.push({
      degree: this.state.degree,
      university: this.state.university,
      classOrGrade: this.state.classOrGrade,
      passingYear: this.state.passingYear
    });

    if (this.state.users[0].educations !== null) {
      educations = this.state.users[0].educations;
    }

    if (param != null) {
      educations[param] = newArray[0];
    } else {
      educations.push({
        degree: this.state.degree,
        university: this.state.university,
        classOrGrade: this.state.classOrGrade,
        passingYear: this.state.passingYear
      });
    }

    Auth.currentAuthenticatedUser().then(dt => {
      let token = dt
        .getSignInUserSession()
        .getAccessToken()
        .getJwtToken();
      fetch(
        `http://newbee-prod-1105238445.ap-south-1.elb.amazonaws.com/api/user-api/user/${
          this.state.users[0].id
        }`,
        {
          method: "PUT",
          headers: {
            Accept: "application/json",
            Authorization: `Bearer ${token}`,
            "Content-Type": "application/json"
          },
          body: JSON.stringify({
            profileImage: this.state.users[0].profileImage,
            firstName: this.state.users[0].firstName,
            lastName: this.state.users[0].lastName,
            email: dt.attributes.email,
            id: this.state.users[0].id,
            role: "Customer",
            educations: educations
          })
        }
      )
        .then(response => response.json())
        .then(data => {
          this.setState(
            {
              degree: "",
              university: "",
              classOrGrade: "",
              passingYear: "",
              educationEditIndex: null
            },
            () => {
              this.getEducations();
            }
          );
        })
        .catch(e => this.setState({ error: "Something went wrong" }));
    });
  };
  toggleProgress = () =>
    this.setState(prevState => {
      return {
        ...prevState,
        showProgress: !prevState.showProgress
      };
    });
  toggleEducation = () =>
    this.setState(prevState => {
      return {
        ...prevState,
        showEducation: !prevState.showEducation
      };
    });

  renderAddEducationCard = () => {
    return (
      <Card
        containerStyle={{
          borderRadius: 10,
          elevation: 6,
          shadowColor: "black",
          shadowOpacity: 0.26,
          shadowOffset: { width: 0, height: 2 },
          shadowRadius: 3,
          overflow: "hidden",
          marginBottom: "2%",
          height: "80%",
          top: "10%",
          position: "absolute",
          alignSelf: "center",
          width: "90%"
        }}
      >
        <ScrollView
          contentContainerStyle={{ flexGrow: 1 }}
          showsVerticalScrollIndicator={false}
        >
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-between",
              alignItems: "center"
            }}
          >
            <Text
              style={{
                marginTop: "3%",
                fontSize: 16,
                lineHeight: 20,
                fontFamily: "Signika-Regular",
                textAlign: "center"
              }}
            >
              Add more education
            </Text>
            <TouchableHighlight
              underlayColor="white"
              onPress={() => this.setState({ showAddEducation: false })}
            >
              <Icon name="close" style={{ alignSelf: "flex-end" }} />
            </TouchableHighlight>
          </View>
          <View
            style={{
              width: "100%",
              height: "100%",
              marginTop: 30
            }}
          >
            <Text style={{ fontSize: 14,  color: "#555555" }}>
              Degree / Standard
            </Text>
            <TextInput
              value={this.state.degree}
              onChangeText={value => this.setState({ degree: value })}
              style={{
                marginTop: 10,
                backgroundColor: "white",
                borderWidth: 1,
                borderRadius: 5,
                borderColor: "rgba(0, 0, 0, 0.13)",
                fontSize: 14,
                // lineHeight: 30,
                height: 40,
                fontFamily: "Signika-Regular"
              }}
              placeholder="B.E"
            />
            <Text
              style={{
                fontSize: 14,
                marginTop: 10,
                color: "#555555"
              }}
            >
              University
            </Text>
            <TextInput
              value={this.state.university}
              onChangeText={value => this.setState({ university: value })}
              style={{
                marginTop: 10,
                backgroundColor: "white",
                borderWidth: 1,
                borderRadius: 5,
                borderColor: "rgba(0, 0, 0, 0.13)",
                fontSize: 14,
                height: 40,
                fontFamily: "Signika-Regular"
              }}
              placeholder="Gujrat Tech. University"
            />

            <Text
              style={{
                fontSize: 14,
                marginTop: 10,
                color: "#555555"
              }}
            >
              Class / Percentage / CGPA / Grade
            </Text>
            <TextInput
              value={this.state.classOrGrade}
              onChangeText={value => this.setState({ classOrGrade: value })}
              style={{
                marginTop: 10,
                backgroundColor: "white",
                borderWidth: 1,
                borderRadius: 5,
                borderColor: "rgba(0, 0, 0, 0.13)",
                fontSize: 14,
                height: 40,
                fontFamily: "Signika-Regular"
              }}
              placeholder="First"
            />
            <Text
              style={{
                fontSize: 14,
                marginTop: 10,
                color: "#555555"
              }}
            >
              Passing Year
            </Text>
            <TextInput
              value={this.state.passingYear}
              onChangeText={value => this.setState({ passingYear: value })}
              style={{
                marginTop: 10,
                backgroundColor: "white",
                borderWidth: 1,
                borderRadius: 5,
                borderColor: "rgba(0, 0, 0, 0.13)",
                fontSize: 14,
                height: 40,
                fontFamily: "Signika-Regular"
              }}
              placeholder="1996"
            />
            <Text
              style={{
                marginTop: 5,
                fontSize: 14,
                color: "red",
                textAlign: "center",
                fontFamily: "Signika-Regular"
              }}
            >
              {this.state.error}
            </Text>
            <Button
              containerStyle={{
                alignSelf: "center",
                width: "100%",
                marginTop: "5%",
                elevation: 6,
                shadowColor: "black",
                shadowOpacity: 0.26,
                shadowOffset: { width: 0, height: 2 },
                shadowRadius: 3,
                overflow: "hidden"
              }}
              buttonStyle={{
                width: "100%",

                backgroundColor: "black"
              }}
              titleStyle={{
                fontFamily: "Signika-Regular",
                color: "#FCB61A",
                fontSize: 20
              }}
              title={
                this.state.educationEditIndex !== null
                  ? "Update"
                  : "Save & Submit"
              }
              onPress={() =>
                this.addEducation(
                  this.state.educationEditIndex !== null
                    ? this.state.educationEditIndex
                    : null
                )
              }
            />
          </View>
        </ScrollView>
      </Card>
    );
  };

  renderEducation = () => (
    <View style={{ paddingHorizontal: 10, marginTop: 10 }}>
      {Array.isArray(this.state.users) &&
        this.state.users.length > 0 &&
        this.state.users[0].educations !== null &&
        this.state.users[0].educations.map((education, index) => (
          <View>
            <TouchableWithoutFeedback
              onPress={() => {
                this.setState(
                  {
                    degree: education.degree,
                    university: education.university,
                    classOrGrade: education.classOrGrade,
                    passingYear: education.passingYear.toString(),
                    educationEditIndex: index
                  },
                  () => {
                    this.setState({
                      showAddEducation: true
                    });
                  }
                );
              }}
            >
              <Card
                containerStyle={{
                  height: 70,
                  flex: 1,
                  margin: 0,
                  marginBottom: 10,
                  shadowColor: "#000",
                  shadowOffset: {
                    width: 0,
                    height: 3
                  },
                  shadowOpacity: 0.27,
                  shadowRadius: 4.65,
                  elevation: 6,
                  width: "95%",
                  borderRadius: 10,
                  borderWidth: 2,
                  borderColor: "#FCB61A",
                  borderTopColor: "transparent",
                  borderBottomColor: "transparent"
                }}
              >
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-between"
                  }}
                >
                  <View
                    style={{
                      flex: 2,
                      flexDirection: "row"
                    }}
                  >
                    <Text
                      style={{
                        fontSize: 10,
                        fontFamily: "Signika-Regular",
                        lineHeight: 11,
                        textAlign: "center"
                      }}
                    >
                      Degree
                    </Text>
                    <Text
                      style={{
                        fontSize: 10,
                        fontFamily: "Signika-Regular",
                        lineHeight: 11,
                        textAlign: "center",
                        marginLeft: 20
                      }}
                    >
                      University
                    </Text>
                  </View>
                  <View
                    style={{
                      flex: 1,
                      flexDirection: "row",
                      justifyContent: "space-between",
                      marginLeft: 5
                    }}
                  >
                    <Text
                      style={{
                        fontSize: 10,
                        fontFamily: "Signika-Regular",
                        lineHeight: 11,
                        textAlign: "center"
                      }}
                    >
                      Class
                    </Text>
                    <Text
                      style={{
                        fontSize: 10,
                        fontFamily: "Signika-Regular",
                        lineHeight: 11,
                        textAlign: "center"
                      }}
                    >
                      Passing year
                    </Text>
                  </View>
                </View>
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-between",
                    marginTop: 10
                  }}
                >
                  <View
                    style={{
                      flex: 2,
                      flexDirection: "row"
                    }}
                  >
                    <Text
                      style={{
                        fontSize: 14,
                        fontFamily: "Signika-Regular",
                        lineHeight: 17,
                        textAlign: "center"
                      }}
                    >
                      {education.degree}
                    </Text>
                    <Text
                      style={{
                        fontSize: 14,
                        fontFamily: "Signika-Regular",
                        lineHeight: 17,
                        textAlign: "center",
                        marginLeft: 20
                      }}
                    >
                      {education.university}
                    </Text>
                  </View>
                  <View
                    style={{
                      flex: 1,
                      flexDirection: "row",
                      justifyContent: "space-between",
                      marginLeft: 5
                    }}
                  >
                    <Text
                      style={{
                        fontSize: 14,
                        fontFamily: "Signika-Regular",
                        lineHeight: 17,
                        textAlign: "center"
                      }}
                    >
                      {education.classOrGrade}
                    </Text>
                    <Text
                      style={{
                        fontSize: 15,
                        fontFamily: "Signika-Regular",
                        lineHeight: 17,
                        textAlign: "center"
                      }}
                    >
                      {education.passingYear}
                    </Text>
                  </View>
                </View>
              </Card>
            </TouchableWithoutFeedback>
            <TouchableWithoutFeedback
              onPress={() => {
                Alert.alert(
                  "Delete Education Profile",
                  "Are you sure you want to delete this education profile ?",
                  [
                    {
                      text: "No",
                      onPress: () => {},
                      style: "cancel"
                    },
                    { text: "Yes", onPress: () => this.removeEducation(index) }
                  ],
                  { cancelable: false }
                );
              }}
            >
              <Image
                source={require("App/Assets/Images/del.png")}
                style={{
                  width: 20,
                  height: 16,
                  left: "96%",
                  position: "absolute",
                  top: "30%",

                  zIndex: 5
                }}
                resizeMode="contain"
              />
            </TouchableWithoutFeedback>
          </View>
        ))}
    </View>
  );

  removeEducation = index => {
    this.setState({
      educationLoading: true
    });
    let newEducation = this.state.users[0].educations.filter(
      (item, index1) => index1 !== index
    );

    Auth.currentAuthenticatedUser().then(dt => {
      let token = dt
        .getSignInUserSession()
        .getAccessToken()
        .getJwtToken();
      fetch(
        `http://newbee-prod-1105238445.ap-south-1.elb.amazonaws.com/api/user-api/user/${
          this.state.users[0].id
        }`,
        {
          method: "PUT",
          headers: {
            Accept: "application/json",
            Authorization: `Bearer ${token}`,
            "Content-Type": "application/json"
          },
          body: JSON.stringify({
            profileImage: this.state.users[0].profileImage,
            firstName: this.state.users[0].firstName,
            lastName: this.state.users[0].lastName,
            email: dt.attributes.email,
            id: this.state.users[0].id,
            role: "Customer",
            educations: newEducation
          })
        }
      )
        .then(response => response.json())
        .then(data => {
          this.setState({
            educationLoading: false
          });
          this.getEducations();
        })
        .catch(e => this.setState({ error: "Something went wrong" }));
    });
  };

  renderProgress = () => {
    return (
      <View style={{ flex: 1, paddingHorizontal: 20, marginTop: 10 }}>
        <Text style={{ fontSize: 16, fontFamily: "Signika-Regular" }}>
          Course progress
        </Text>
        <Text
          style={{
            color: "#999999",
            fontSize: 14,
            lineHeight: 17,
            fontFamily: "Signika-Regular",
            padding: 20
          }}
        >
          Courses
        </Text>
        <FlatList
          data={this.state.coursesProgress}
          horizontal
          showsHorizontalScrollIndicator={false}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item, index }) => {
            return (
              <View
                style={{
                  justifyContent: "center",
                  marginVertical: 10,
                  marginHorizontal: 20,
                  marginBottom: 20,
                  alignItems: "center"
                }}
              >
                <Slider
                  thumbTintColor="#999999"
                  thumbStyle={{
                    height: 5
                  }}
                  maximumTrackTintColor={
                    index % 2 === 0 ? "#FFCF26" : "#8FCAD2"
                  }
                  minimumTrackTintColor={
                    index % 2 === 0 ? "#FFCF26" : "#8FCAD2"
                  }
                  style={{
                    height: 70,
                    marginBottom: 10,
                    transform: [{ rotate: "180deg" }]
                  }}
                  disabled
                  orientation="vertical"
                  value={item.progessPercent / 100}
                />
                <View>
                  <Text
                    style={{
                      textAlign: "center",
                      fontSize: 14,
                      lineHeight: 17,
                      fontFamily: "Signika-Regular",
                      fontWeight: "900"
                    }}
                  >
                    {item.courseDescription}
                  </Text>
                  <Text
                    style={{
                      marginTop: 5,
                      textAlign: "center",
                      fontSize: 14,
                      lineHeight: 17,
                      fontFamily: "Signika-Regular",
                      fontWeight: "900"
                    }}
                  >
                    {item.courseLevel}
                  </Text>
                </View>
              </View>
            );
          }}
        />

        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between"
          }}
        >
          <Card
            containerStyle={{
              height: 91,
              width: 155,
              padding: 0,
              margin: 0,
              borderRadius: 10,
              shadowColor: "#000",
              shadowOffset: {
                width: 0,
                height: 2
              },
              shadowOpacity: 0.23,
              shadowRadius: 2.62,
              elevation: 4,
              justifyContent: "center",
              alignItems: "center",
              borderWidth: 2,
              borderColor: "#FCB61A",
              borderTopColor: "transparent",

              borderBottomColor: "transparent"
            }}
          >
            <Icon type="antdesign" name="like1" color="#FFBFAB" />
            <Text style={{ fontFamily: "Signika-Regular" }}>Total points</Text>
            <Text
              style={{
                fontSize: 16,
                lineHeight: 20,
                fontWeight: "bold",
                fontFamily: "Signika-Regular",
                textAlign: "center"
              }}
            >
              {this.state.totalScore}
            </Text>
          </Card>
          <Card
            containerStyle={{
              height: 91,
              width: 155,
              padding: 0,
              margin: 0,
              borderRadius: 10,
              shadowColor: "#000",
              shadowOffset: {
                width: 0,
                height: 2
              },
              shadowOpacity: 0.23,
              shadowRadius: 2.62,
              elevation: 4,
              justifyContent: "center",
              alignItems: "center",
              borderWidth: 2,
              borderColor: "#FCB61A",
              borderTopColor: "transparent",

              borderBottomColor: "transparent"
            }}
          >
            <Icon type="font-awesome" name="certificate" color="#FCB61A" />
            <Text style={{ fontFamily: "Signika-Regular" }}>Certficates</Text>
            <Text
              style={{
                fontSize: 16,
                lineHeight: 20,
                fontWeight: "bold",
                fontFamily: "Signika-Regular",
                textAlign: "center"
              }}
            >
              {this.state.certificate.data.length}
            </Text>
          </Card>
        </View>
      </View>
    );
  };

  imagePickerHandler = () => {
    const options = {
      title: "Select Avatar",
      qualify: 0.2,
      width: 100,
      maxWidth: 100,
      maxHeight: 100,
      height: 100,
      customButtons: [{ name: "fb", title: "Choose Photo from Facebook" }],
      storageOptions: {
        skipBackup: true,
        path: "images"
      }
    };
    ImagePicker.showImagePicker(options, response => {
      console.log("Response = ", response);

      if (response.didCancel) {
        console.log("User cancelled image picker");
      } else if (response.error) {
        console.log("ImagePicker Error: ", response.error);
      } else if (response.customButton) {
        console.log("User tapped custom button: ", response.customButton);
      } else {
        // const source = { uri: response.uri };

        // You can also display the image using data:
        const source = { uri: "data:image/jpeg;base64," + response.data };
        this.setState(
          {
            avatarSource: source
          },
          () => {
            Auth.currentAuthenticatedUser().then(dt => {
              let token = dt
                .getSignInUserSession()
                .getAccessToken()
                .getJwtToken();
              fetch(
                `http://newbee-prod-1105238445.ap-south-1.elb.amazonaws.com/api/user-api/user/${
                  this.state.users[0].id
                }`,
                {
                  method: "PUT",
                  headers: {
                    Accept: "application/json",
                    Authorization: `Bearer ${token}`,
                    "Content-Type": "application/json"
                  },
                  body: JSON.stringify({
                    profileImage: source.uri,
                    firstName: this.state.users[0].firstName,
                    lastName: this.state.users[0].lastName,
                    email: dt.attributes.email,
                    id: this.state.users[0].id,
                    role: "Customer",
                    educations: this.state.users[0].education
                  })
                }
              )
                .then(response => response.json())
                .then(data => {
                  this.getEducations();
                });
            });
          }
        );
      }
    });
  };
  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <ScrollView
          style={styles.container}
          contentContainerStyle={{ flexGrow: 1 }}
        >
          <NavigationEvents
            onDidFocus={() => {
              this.getEducations();
              this.getProgress();
            }}
          />
          <View style={{ flex: 1, padding: 10, backgroundColor: "#FCB61A" }}>
            <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
              <Icon
                type="font-awesome"
                name="close"
                style={{ alignSelf: "flex-end" }}
              />
            </TouchableOpacity>
            <View
              style={{
                flex: 1,
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <Avatar
              rounded
              source={{
                uri: this.state.users[0]
                  ? this.state.users[0].profileImage !==null ? this.state.users[0].profileImage : "https://icon-library.com/images/no-profile-pic-icon/no-profile-pic-icon-27.jpg"
                  : "https://icon-library.com/images/no-profile-pic-icon/no-profile-pic-icon-27.jpg"
              }}
            
                size={120}
                containerStyle={{
                  borderWidth: 5,
                  borderColor: "#FCB61A",
                  shadowColor: "#000",
                  shadowOffset: {
                    width: 0,
                    height: 3
                  },
                  shadowOpacity: 0.27,
                  shadowRadius: 4.65,
                  marginBottom: 10,
                  elevation: 6
                }}
                showAccessory
                accessory={{ name: "camera" }}
                onAccessoryPress={this.imagePickerHandler}
              />
              
              <Text
                style={{
                  fontSize: 20,
                  lineHeight: 25,
                  fontWeight: "bold",
                  fontFamily: "Signika-Regular"
                }}
              >
                Hello {this.state.users[0] ? this.state.users[0].firstName : ""}
              </Text>
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <Icon type="materialicons" name="mail" size={20} />
                <Text
                  style={{
                    fontSize: 17,
                    marginLeft: 5,
                    fontFamily: "Signika-Regular"
                  }}
                >
                  {this.state.users[0] ? this.state.users[0].email : ""}
                </Text>
              </View>
            </View>
          </View>
          <View
            style={{
              flex: 2,
              color: "black",
              padding: 10
            }}
          >
            <TouchableWithoutFeedback
              onPress={() => {
                this.toggleProgress();
                this.setState({ showEducation: false });
              }}
            >
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                  paddingHorizontal: 20,
                  marginTop: 10
                }}
              >
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "flex-start"
                  }}
                >
                  <Icon type="antdesign" name="areachart" color="#FCB61A" />
                  <Text
                    style={{
                      fontSize: 20,
                      marginLeft: 10,
                      fontFamily: "Signika-Regular"
                    }}
                  >
                    Progress
                  </Text>
                </View>
                {this.state.showProgress ? (
                  <Icon
                    type="antdesign"
                    name="up"
                    size={20}
                    color="#999999"
                    onPress={() => {
                      this.toggleProgress();
                      this.setState({ showEducation: false });
                    }}
                  />
                ) : (
                  <Icon
                    type="antdesign"
                    name="down"
                    size={20}
                    color="#999999"
                  />
                )}
              </View>
            </TouchableWithoutFeedback>
            {this.state.showProgress && this.renderProgress()}
            <TouchableWithoutFeedback
              onPress={() => {
                this.toggleEducation();
                this.setState({ showProgress: false });
              }}
            >
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                  paddingHorizontal: 15,
                  marginTop: 40
                }}
              >
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "flex-start"
                  }}
                >
                  <Icon
                    type="font-awesome"
                    name="graduation-cap"
                    color="#FCB61A"
                  />
                  <Text
                    style={{
                      fontSize: 20,
                      marginLeft: 10,
                      fontFamily: "Signika-Regular"
                    }}
                  >
                    Educational Background
                  </Text>
                </View>

                {this.state.showEducation ? (
                  <Icon type="antdesign" name="up" size={20} color="#999999" />
                ) : (
                  <Icon
                    type="antdesign"
                    name="down"
                    size={20}
                    color="#999999"
                  />
                )}
              </View>
            </TouchableWithoutFeedback>
            {this.state.showEducation ? (
              this.state.educationLoading ? (
                <ActivityIndicator color="black" size={40} />
              ) : (
                this.renderEducation()
              )
            ) : null}
            {this.state.showEducation && (
              <TouchableHighlight
                onPress={() => this.setState({ showAddEducation: true })}
              >
                <View
                  style={{
                    flexDirection: "row",
                    marginTop: 20,
                    paddingHorizontal: 20,
                    alignItems: "center"
                  }}
                >
                  <Icon
                    type="materialicons"
                    name="add-circle"
                    color="#FCB61A"
                  />
                  <Text
                    style={{
                      fontSize: 12,
                      fontFamily: "Signika-Regular",
                      marginLeft: 10
                    }}
                  >
                    Add
                  </Text>
                </View>
              </TouchableHighlight>
            )}

            <View
              style={{
                flexDirection: "row",
                paddingHorizontal: 20,
                marginTop: 40
              }}
            >
              <Icon type="font-awesome" name="share-alt" color="#FCB61A" />
              <Text
                style={{
                  fontSize: 20,
                  marginLeft: 20,
                  fontFamily: "Signika-Regular"
                }}
              >
                Share
              </Text>
            </View>
          </View>
          {this.state.showAddEducation && this.renderAddEducationCard()}
          {/* {this.state.showEditEducation && this.renderAddEducationCard()} */}
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});
export default MyAccount;
