import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  TouchableWithoutFeedback,
  FlatList,
  ActivityIndicator
} from "react-native";
import { Card, Icon } from "react-native-elements";
// import logo from '../../Assets/Images/certificate.png';
import { Auth } from "aws-amplify";
import axios from "axios";
import PDFView from "react-native-view-pdf";
const Buffer = require("buffer").Buffer;
import empty from "../../Assets/Images/empty.png";

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showCertificate: false,
      certificateURL:
        "https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf",
      certificateLoading: false,
      certificates: null,
      currentCertificate: null,
      modalLoading: false,
      userName: ""
    };
  }

  static navigationOptions = ({ navigation }) => ({
    drawerLabel: "Certifications",
    drawerIcon: ({ tintColor }) => (
      <Icon
        name="medal"
        type="material-community"
        color={tintColor}
        style={{ fontSize: 24, opacity: 1 }}
      />
    )
  });

  componentDidMount = () => {
    this.setState({ certificateLoading: true });
    Auth.currentAuthenticatedUser().then(dt => {
      let token = dt
        .getSignInUserSession()
        .getAccessToken()
        .getJwtToken();

      fetch(
        `http://newbee-prod-1105238445.ap-south-1.elb.amazonaws.com/api/user-api/user/search/findByEmail?email=${
          dt.attributes.email
        }`,
        {
          method: "GET",
          headers: {
            Accept: "application/json",
            Authorization: `Bearer ${token}`,
            "Content-Type": "application/json"
          }
        }
      )
        .then(profile => profile.json())
        .then(data1 => {
          let uid = data1._embedded.users[0].id;
          const userName = data1._embedded.users[0].firstName;
          axios
            .get(
              `http://newbee-prod-1105238445.ap-south-1.elb.amazonaws.com/api/course-api/certificate/all/${uid}`,
              {
                headers: {
                  Authorization: `Bearer ${token}`
                }
              }
            )
            .then(resp123 => {
              this.setState({
                certificateLoading: false,
                certificates: resp123.data,
                userName
              });
            })
            .catch(e => {
              console.log(e);
              this.setState({ certificateLoading: false });
            });
        });
    });
  };

  getCertificate = (courseDescription, date) => {
    this.setState({ modalLoading: true });
    const data = {
      userName: this.state.userName,
      courseDescription: courseDescription,
      onDate: new Date()
    };

    Auth.currentAuthenticatedUser().then(dt => {
      let token = dt
        .getSignInUserSession()
        .getAccessToken()
        .getJwtToken();
      axios
        .post(
          "http://newbee-prod-1105238445.ap-south-1.elb.amazonaws.com/api/course-api/certificate/course",
          data,
          {
            responseType: "arraybuffer",
            headers: {
              Authorization: `Bearer ${token}`
            }
          }
        )

        .then(response => {
          let encodedAuth = new Buffer(response.data).toString("base64");
          this.setState({ modalLoading: false, certificateURL: encodedAuth });
        })

        .catch(e => {
          this.setState({ modalLoading: false });
          console.log(e);
        });
    });
  };

  renderCertificates = (item, index) => (
    <TouchableWithoutFeedback
      onPress={() => {
        this.getCertificate(item.courseDescription);
        this.setState({ showCertificate: true });
      }}
    >
      <Card
        containerStyle={{
          borderRadius: 10,
          elevation: 6,
          shadowColor: "black",
          shadowOpacity: 0.26,
          shadowOffset: { width: 0, height: 2 },
          shadowRadius: 3,
          overflow: "hidden",
          marginBottom: 10,
          borderRadius: 10,
          borderWidth: 2,

          borderColor: index % 2 === 0 ? "#E4FFC1" : "#FFE3E3",
          borderTopColor: "#F5F5F5",
          borderBottomColor: "#F5F5F5"
        }}
      >
        <View
          style={{
            flexDirection: "row",
            textAlign: "left",
            alignItems: "center"
          }}
        >
          {/* <Image source={logo} /> */}
          <Text
            style={{
              marginBottom: 10,

              justifyContent: "center",
              alignItems: "center",
              fontSize: 22,
              lineHeight: 32,
              fontFamily: "Signika-Regular",
              marginLeft: 20
            }}
          >
            {item.courseDescription}
          </Text>
        </View>
      </Card>
    </TouchableWithoutFeedback>
  );

  renderCertificateModal = () => (
    <Card
      containerStyle={{
        top: "10%",
        borderRadius: 10,
        elevation: 6,
        shadowColor: "black",
        shadowOpacity: 0.26,
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 3,
        overflow: "hidden",
        marginBottom: "2%",
        height: "40%",
        position: "absolute",
        alignSelf: "center",
        width: "90%"
      }}
    >
      <View
        style={{
          flexDirection: "row",
          justifyContent: "flex-end",
          marginBottom: 20
        }}
      >
        <TouchableOpacity>
          <Icon type="antdesign" name="download" />
        </TouchableOpacity>
        <TouchableOpacity
          style={{ marginLeft: 10 }}
          onPress={() => this.setState({ showCertificate: false })}
        >
          <Icon name="close" />
        </TouchableOpacity>
      </View>
      {this.state.modalLoading ? (
        <View
          style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
        >
          <ActivityIndicator size={50} color="black" />
        </View>
      ) : (
        <View
          style={{
            height: 250
          }}
        >
          <PDFView
            fadeInDuration={250.0}
            style={{ flex: 1 }}
            resourceType="base64"
            resource={this.state.certificateURL}
            onLoad={() => console.log(`PDF rendered from `)}
            onError={error => console.log("Cannot render PDF", error)}
          />
        </View>
      )}
    </Card>
  );

  renderEmptyContainer = () => {
    return (
      <ScrollView style={{ flex: 1, paddingTop: '40%' }}>
          <View
            style={{
              flex: 1,
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <Image
              style={{
                flex: 1,
                justifyContent: "center",
                alignItems: "center",
                resizeMode: "contain",
                width: 300,
                height: 200
              }}
              source={empty}
            />
          </View>
          <Text
              style={{
                fontFamily: "Signika-Regular",
                fontWeight: "600",
                fontSize: 24,
                padding:'3%',
                textAlign: 'center',
                lineHeight: 37,
                color: "#000000"
              }}
            >
              Oops, you haven't completed any certications yet
            </Text>
        </ScrollView>
    )
  };

  render() {
    return (
      <View
        style={{
          flex: 1
        }}
      >
        <View
          style={{
            height: "5%",
            justifyContent: "space-between",
            flexDirection: "row",
            alignItems: "center",
            padding: 10
          }}
        >
          <TouchableOpacity onPress={() => this.props.navigation.openDrawer()}>
            <Icon name="menu" color="#FCB61A" size={30} />
          </TouchableOpacity>
          <Text
            style={{
              fontFamily: "Signika-Regular",
              fontSize: 16,
              lineHeight: 20,
              textAlign: "center"
            }}
          >
            Certificates
          </Text>
          <View style={{ flexDirection: "row" }}>
            <TouchableOpacity>
              <Icon name="search" color="#FCB61A" size={20} />
            </TouchableOpacity>
          </View>
        </View>
        <View style={{ flex: 1 }}>
          {this.state.certificateLoading ? (
            <View
              style={{
                flex: 1,
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <ActivityIndicator size={50} color="black" />
            </View>
          ) : (
            <FlatList
              data={this.state.certificates}
              keyExtractor={(item, index) => index.toString()}
              renderItem={({ item, index }) =>
                this.renderCertificates(item, index)
              }
              ListEmptyComponent={this.renderEmptyContainer()}
            />
          )}
        </View>
        {this.state.showCertificate && this.renderCertificateModal()}
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});
export default Dashboard;
