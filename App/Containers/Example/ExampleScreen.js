import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  TouchableHighlight,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';
import contents from '../contents.json'
import one from '../../Assets/Images/1.png';
import two from '../../Assets/Images/2.png';
import { Icon, Button } from 'react-native-elements';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showRealApp: false,
    };
  }
  _onDone = () => {
    this.setState({ showRealApp: true });
  };
  _onSkip = () => {
    this.setState({ showRealApp: true });
  };
  _renderDoneButton = () => (
    <TouchableOpacity
      style={{
        width: 75,
        height: 35,
        justifyContent: 'center',
        borderRadius: 25,
        backgroundColor: '#000000',
        alignItems: 'center',
      }}
      onPress={this._onDone}
    >
      <Text style={{ color: '#FCB61A' }}>Done</Text>
    </TouchableOpacity>
  );
  _renderNextButton = () => (
    <View
      style={{
        width: 75,
        height: 35,
        justifyContent: 'center',
        borderRadius: 25,
        backgroundColor: '#000000',
        alignItems: 'center',
      }}
    >
      <Text style={{ color: '#FCB61A' }}>Next</Text>
    </View>
  );
  _renderSkipButton = () => (
    <Button
      containerStyle={{
        width: 75,
        height: 35,
        justifyContent: 'center',
        borderRadius: 25,
      }}
      buttonStyle={{ backgroundColor: '#FFCF26' }}
      title="Skip"
      titleStyle={{ color: 'white' }}
      onPress={this._onSkip}
    />
  );
  _renderItem = ({ item }) => {
    return (
      <ScrollView style={{ flex: 1 }} contentContainerStyle={{ flex: 1 }}>
        <View
          style={{
            flex: 1,
         
            backgroundColor: 'white',
          }}
        >
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              flex: 2,
            }}
          >
            <Image style={styles.image} source={item.image} />
          </View>
          <View
            style={{
              flex: 1,
            }}
          >
            <Image
              source={require('App/Assets/Images/introCard.png')}
              resizeMode="cover"
              style={{
                marginTop: 10,
                width: '100%',
                height: '100%',
              }}
            />
            <Text style={styles.text}>{item.text}</Text>
          </View>
        </View>
      </ScrollView>
    );
  };
  render() {
    if (this.state.showRealApp) {
      return (
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            padding: 50,
            backgroundColor: '#FCB61A',
          }}
        >
          <Text
            style={{
              lineHeight: 17,
              fontSize: 14,
              fontFamily: 'Signika-Regular',
            }}
          >
           {contents.landingscreen}
          </Text>

          <Text
            style={{
              fontWeight: '600',
              fontSize: 25,
              fontFamily: 'Signika-Regular',
              paddingTop: 20,
              color: '#000000',
              lineHeight: 31,
              paddingBottom: 20,
            }}
          >
            Start Now !
          </Text>
          <TouchableHighlight
            onPress={() => this.props.navigation.navigate('LoginScreen')}
            underlayColor="#FFFFFF"
          >
            <View
              style={{
                width: 50,
                borderRadius: 50,
                height: 50,
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: 'white',
              }}
            >
              <Icon
                onPress={() => this.props.navigation.navigate('LoginScreen')}
                name="ios-arrow-forward"
                type="ionicon"
              />
            </View>
          </TouchableHighlight>
        </View>
      );
    } else {
      //Intro slides
      return (
        <AppIntroSlider
          data={slides}
          renderItem={this._renderItem}
          renderDoneButton={this._renderDoneButton}
          showSkipButton={true}
          renderNextButton={this._renderNextButton}
          renderSkipButton={this._renderSkipButton}
        />
      );
    }
  }
}
const styles = StyleSheet.create({
  image: {
    width: 300,
    height: 500,
  },
  text: {
    fontSize: 14,
    color: 'white',
    fontFamily: 'Signika-Regular',
    textAlign: 'center',
    justifyContent:'center',
    position: 'absolute',
    top: '40%',
    zIndex: 3,
    width: '100%',
    paddingHorizontal: 60,
  },
  title: {
    fontSize: 25,
    color: 'white',
    textAlign: 'center',
  },
});

const slides = [
  {
    key: 's1',
    text: `${contents.introscreen1}`,
    title: '',
    image: two,
    backgroundColor: '#FCB61A',
  },
  {
    key: 's2',
    title: '',
    text: `${contents.introscreen2}`,
    image: one,
    backgroundColor: '#FCB61A',
  },
  {
    key: 's3',
    title: '',
    text: `${contents.introscreen3}`,
    image: two,
    backgroundColor: '#FCB61A',
  },
];
