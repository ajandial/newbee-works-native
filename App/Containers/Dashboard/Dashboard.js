import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  FlatList,
  Image,
  ActivityIndicator,
  TouchableWithoutFeedback
} from "react-native";
import { NavigationEvents } from "react-navigation";
import { Auth } from "aws-amplify";
import { Card, Icon, Button } from "react-native-elements";
import axios from "axios";

class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      courses: [],
      courseSubscription: [],
      subscribedCourses: [],
      name: [],
      error: "",
      loading: false
    };
  }

  static navigationOptions = ({ navigation }) => ({
    drawerLabel: "Dashboard",
    drawerIcon: ({ tintColor }) => (
      <Icon
        name="tachometer"
        color={tintColor}
        type="font-awesome"
        style={{ fontSize: 24, opacity: 1 }}
      />
    )
  });

  async componentDidMount() {
    await this.loadAllInfo();
    await this.loadData();
  }

  loadData = async () => {
    Auth.currentAuthenticatedUser().then(dt => {
      let token = dt
        .getSignInUserSession()
        .getAccessToken()
        .getJwtToken();
      axios
        .get(
          `http://newbee-prod-1105238445.ap-south-1.elb.amazonaws.com/api/course-api/subscription/search/findByUserId?userId=${
            dt.attributes.email
          }`,
          {
            headers: {
              Authorization: `Bearer ${token}`
            }
          }
        )
        .then(response => {
          this.setState(
            {
              courseSubscription: response.data._embedded.coursesSubscription
            },
            () => {
              axios
                .get(`http://newbee-prod-1105238445.ap-south-1.elb.amazonaws.com/api/course-api/course`, {
                  headers: {
                    Authorization: `Bearer ${token}`
                  }
                })
                .then(data => {
                  let allCourse = data.data._embedded.courses;
                  let subscription = [];
                  response.data._embedded.coursesSubscription.forEach(item => {
                    allCourse.forEach(allData => {
                      if (allData.id === item.courseId) {
                        subscription.push(allData);
                      }
                    });
                  });
                  this.setState({
                    subscribedCourses: subscription
                  });
                });
            }
          );
        });
    });
  };

  loadAllInfo = async () => {
    this.setState({ loading: true });
    try {
      Auth.currentAuthenticatedUser().then(dt => {
        let token = dt
          .getSignInUserSession()
          .getAccessToken()
          .getJwtToken();

        fetch(
          `http://newbee-prod-1105238445.ap-south-1.elb.amazonaws.com/api/user-api/user/search/findByEmail?email=${
            dt.attributes.email
          }`,
          {
            method: "GET",
            headers: {
              Accept: "application/json",
              Authorization: `Bearer ${token}`,
              "Content-Type": "application/json"
            }
          }
        )
          .then(profile => profile.json())
          .then(data1 => {
            let uid = data1._embedded.users[0].id;

            axios
              .get(
                `http://newbee-prod-1105238445.ap-south-1.elb.amazonaws.com/api/course-api/progress/search/findByUserId?userId=${uid}`,
                {
                  headers: {
                    Authorization: `Bearer ${token}`
                  }
                }
              )
              .then(resp123 => {
                this.setState({
                  courses: resp123.data._embedded.coursesProgress,
                  loading: false,
                  name: data1._embedded.users[0].firstName
                });
              });
          });
      });
    } catch (error) {
      this.setState({ error, loading: false });
    }
  };

  resumeCourseCard = (item, index) => {
    return (
      <TouchableWithoutFeedback
        onPress={() => {
          this.props.navigation.navigate("CourseDetailsDescription", {
            chapterNumber: item.chapterNumber,
            chapterIndex: item.chapterNumber,
            courseLevel: item.courseLevel,
            courseId: item.courseId
          });
        }}
      >
        <Card
          containerStyle={{
            height: 100,
            width: 200,
            shadowColor: "#000",
            shadowOffset: {
              width: 0,
              height: 2
            },
            shadowOpacity: 0.25,
            shadowRadius: 3.84,
            elevation: 5,
            padding: 0,
            marginHorizontal: 10,
            borderRadius: 2,
            zindex: 0,
            borderWidth: 2,
            borderColor: index % 2 === 0 ? "#FFCF26" : "#8FCAD2",
            borderTopColor: "transparent",
            borderRightColor: "white",
            borderEndColor: "white",
            borderBottomColor: "transparent"
          }}
        >
          <Button
            containerStyle={{
              height: 30,
              width: 100,
              borderRadius: 18,
              justifyContent: "center",
              marginBottom: 10,
              position: "absolute",
              top: "-12%",
              right: 20,
              zIndex: 1,
              shadowColor: "#000",
              shadowOffset: {
                width: 0,
                height: 2
              },
              shadowOpacity: 0.25,
              shadowRadius: 3.84,
              elevation: 5
            }}
            buttonStyle={{
              backgroundColor: index % 2 === 0 ? "#FFCF26" : "#8FCAD2"
            }}
            title={item.courseLevel}
            titleStyle={{
              fontSize: 12,
              height: 20,
              fontFamily: "Signika-Regular",
              color: "white"
            }}
          />
          <View style={{ padding: 5, marginTop: 10, marginBottom: 5 }}>
            <Text
              style={{
                fontSize: 10,
                lineHeight: 13,
                color: "#989595",
                fontFamily: "Signika-Regular"
              }}
            >
              Course
            </Text>
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                alignItems: "center",
                marginTop: 10
              }}
            >
              <Text
                style={{
                  fontSize: 12,
                  lineHeight: 13,
                  fontFamily: "Signika-Regular"
                }}
              >
                {item.courseDescription}
              </Text>
              <Text
                style={{
                  fontFamily: "Signika-Regular",
                  fontSize: 8,
                  lineHeight: 9
                }}
              >
                Chapter:{item.chapterNumber}
              </Text>
            </View>
            <View
              style={{
                marginVertical: 20,
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "space-between"
              }}
            >
              <View
                style={{
                  flex: 3,
                  height: 3,
                  borderRadius: 5,
                  backgroundColor: "#EAEAEA"
                }}
              >
                <View
                  style={{
                    backgroundColor: "#787878",
                    borderRadius: 5,
                    height: 3,
                    width: `${item.progessPercent}%`
                  }}
                />
              </View>
              <Text
                style={{
                  fontSize: 10,
                  lineHeight: 11,
                  fontFamily: "Signika-Regular",
                  marginLeft: 10
                }}
              >
                {item.progessPercent} %
              </Text>
            </View>
          </View>
        </Card>
      </TouchableWithoutFeedback>
    );
  };

  activeCoursesCard = (item, index) => (
    <TouchableWithoutFeedback
      onPress={() => {
        this.props.navigation.navigate("CourseDetails", item.id);
      }}
    >
      <Card
        containerStyle={{
          shadowColor: "#000",
          shadowOffset: {
            width: 0,
            height: 2
          },
          shadowOpacity: 0.25,
          shadowRadius: 3.84,
          elevation: 5,
          padding: 15,
          marginHorizontal: 10,
          marginVertical: 30,
          borderRadius: 10,
          borderWidth: 2,
          borderColor: index % 2 === 0 ? "#E4FFC1" : "#FFE3E3",
          borderTopColor: "transparent",

          borderBottomColor: "transparent"
        }}
      >
        <View
          style={{
            height: 100,
            padding: 10,
            flex: 1
          }}
        >
          <View
            style={{
              flexDirection: "row",
              alignItems: "center"
            }}
          >
            <Text
              style={{
                fontSize: 26,
                lineHeight: 32,
                fontFamily: "Signika-Regular"
              }}
            >
              01
            </Text>
            <Text
              style={{
                fontSize: 14,
                lineHeight: 17,
                fontFamily: "Signika-Regular",
                marginLeft: 10
              }}
            >
              {item.courseDescription}
            </Text>
          </View>
          <View style={{ marginTop: 30 }}>
            <Text
              style={{
                fontFamily: "Signika-Regular",
                fontSize: 14,
                lineHeight: 15
              }}
            >
              Total Chapters: {item.totalChapters}
            </Text>
          </View>
          <View
            style={{
              marginTop: 20,
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "space-between"
            }}
          />
        </View>
      </Card>
    </TouchableWithoutFeedback>
  );

  render() {
    return (
      <View style={{ flex: 1 }}>
        <View
          style={{
            height: 50,
            justifyContent: "space-between",
            flexDirection: "row",
            alignItems: "center",
            padding: 10
          }}
        >
          <NavigationEvents
            onDidFocus={() => {
              this.loadAllInfo();
              this.loadData();
            }}
          />
          <TouchableOpacity onPress={() => this.props.navigation.openDrawer()}>
            <Icon name="menu" color="#FCB61A" size={30} />
          </TouchableOpacity>
          <Text
            style={{
              fontFamily: "Signika-Regular",
              fontSize: 16,
              lineHeight: 20,
              textAlign: "center"
            }}
          >
            Hello {this.state.name}
          </Text>
          <TouchableOpacity>
            <Icon name="search" color="#FCB61A" size={20} />
          </TouchableOpacity>
        </View>
        <View style={{ flex: 1, justifyContent: "flex-end" }}>
          <Card
            containerStyle={{
              height: 117,
              backgroundColor: "#FCB61A",
              borderTopStartRadius: 20,
              borderBottomStartRadius: 50,
              padding: 0,
              margin: 0,
              marginLeft: 10,
              zindex: 0
            }}
          >
            <Button
              containerStyle={{
                width: 49,
                height: 20,
                borderRadius: 18,
                justifyContent: "center",
                marginBottom: 10,
                position: "absolute",
                top: "-10%",
                right: 30,
                zIndex: 5,
                shadowColor: "#000",
                shadowOffset: {
                  width: 0,
                  height: 2
                },
                shadowOpacity: 0.25,
                shadowRadius: 3.84,
                elevation: 5
              }}
              buttonStyle={{
                backgroundColor: "#FCB61A"
              }}
              title="New"
              titleStyle={{
                fontSize: 12,
                lineHeight: 15,
                fontFamily: "Signika-Regular",
                color: "white"
              }}
            />
            <Image
              source={require("App/Assets/Images/Dashboard.png")}
              style={{
                width: 172,
                height: 147,
                position: "absolute",
                top: "-65%",
                left: 10,
                zIndex: 5
              }}
              resizeMode="contain"
            />
            <View
              style={{
                width: "50%",
                alignSelf: "flex-end",
                justifyContent: "flex-end",
                height: "80%",
                marginTop: 15
              }}
            >
              <Text
                style={{
                  fontFamily: "Signika-Regular",
                  fontSize: 12,
                  lineHeight: 15
                }}
              >
                Please click here to navigate to the course section
              </Text>
              <TouchableOpacity
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                  alignSelf: "center",
                  marginTop: 10
                }}
                onPress={() => this.props.navigation.navigate("AllCourses")}
              >
                <Text
                  style={{
                    fontFamily: "Signika-Regular",
                    fontSize: 12,
                    lineHeight: 15,
                    fontWeight: "bold"
                  }}
                >
                  View All Courses
                </Text>
                <Icon
                  type="antdesign"
                  name="arrowright"
                  size={15}
                  style={{ marginLeft: 5 }}
                />
              </TouchableOpacity>
            </View>
          </Card>
        </View>
        <View
          style={{
            flex: 3,
            marginTop: 10,
            paddingHorizontal: 10
          }}
        >
          <View style={{ flex: 1 }}>
            <Text
              style={{
                fontSize: 18,
                lineHeight: 22,
                fontFamily: "Signika-Regular"
              }}
            >
              Start where you left
            </Text>

            <FlatList
              data={this.state.courses}
              horizontal={true}
              showsHorizontalScrollIndicator={false}
              keyExtractor={item => item.toString()}
              renderItem={({ item, index }) =>
                item.progessPercent !== null
                  ? this.resumeCourseCard(item, index)
                  : null
              }
            />
          </View>
          <View style={{ flex: 2 }}>
            <Text
              style={{
                fontSize: 18,
                lineHeight: 22,
                fontFamily: "Signika-Regular"
              }}
            >
              Active Courses
            </Text>
            <FlatList
              data={this.state.subscribedCourses}
              showsVerticalScrollIndicator={false}
              keyExtractor={item => item.id.toString()}
              renderItem={({ item, index }) =>
                this.activeCoursesCard(item, index)
              }
              ListEmptyComponent={
                <View
                  style={{
                    justifyContent: "center",
                    alignItems: "center",
                    height: 100
                  }}
                >
                  <Text style={{ fontSize: 20, fontFamily: "Signika-Regular" }}>
                    No Active Courses Yet!!
                  </Text>
                </View>
              }
              ListFooterComponentStyle={{
                marginVertical: 10,
                alignItems: "center"
              }}
              ListFooterComponent={
                <Button
                  containerStyle={{
                    width: "90%",
                    borderRadius: 5,
                    overflow: "hidden",
                    shadowColor: "#000",
                    shadowOffset: {
                      width: 0,
                      height: 2
                    },
                    shadowOpacity: 0.23,
                    shadowRadius: 2.62,
                    elevation: 4
                  }}
                  buttonStyle={{
                    width: "100%",

                    backgroundColor: "#FCB61A"
                  }}
                  titleStyle={{
                    fontFamily: "Signika-Regular",
                    color: "black",
                    fontSize: 20
                  }}
                  onPress={() => this.props.navigation.navigate("AllCourses")}
                  title="See All Courses"
                />
              }
            />
          </View>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});
export default Dashboard;
