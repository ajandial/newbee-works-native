import React, { Component } from "react";
import {
  View,
  Text,
  ActivityIndicator,
  StyleSheet,
  ScrollView,
  FlatList,
  TouchableWithoutFeedback,
  TouchableOpacity
} from "react-native";
import axios from "axios";
import { Auth } from "aws-amplify";
import { Card, Button, Icon } from "react-native-elements";

class Dashboard extends Component {
  static navigationOptions = ({ navigation }) => ({
    drawerLabel: "Active Courses",
    drawerIcon: ({ tintColor }) => (
      <Icon
        name="school"
        color={tintColor}
        style={{ fontSize: 24, opacity: 1 }}
        onPress={() => this.props.navigation.push("Dashboard")}
      />
    )
  });

  constructor(props) {
    super(props);
    this.state = {
      courseSubscription: [],
      subscribedCourses: []
    };
  }

  componentDidMount() {
    Auth.currentAuthenticatedUser().then(dt => {
      let token = dt
        .getSignInUserSession()
        .getAccessToken()
        .getJwtToken();

      axios
        .get(
          `http://newbee-prod-1105238445.ap-south-1.elb.amazonaws.com/api/course-api/subscription/search/findByUserId?userId=${
            dt.attributes.email
          }`,
          {
            headers: {
              Authorization: `Bearer ${token}`
            }
          }
        )
        .then(response => {
          this.setState(
            {
              courseSubscription: response.data._embedded.coursesSubscription
            },
            () => {
              axios
                .get(`http://newbee-prod-1105238445.ap-south-1.elb.amazonaws.com/api/course-api/course`, {
                  headers: {
                    Authorization: `Bearer ${token}`
                  }
                })
                .then(data => {
                  let allCourse = data.data._embedded.courses;
                  let subscription = [];
                  response.data._embedded.coursesSubscription.forEach(item => {
                    allCourse.forEach(allData => {
                      if (allData.id === item.courseId) {
                        subscription.push(allData);
                      }
                    });
                  });
                  this.setState({
                    subscribedCourses: subscription
                  });
                });
            }
          );
        });
    });
  }

  loadData = () => {
    Auth.currentAuthenticatedUser().then(dt => {
      let token = dt
        .getSignInUserSession()
        .getAccessToken()
        .getJwtToken();
      axios
        .get(
          `http://newbee-prod-1105238445.ap-south-1.elb.amazonaws.com/api/course-api/subscription/search/findByUserId?userId=${
            dt.attributes.email
          }`,
          {
            headers: {
              Authorization: `Bearer ${token}`
            }
          }
        )
        .then(response => {
          this.setState(
            {
              courseSubscription: response.data._embedded.coursesSubscription
            },
            () => {
              axios
                .get(`http://newbee-prod-1105238445.ap-south-1.elb.amazonaws.com/api/course-api/course`, {
                  headers: {
                    Authorization: `Bearer ${token}`
                  }
                })
                .then(data => {
                  let allCourse = data.data._embedded.courses;
                  let subscription = [];
                  response.data._embedded.coursesSubscription.forEach(item => {
                    allCourse.forEach(allData => {
                      if (allData.id === item.courseId) {
                        subscription.push(allData);
                      }
                    });
                  });
                  this.setState({
                    subscribedCourses: subscription
                  });
                });
            }
          );
        });
    });
  };

  componentDidMount() {
    this.loadData();
    //Here is the Trick
    const { navigation } = this.props;
    //Adding an event listner om focus
    //So whenever the screen will have focus it will set the state to zero
    this.focusListener = navigation.addListener("didFocus", () => {
      this.loadData();
    });
  }

  componentWillUnmount() {
    // Remove the event listener before removing the screen from the stack
    this.focusListener.remove();
  }

  renderCourseCard = (item, index) => (
    <TouchableWithoutFeedback
      onPress={() => {
        this.props.navigation.navigate("CourseDetails", item.id);
      }}
    >
      <Card
        containerStyle={{
          borderRadius: 10,
          elevation: 6,
          shadowColor: "black",
          shadowOpacity: 0.26,
          shadowOffset: { width: 0, height: 2 },
          shadowRadius: 3,
          overflow: "hidden",
          marginBottom: 10,
          borderWidth: 2,
          borderColor: index % 2 === 0 ? "#FFCF26" : "#8FCAD2",
          borderTopColor: "white",
          borderRightColor: index % 2 === 0 ? "#FFCF26" : "#8FCAD2",
          borderEndColor: index % 2 === 0 ? "#FFCF26" : "#8FCAD2",
          borderBottomColor: "white"
        }}
      >
        <View>
          <Text
            style={{
              marginBottom: 10,
              fontFamily: "Signika-Regular",
              lineHeight: 15,
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <Text style={styles.boldText}> 01 </Text>
            {item.courseDescription}
          </Text>
          <Text
            style={{
              marginBottom: 10,
              fontSize: 10,
              fontFamily: "Signika-Regular",
              lineHeight: 15
            }}
          >
            Total Chapter : {item.totalChapters}
          </Text>
        </View>
      </Card>
    </TouchableWithoutFeedback>
  );

  renderFooter() {
    return (
      <View style={styles.footer}>
        {this.state.fetching_from_server ? (
          <ActivityIndicator color="black" style={{ margin: 15 }} />
        ) : null}
      </View>
    );
  }

  render() {
    return (
      <View style={styles.container}>
        <View
          style={{
            height: "5%",
            justifyContent: "space-between",
            flexDirection: "row",
            alignItems: "center",
            padding: 10
          }}
        >
          <TouchableOpacity onPress={() => this.props.navigation.openDrawer()}>
            <Icon name="menu" color="#FCB61A" size={30} />
          </TouchableOpacity>
          <Text
            style={{
              fontFamily: "Signika-Regular",
              fontSize: 16,
              lineHeight: 20,
              fontWeight: 'bold',
              textAlign: "center"
            }}
          >
            Active Courses
          </Text>
          <View style={{ flexDirection: "row" }}>
            <TouchableOpacity>
              {/* <Icon
                type="font-awesome"
                name="sliders"
                color="#FCB61A"
                size={40}
                style={{ marginRight: 5 }}
              /> */}
            </TouchableOpacity>
            <TouchableOpacity>
              <Icon name="search" color="#FCB61A" size={20} />
            </TouchableOpacity>
          </View>
        </View>
        {this.state.loading ? (
          <ActivityIndicator size={40} color="black" style={{ flex: 1 }} />
        ) : this.state.subscribedCourses.length > 0 ? (
          <FlatList
            data={this.state.subscribedCourses}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item, index }) => {
              return this.renderCourseCard(item, index);
            }}
            ListFooterComponent={this.renderFooter.bind(this)}
          />
        ) : (
          <View
            style={{
              flex: 1,
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <Text
              style={{
                fontFamily: "Signika-Regular",
                fontWeight: "700",
                fontSize: 20,
                lineHeight: 21
              }}
            >
              No Subscribed Courses Yet!!
            </Text>
          </View>
        )}
        <View
          style={{
            position: "absolute",
            bottom: 10,
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center",
            marginHorizontal: 20
          }}
        />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  boldText: {
    fontSize: 22,
    lineHeight: 32,
    fontFamily: "Signika-Regular"
  }
});
export default Dashboard;
