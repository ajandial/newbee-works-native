import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Image,
  ActivityIndicator
} from "react-native";
import { Icon, Card, Avatar } from "react-native-elements";
import axios from "axios";
import { Auth } from "aws-amplify";

class CourseDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      error: null,
      currentCourse: null
    };
  }
  static navigationOptions = ({ navigation }) => ({
    headerTitle: "Courses",
    drawerIcon: ({ tintColor }) => (
      <Icon
        name="tachometer"
        color={tintColor}
        type="font-awesome"
        style={{ fontSize: 24, opacity: 1 }}
      />
    )
  });

  componentDidMount = async () => {
    try {
      this.setState({ loading: true });

      Auth.currentSession().then(dt => {
        let token = dt.getAccessToken();
        let jwt = token.getJwtToken();

        axios
          .get(
            `http://newbee-prod-1105238445.ap-south-1.elb.amazonaws.com/api/course-api/course/${
              this.props.navigation.state.params
            }`,
            {
              headers: {
                Authorization: `Bearer ${jwt}`
              }
            }
          )
          .then(response => {
            this.setState({ currentCourse: response.data, loading: false });
          });
      });
    } catch (error) {
      this.setState({
        loading: false,
        error: "Something Went Wrong Please Try Again"
      });
    }
  };

  render() {
    const courseItem = this.state.currentCourse;
    return (
      <View style={styles.container}>
        <View
          style={{
            height: 35,
            justifyContent: "space-between",
            flexDirection: "row",
            alignItems: "center",
            padding: 10
          }}
        >
          <TouchableOpacity onPress={() => this.props.navigation.openDrawer()}>
            <Icon name="menu" color="#FCB61A" size={30} />
          </TouchableOpacity>
          <Text
            style={{
              fontFamily: "Signika-Regular",
              fontSize: 16,
              flex: 1,
              lineHeight: 20,
              fontWeight: "700",
              textAlign: "center"
            }}
          >
            Courses
          </Text>
        </View>
        {this.state.loading ? (
          <ActivityIndicator color="black" size={30} />
        ) : courseItem ? (
          <ScrollView style={{ flex: 1 }}>
            <View
              style={{
                flex: 1,
                paddingTop: 20,
                paddingHorizontal: 20
              }}
            >
              <Text style={styles.courseText}>Course</Text>
              <Text style={styles.courseNum}>01</Text>
              <View style={{ marginTop: 20 }}>
                <Text style={styles.courseDetailsText}>
                  {courseItem.courseDescription}
                </Text>
              </View>
              <View style={{ marginTop: 30 }}>
                {courseItem.level.map((item, index) => {
                  return (
                    <TouchableWithoutFeedback
                      onPress={() =>
                        this.props.navigation.navigate("CourseDetailsInner", {
                          item,
                          level: courseItem.level[index].level,
                          levelNumber: index,
                          count: courseItem.level.length,
                          courses: courseItem.level,
                          courseItem: this.state.currentCourse
                        })
                      }
                    >
                      <Card
                        containerStyle={{
                          marginHorizontal: 0,
                          borderRadius: 10,
                          elevation: 6,
                          shadowColor: "black",
                          shadowOpacity: 0.26,
                          shadowOffset: {
                            width: 0,
                            height: 2
                          },
                          shadowRadius: 3,
                          overflow: "hidden",
                          marginBottom: 10,
                          flex: 1,
                          justifyContent: "space-between",
                          minHeight: 71,
                          borderWidth: 2,
                          borderColor: index % 2 === 0 ? "#FFCF26" : "#8FCAD2",
                          borderTopColor: "#F5F5F5",
                          borderColor: index % 2 === 0 ? "#FFCF26" : "#8FCAD2",
                          borderColor: index % 2 === 0 ? "#FFCF26" : "#8FCAD2",
                          borderBottomColor: "#F5F5F5"
                        }}
                      >
                        <View
                          style={{
                            height: 50,
                            flexDirection: "row",
                            justifyContent: "space-between",
                            alignItems: "center"
                          }}
                        >
                          <View>
                            <Text
                              style={{
                                marginVertical: 5,
                                fontFamily: "Signika-Regular",
                                lineHeight: 35,
                                justifyContent: "center",
                                alignItems: "center"
                              }}
                            >
                              <Text
                                style={{
                                  color:
                                    index % 2 === 0 ? "#7DE0FF" : "#EFB726",
                                  fontSize: 25
                                }}
                              >
                                •
                              </Text>
                              {"   "}
                              <Text style={styles.courseText}> Level </Text>
                              <Text style={styles.boldText}> {item.level}</Text>
                            </Text>
                            <Text
                              style={{
                                marginVertical: 5,
                                fontSize: 12,
                                fontFamily: "Signika-Regular",
                                lineHeight: 15,
                                marginLeft: 25
                              }}
                            >
                              {`Chapter ${item.startingChapter}-${
                                item.endChapter
                              }`}
                            </Text>
                          </View>
                          <View>
                            <Icon
                              type="antdesign"
                              name="right"
                              color="#FCB61A"
                            />
                          </View>
                        </View>
                      </Card>
                    </TouchableWithoutFeedback>
                  );
                })}
              </View>
            </View>
            <View
              style={{
                flex: 1,
                marginTop: 20,
                alignItems: "center",
                width: "45%",
                padding: 10,
                marginLeft:20,
                flexDirection: "row",
                borderRadius: 20,
                backgroundColor: "rgba(228, 255, 193, 0.48)"
              }}
            >
              <Avatar
                containerStyle={{ alignSelf: "center" }}
                source={require("../../Assets/Images/usericon1.png")}
              />
              <View
                style={{
                  height: 30,
                  width: 3,
                  marginLeft: 10,
                  borderRadius: 6,
                  shadowColor: "#000",
                  shadowOffset: {
                    width: 0,
                    height: 2
                  },
                  shadowOpacity: 0.25,
                  shadowRadius: 3.84,
                  elevation: 5,
                  backgroundColor: "#FAC408"
                }}
              />
              <View>
                <Text style={styles.courseText}>Author</Text>
                <Text
                  style={{
                    fontSize: 14,
                    marginLeft: 10,
                    lineHeight: 17,
                    fontFamily: "Signika-Regular"
                  }}
                >
                  {courseItem.author}
                </Text>
              </View>
            </View>
            <View
              style={{
                flex: 1,
                justifyContent: "center"
              }}
            >
              <View
                style={{
                  marginTop: 20,
                  alignItems: "center",
                  width: "40%",
                  padding: 10,
                  flexDirection: "row",
                  borderTopLeftRadius: 25,
                  borderBottomLeftRadius: 25,
                  right: 0,
                  alignSelf: "flex-end",
                  backgroundColor: "#f5f5f5"
                }}
              >
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-between",
                    alignItems: "center",
                    flex: 1
                  }}
                >
                  <Text
                    style={{
                      fontSize: 16,
                      lineHeight: 20,
                      fontFamily: "Signika-Regular"
                    }}
                  >
                    Actual Price
                  </Text>
                  <Text
                    style={{
                      fontSize: 16,
                      lineHeight: 20,
                      fontFamily: "Signika-Regular"
                    }}
                  >
                    <Text style={{ color: "black" }}>₹</Text>{" "}
                    {courseItem.coursePrice}
                  </Text>
                </View>
              </View>
              {courseItem.discountedPrice !== 0 ? (
                <View
                  style={{
                    marginTop: 20,
                    alignItems: "center",
                    width: "40%",
                    padding: 10,
                    flexDirection: "row",
                    borderTopLeftRadius: 25,
                    borderBottomLeftRadius: 25,
                    right: 0,
                    alignSelf: "flex-end",
                    backgroundColor: "#f5f5f5"
                  }}
                >
                  <View
                    style={{
                      flexDirection: "row",
                      justifyContent: "space-between",
                      alignItems: "center",
                      flex: 1
                    }}
                  >
                    <Text
                      style={{
                        fontSize: 16,
                        lineHeight: 20,
                        fontFamily: "Signika-Regular",
                        color: "#FAC408"
                      }}
                    >
                      Offer Price
                    </Text>
                    <Text
                      style={{
                        fontSize: 16,
                        lineHeight: 20,
                        fontFamily: "Signika-Regular",
                        color: "#FAC408"
                      }}
                    >
                      <Text style={{ color: "black" }}>₹</Text>{" "}
                      {courseItem.discountedPrice}
                    </Text>
                  </View>
                </View>
              ) : null}
            </View>
          </ScrollView>
        ) : (
          <Text>{this.state.error}</Text>
        )}
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  courseText: {
    fontSize: 12,
    lineHeight: 40,
    marginLeft: 12,
    color: "#959595",
    fontWeight: "300",
    fontFamily: "Signika-Regular"
  },
  courseNum: {
    fontSize: 32,
    lineHeight: 39,
    color: "#B0D283",
    marginLeft: 12,
    fontWeight: "300",
    fontFamily: "Signika-Regular"
  },
  boldText: {
    fontSize: 22,
    lineHeight: 40,
    fontFamily: "Signika-Regular"
  },
  courseDetailsText: {
    fontSize: 12,
    lineHeight: 15,
    marginLeft: 12,
    fontFamily: "Signika-Regular",
    fontWeight: "300",
  }
});
export default CourseDetails;
