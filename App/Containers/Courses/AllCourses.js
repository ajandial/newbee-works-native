import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  TouchableWithoutFeedback,
  ActivityIndicator,
  FlatList,
  TextInput,
} from 'react-native';
import { Auth } from 'aws-amplify';
import { Icon, Card, Button } from 'react-native-elements';
import axios from 'axios';

class AllCourses extends Component {
  constructor(props) {
    super(props);
    this.state = {
      size: 8,
      page: 0,
      totalPages: 0,
      isListEnd: false,
      fetching_from_server: false,
      courses: [],
      loading: false,
      isLoading: false,
      isRefreshing: false,
      error: '',
      showSearchBar: false,
      searchText: '',
      arrayholder: [],
    };
  }
  static navigationOptions = ({ navigation }) => ({
    headerTitle: 'Courses',
    drawerIcon: ({ tintColor }) => (
      <Icon
        name="tachometer"
        color={tintColor}
        type="font-awesome"
        style={{ fontSize: 24, opacity: 1 }}
      />
    ),
  });

  toggleSearchBar = () =>
    this.setState((prevState) => ({ showSearchBar: !prevState.showSearchBar }));
  componentDidMount = async () => {
    this.setState({ loading: true });
    await this.loadCourse();
  };

  searchFilterFunction = () => {
    const newData = this.state.courses.filter((item) => {
      const itemData = `${item.courseDescription.toUpperCase()}`;

      const textData = this.state.searchText.toUpperCase();

      return itemData.indexOf(textData) > -1;
    });

    this.setState({ arrayholder: newData });
  };

  loadCourse = async () => {
    const { size, page } = this.state;
    page === 1
      ? this.setState({ fetching_from_server: false })
      : this.setState({ fetching_from_server: true });
    try {
      Auth.currentSession().then((dt) => {
        let token = dt.getAccessToken();
        let jwt = token.getJwtToken();
        axios
          .get(`http://newbee-prod-1105238445.ap-south-1.elb.amazonaws.com/api/course-api/course?size=${size}&page=${page}`, {
            headers: {
              Authorization: `Bearer ${jwt}`,
            },
          })
          .then((response) => {
            this.setState({
              fetching_from_server: false,
              totalPages: response.data.page.totalPages,
              courses:
                page === 0
                  ? response.data._embedded.courses
                  : [...this.state.courses, ...response.data._embedded.courses],
              arrayholder:
                page === 0
                  ? response.data._embedded.courses
                  : [...this.state.courses, ...response.data._embedded.courses],
              loading: false,
            });
          });
      });
    } catch (error) {
      this.setState({ error, loading: false });
    }
  };

  handleLoadMore = () => {
    this.setState(
      {
        page: this.state.page + 1,
      },
      () => {
        if (this.state.page <= this.state.totalPages) {
          this.loadCourse();
        }
      }
    );
  };

  renderCourseCard = (item, index) => (
    <TouchableWithoutFeedback
      onPress={() => this.props.navigation.navigate('CourseDetails', item.id)}
    >
      <Card
        containerStyle={{
          borderRadius: 10,
          elevation: 6,
          shadowColor: 'black',
          shadowOpacity: 0.26,
          shadowOffset: { width: 0, height: 2 },
          shadowRadius: 3,
          overflow: 'hidden',
          marginBottom: 10,
          borderRadius: 10,
          borderWidth: 2,
          borderColor: index % 2 === 0 ? '#E4FFC1' : '#FFE3E3',
          borderTopColor: "#F5F5F5",
          borderBottomColor: "#F5F5F5",
        }}
      >
        <View>
          <Text
            style={{
              marginBottom: 10,
              fontFamily: 'Signika-Regular',
              lineHeight: 15,
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            <Text style={styles.boldText}> 01 </Text>
            {item.courseDescription}
          </Text>
          <Text
            style={{
              marginBottom: 10,
              fontSize: 10,
              fontFamily: 'Signika-Regular',
              lineHeight: 15,
            }}
          >
            Total Chapter : {item.totalChapters}
          </Text>
        </View>
      </Card>
    </TouchableWithoutFeedback>
  );

  renderFooter() {
    return (
      <View style={styles.footer}>
        {this.state.fetching_from_server ? (
          <ActivityIndicator color="black" style={{ margin: 15 }} />
        ) : null}
      </View>
    );
  }

  render() {
    return (
      <View style={styles.container}>
        <View
          style={{
            height: '5%',
            justifyContent: 'space-between',
            flexDirection: 'row',
            alignItems: 'center',
            padding: 10,
          }}
        >
          <TouchableOpacity
            onPress={() => this.props.navigation.openDrawer()}
          >
            <Icon name="menu" color="#FCB61A" size={30} />
          </TouchableOpacity>
          <Text
            style={{
              fontFamily: 'Signika-Regular',
              fontSize: 16,
              lineHeight: 20,
              textAlign: 'center',
            }}
          >
            Courses
          </Text>
          <View style={{ flexDirection: 'row' }}>
            <TouchableOpacity>
              <Icon
                type="font-awesome"
                name="sliders"
                color="#FCB61A"
                size={30}
                style={{ marginRight: 5 }}
              />
            </TouchableOpacity>
            <TouchableOpacity onPress={this.toggleSearchBar}>
              <Icon name="search" color="#FCB61A" size={20} />
            </TouchableOpacity>
          </View>
        </View>
        {this.state.showSearchBar && (
          <View
            style={{
              height: 40,
              borderRadius: 5,
              backgroundColor: 'white',
              margin: 20,

              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
              paddingHorizontal: 5,
              shadowColor: '#000',
              shadowOffset: {
                width: 0,
                height: 2,
              },
              shadowOpacity: 0.25,
              shadowRadius: 3.84,

              elevation: 5,
            }}
          >
            <Icon
              type="antdesign"
              name="search1"
              size={15}
              color="#999999"
            />
            <TextInput
              value={this.state.searchText}
              onChangeText={(text) => {
                this.setState({ searchText: text }, () => {
                  this.searchFilterFunction(text);
                });
              }}
              style={{ flex: 1, paddingLeft: 5 }}
              placeholder="Search A Course"
            />
          </View>
        )}

        {this.state.loading ? (
          <ActivityIndicator size={40} color="black" style={{ flex: 1 }} />
        ) : this.state.courses.length > 0 ? (
          <>
            <FlatList
              data={this.state.arrayholder}
              keyExtractor={(item, index) => index.toString()}
              onEndReached={({ distanceFromEnd }) => {
                {
                  this.handleLoadMore();
                }
              }}
              // onEndReached={() => }
              onEndReachedThreshold={0}
              renderItem={({ item, index }) => {
                return this.renderCourseCard(item, index);
              }}
              ListFooterComponent={this.renderFooter.bind(this)}
            />
          </>
        ) : (
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            <Text
              style={{
                fontFamily: 'Signika-Regular',
                fontWeight: '700',
                fontSize: 20,
                lineHeight: 21,
              }}
            >
              No Courses Yet!!
            </Text>
          </View>
        )}
        <View
          style={{
            position: 'absolute',
            bottom: 10,
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            marginHorizontal: 20,
          }}
        />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  boldText: {
    fontSize: 22,
    lineHeight: 32,
    fontFamily: 'Signika-Regular',
  },
});
export default AllCourses;
