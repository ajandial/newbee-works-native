import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  FlatList,
  TouchableHighlight,
  TouchableOpacity,
  TouchableWithoutFeedback
} from "react-native";
import { Auth } from "aws-amplify";
import axios from "axios";
import { Icon, Card, Image, Button } from "react-native-elements";

class CourseDetailsInner extends Component {
  constructor(props) {
    super(props);
    this.state = {
      subscriptionStatus: false,
      showSubscribeCard: false
    };
  }

  componentDidMount() {
    Auth.currentAuthenticatedUser().then(dt => {
      let token = dt
        .getSignInUserSession()
        .getAccessToken()
        .getJwtToken();
      axios
        .get(
          `http://newbee-prod-1105238445.ap-south-1.elb.amazonaws.com/api/course-api/subscription/search/findByUserId?userId=${
            dt.attributes.email
          }`,
          {
            headers: {
              Authorization: `Bearer ${token}`
            }
          }
        )
        .then(response => {
          let flag = false;
          response.data._embedded.coursesSubscription.forEach(item => {
            if (
              this.props.navigation.state.params.courseItem.id === item.courseId
            ) {
              flag = true;
            }
          });
          this.setState({
            subscriptionStatus: flag
          });
        });
    });
  }

  static navigationOptions = ({ navigation }) => ({
    headerTitle: "Courses",
    drawerIcon: ({ tintColor }) => (
      <Icon
        name="tachometer"
        color={tintColor}
        type="font-awesome"
        style={{ fontSize: 24, opacity: 1 }}
      />
    )
  });

  toggleSubscribeCard = () =>
    this.setState(state => ({
      showSubscribeCard: !state.showSubscribeCard
    }));

  renderCourseCard = (item, index) => {
    return (
      <TouchableWithoutFeedback
        onPress={() => {
          if (
            this.state.subscriptionStatus === false &&
            this.props.navigation.state.params.levelNumber === 0 &&
            index === 0
          ) {
            this.props.navigation.navigate("CourseDetailsDescription", {
              courseId: this.props.navigation.state.params.courseItem.id,
              courseLevel: this.props.navigation.state.params.courseItem.level,
              chapterNumber: item.chapterNumber,
              chapterIndex:index +1,
              courseLevel: this.props.navigation.state.params.level
            });
          } else if (this.state.subscriptionStatus === true) {
            this.props.navigation.navigate("CourseDetailsDescription", {
              courseId: this.props.navigation.state.params.courseItem.id,
              courseLevel: this.props.navigation.state.params.courseItem.level,
              chapterNumber: item.chapterNumber,
              chapterIndex:index +1,
              courseLevel: this.props.navigation.state.params.level
            });
          } else {
            this.setState({
              showSubscribeCard: true
            });
          }
        }}
      >
        <Card
          containerStyle={{
            marginHorizontal: 10,
            marginVertical: 10,
            borderRadius: 10,
            height:250,
            flex:1,
            elevation: 6,
            shadowColor: "black",
            shadowOpacity: 0.26,
            shadowOffset: { width: 0, height: 2 },
            shadowRadius: 3,
            overflow: "hidden",
            marginBottom: "2%",
            width: 142
          }}
          image={require("../../Assets/Images/splash.png")}
          imageWrapperStyle={{
            width: "100%",
            height: "20%",
            alignSelf: "center"
          }}
          imageProps={{
            resizeMode: "contain"
          }}
        >
          <View style={{ height: 100}}>
            <Text style={styles.chapter}>Chapter {item.chapterNumber}</Text>
            <Text style={styles.chapterDetails}>
              {item.chapterDescription}{" "}
            </Text>
            <Text style={styles.runtime}>{item.timeRequired} min</Text>
          </View>
        </Card>
      </TouchableWithoutFeedback>
    );
  };

  render() {
    const courseItem = this.props.navigation.state.params.item;
    return (
      <View style={styles.container}>
        <View
          style={{
            height: "5%",
            justifyContent: "space-between",
            flexDirection: "row",
            alignItems: "center",
            padding: 10
          }}
        >
          <TouchableOpacity onPress={() => this.props.navigation.openDrawer()}>
            <Icon name="menu" color="#FCB61A" size={30} />
          </TouchableOpacity>
          <Text
            style={{
              fontFamily: "Signika-Regular",
              fontSize: 16,
              lineHeight: 20,
              textAlign: "center"
            }}
          >
            Courses
          </Text>
          <View style={{ flexDirection: "row" }}>
            <TouchableOpacity>
              <Icon
                type="font-awesome"
                name="sliders"
                color="#FCB61A"
                size={30}
                style={{ marginRight: 5 }}
              />
            </TouchableOpacity>
            <TouchableOpacity>
              <Icon name="search" color="#FCB61A" size={20} />
            </TouchableOpacity>
          </View>
        </View>
        <ScrollView
          style={{
            flex: 1,
            paddingTop: "10%",
            paddingHorizontal: "3%"
          }}
        >
          <Text style={styles.courseText}>Level</Text>
          <Text style={styles.courseNum}>{courseItem.level}</Text>
          <FlatList
            data={courseItem.chapters}
            horizontal={false}
            numColumns={2}
            showsHorizontalScrollIndicator={false}
            keyExtractor={item => item.chapterNumber.toString()}
            renderItem={({ item, index }) => this.renderCourseCard(item, index)}
            ListEmptyComponent={
              <Text
                style={{
                  textAlign: "center",
                  fontSize: 20,
                  fontFamily: "Signika-Regular",
                  marginTop: 10
                }}
              >
                No Chapters Yet!!
              </Text>
            }
          />
        </ScrollView>
        {this.state.showSubscribeCard && (
          <Card
            containerStyle={{
              borderRadius: 10,
              elevation: 6,
              shadowColor: "black",
              shadowOpacity: 0.26,
              shadowOffset: { width: 0, height: 2 },
              shadowRadius: 3,
              overflow: "hidden",
              marginBottom: "2%",
              height: "60%",
              position: "absolute",
              alignSelf: "center",
              width: "90%"
            }}
          >
            <TouchableHighlight
              underlayColor="white"
              onPress={this.toggleSubscribeCard}
            >
              <Icon name="close" style={{ alignSelf: 'flex-end' }} />
            </TouchableHighlight>
            <View
              style={{
                alignItems: "center",
                width: "100%",
                height: "100%"
              }}
            >
              <Image
                source={require("App/Assets/Images/Subscribe.jpeg")}
                style={{
                  width: 250,
                  height: 250
                }}
              />
              <Text
                style={{
                  marginTop: "3%",
                  fontSize: 24,
                  fontWeight: "300",
                  fontFamily: "Signika-Regular",
                  textAlign: "center"
                }}
              >
                Please, subscribe to{"\n"} read more...
              </Text>
              <Button
                containerStyle={{
                  width: "80%",
                  marginTop: "5%",
                  elevation: 6,
                  shadowColor: "black",
                  shadowOpacity: 0.26,
                  shadowOffset: { width: 0, height: 2 },
                  shadowRadius: 3,
                  overflow: "hidden"
                }}
                buttonStyle={{
                  width: "100%",

                  backgroundColor: "#FCB61A"
                }}
                titleStyle={{
                  fontFamily: "Signika-Regular",
                  color: "black",
                  fontSize: 20
                }}
                onPress={() =>
                  this.props.navigation.navigate(
                    "Subscription",
                    this.props.navigation.state.params
                  )
                }
                title="Subscribe Now"
              />
            </View>
          </Card>
        )}
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center"
  },
  courseText: {
    fontSize: 12,
    color: "#959595",
    fontWeight: "300",
    justifyContent: "center",
    fontFamily: "Signika-Regular"
  },
  courseNum: {
    fontSize: 32,
    justifyContent: "center",
    color: "#B0D283",

    fontFamily: "Signika-Regular"
  },
  chapter: {
    fontSize: 14,
    justifyContent: "center",
    fontFamily: "Signika-Regular"
  },
  chapterDetails: {
    justifyContent: "center",
    fontSize: 12,
    fontFamily: "Signika-Regular"
  },
  runtime: {
    marginTop: 10,
    fontFamily: "Signika-Regular",
    fontSize: 10,
    lineHeight: 10,
    color: "#474747"
  }
});
export default CourseDetailsInner;
