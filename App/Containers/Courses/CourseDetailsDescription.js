import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  FlatList,
  ActivityIndicator,
  TouchableHighlight,
} from 'react-native';
import { WebView } from 'react-native-webview';
import { Button, Icon } from 'react-native-elements';
import axios from 'axios';
import { Auth } from 'aws-amplify';
import GestureRecognizer from 'react-native-swipe-gestures';

class CourseDetailsDescription extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userId: null,
      courseLevel: '',
      courseDescription: '',
      courseId: '',
      courseItem: null,
      progressPercent: 0,
      detailsArray: [],
      currentIndex: 1,
    };
  }
  componentDidMount = async () => {
    Auth.currentAuthenticatedUser().then((dt) => {
      let token = dt
        .getSignInUserSession()
        .getAccessToken()
        .getJwtToken();
      fetch(
        `http://newbee-prod-1105238445.ap-south-1.elb.amazonaws.com/api/user-api/user/search/findByEmail?email=${
          dt.attributes.email
        }`,
        {
          method: 'GET',
          headers: {
            Authorization: `Bearer ${token}`,
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
        }
      )
        .then((response) => response.json())
        .then((data) => {
          this.setState({ userId: data._embedded.users[0].id });
        })
        .catch((e) => console.log(e));
    });
    try {
      Auth.currentAuthenticatedUser().then((dt) => {
        let token = dt
          .getSignInUserSession()
          .getAccessToken()
          .getJwtToken();
        axios
          .get(
            `http://newbee-prod-1105238445.ap-south-1.elb.amazonaws.com/api/course-api/course/${
              this.props.navigation.state.params.courseId
            }`,
            {
              headers: {
                Authorization: `Bearer ${token}`,
                Accept: 'application/json',
                'Content-Type': 'application/json',
              },
            }
          )
          .then((response) => {
            const level = response.data.level.filter(
              (item) =>
                item.level === this.props.navigation.state.params.courseLevel
            );

            const courseItem = level[0].chapters.filter(
              (item) =>
                item.chapterNumber ===
                this.props.navigation.state.params.chapterNumber
            );

            // console.log(typeof courseItem[0].courseDetails);
            const newArray = courseItem[0].courseDetails.split(
              '||PAGE_BREAK||'
            );

            // const newArray = [];
            // let element = '';

            // for (
            //   let index = 0;
            //   index < courseItem[0].courseDetails.length;
            //   index++
            // ) {
            //   if (index === 0) {
            //     element += courseItem[0].courseDetails[index];
            //     continue;
            //   }
            //   element += courseItem[0].courseDetails[index];

            //   if (index % 1000 === 0) {
            //     newArray.push(element);
            //     element = '';
            //   }
            //   if (index + 1 === courseItem[0].courseDetails.length)
            //     newArray.push(element);
            // }

            this.setState({
              courseDescription: response.data.courseDescription,
              courseId: response.data.id,
              courseItem: courseItem[0],
              progressPercent:
                (this.props.navigation.state.params.chapterIndex /
                  level[0].chapters.length) *
                100,

              detailsArray: newArray,
            });
          });
      });
    } catch (error) {
      console.log(error);
    }
  };

  submitProgress = async () => {
    try {
      Auth.currentAuthenticatedUser().then((dt) => {
        let token = dt
          .getSignInUserSession()
          .getAccessToken()
          .getJwtToken();
        axios
          .get('http://newbee-prod-1105238445.ap-south-1.elb.amazonaws.com/api/course-api/progress', {
            headers: {
              Authorization: `Bearer ${token}`,
              Accept: 'application/json',
              'Content-Type': 'application/json',
            },
          })
          .then((response) => {
            const isExist = response.data._embedded.coursesProgress.find(
              (item) => {
                if (
                  item.courseId === this.state.courseId &&
                  item.courseLevel ===
                    this.props.navigation.state.params.courseLevel
                ) {
                  return item;
                }
                return;
              }
            );

            if (isExist && isExist.progessPercent === 100) {
              return;
            }

            if (isExist) {
              axios.patch(
                `http://newbee-prod-1105238445.ap-south-1.elb.amazonaws.com/api/course-api/progress/${isExist.id}`,
                {
                  userId: this.state.userId,
                  courseId: this.state.courseId,
                  courseLevel: this.props.navigation.state.params.courseLevel,
                  courseStatus:
                    this.state.progressPercent === 100
                      ? 'COMPLETED'
                      : 'IN-PROGRESS',
                  courseDescription: this.state.courseDescription,
                  progessPercent: this.state.progressPercent,
                  chapterNumber: this.props.navigation.state.params
                    .chapterNumber,
                },
                {
                  headers: {
                    Authorization: `Bearer ${token}`,
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                  },
                }
              );
            } else {
              axios.post(
                'http://newbee-prod-1105238445.ap-south-1.elb.amazonaws.com/api/course-api/progress',
                {
                  userId: this.state.userId,
                  courseId: this.state.courseId,
                  courseLevel: this.props.navigation.state.params.courseLevel,
                  courseStatus:
                    this.state.progressPercent === 100
                      ? 'COMPLETED'
                      : 'IN-PROGRESS',
                  courseDescription: this.state.courseDescription,
                  progessPercent: this.state.progressPercent,
                  chapterNumber: this.props.navigation.state.params
                    .chapterNumber,
                },
                {
                  headers: {
                    Authorization: `Bearer ${token}`,
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                  },
                }
              );
            }
          });
      });
    } catch (error) {
      console.log(error);
    }
  };

  onSwipeLeft = (gestureState) => {
    this.setState((prevState) => ({
      currentIndex:
        prevState.currentIndex === this.state.detailsArray.length
          ? prevState.currentIndex
          : prevState.currentIndex + 1,
    }));
  };

  onSwipeRight = (gestureState) => {
    this.setState((prevState) => ({
      currentIndex:
        prevState.currentIndex === 1
          ? prevState.currentIndex
          : prevState.currentIndex - 1,
    }));
  };

  render() {
    const config = {
      velocityThreshold: 0.3,
      directionalOffsetThreshold: 80,
    };

    const style =
      "<style> body { -webkit-touch-callout: none;-webkit-user-select: none;-khtml-user-select: none;-moz-user-select: none;-ms-user-select: none;user-select: none;}</style>";

    return (
      <GestureRecognizer
        onSwipeLeft={(state) => this.onSwipeLeft(state)}
        onSwipeRight={(state) => this.onSwipeRight(state)}
        config={config}
        style={{
          flex: 1,
          backgroundColor: this.state.backgroundColor,
        }}
      >
        <View style={{ flex: 1 }}>
          {this.state.detailsArray &&
            this.state.detailsArray.map(
              (element, index) =>
                this.state.currentIndex === index + 1 && (
                  <WebView
                    automaticallyAdjustContentInsets={true}
                    scrollEnabled={true}
                    pagingEnabled={true}
                    style={{ flex: 1 }}
                    source={{
                      html: element + style,
                    }}
                    javaScriptEnabled={true}
                    domStorageEnabled={true}
                  />
                )
            )}
          {this.state.detailsArray.length > 0 && (
            <View
              style={{
                flexDirection: 'row',
                paddingHorizontal: 5,
                bottom: 20,
                alignSelf: 'center',
              }}
            >
              {this.state.currentIndex !== 1 && (
                <TouchableHighlight
                  onPress={this.onSwipeRight}
                  style={{
                    backgroundColor: '#FAC408',
                    height: 40,
                    width: 40,
                    borderRadius: 40,
                    justifyContent: 'center',
                    alignItems: 'center',
                    position: 'absolute',
                    bottom: 30,
                    left: 20,
                    zIndex: 5,
                  }}
                >
                  <Icon
                    type="antdesign"
                    name="left"
                    size={20}
                    color="#000000"
                  />
                </TouchableHighlight>
              )}
              {this.state.detailsArray.length !== this.state.currentIndex && (
                <TouchableHighlight
                  onPress={this.onSwipeLeft}
                  style={{
                    backgroundColor: '#FAC408',
                    height: 40,
                    width: 40,
                    borderRadius: 40,
                    justifyContent: 'center',
                    alignItems: 'center',
                    position: 'absolute',
                    bottom: 30,
                    right: 20,
                    zIndex: 5,
                  }}
                >
                  <Icon
                    type="antdesign"
                    name="right"
                    size={20}
                    color="#000000"
                  />
                </TouchableHighlight>
              )}

              <FlatList
                data={this.state.detailsArray}
                horizontal
                showsHorizontalScrollIndicator={false}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item, index }) =>
                  this.state.currentIndex === index + 1 ? (
                    <Button
                      title={`${index + 1}`}
                      buttonStyle={{
                        backgroundColor: '#FAC408',
                      }}
                      containerStyle={{
                        borderRadius: 25,
                        width: 25,
                        height: 25,
                        marginHorizontal: 5,
                        justifyContent: 'center',
                        alignSelf: 'center',
                      }}
                      titleStyle={{ fontSize: 7 }}
                      onPress={() => this.setState({ currentIndex: index + 1 })}
                    />
                  ) : (
                    <Button
                      title={`${index + 1}`}
                      buttonStyle={{
                        backgroundColor: '#000000',
                      }}
                      containerStyle={{
                        borderRadius: 25,
                        width: 25,
                        height: 25,
                        marginHorizontal: 5,
                        justifyContent: 'center',
                        alignSelf: 'center',
                      }}
                      titleStyle={{ color: 'white', fontSize: 7 }}
                      onPress={() => this.setState({ currentIndex: index + 1 })}
                    />
                  )
                }
              />
              {this.state.detailsArray.length === this.state.currentIndex && (
                <Button
                  containerStyle={{
                    justifyContent: 'center',
                    position: 'absolute',
                    bottom: this.state.detailsArray.length < 6 ? 0 : 30,
                    right: 10,
                  }}
                  buttonStyle={{
                    width: 100,
                    height: 40,
                    borderRadius: 40,
                    backgroundColor: '#000000',
                  }}
                  title="Done"
                  titleStyle={{
                    fontSize: 14,
                    lineHeight: 17,
                    color: '#FAC408',
                    textAlign: 'center',
                    fontFamily: 'Signika-Regular',
                  }}
                  onPress={() => {
                    this.submitProgress();
                    this.props.navigation.goBack();
                  }}
                />
              )}
            </View>
          )}
        </View>
      </GestureRecognizer>
    );
  }
}

export default CourseDetailsDescription;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  heading: {
    fontFamily: 'Signika-Regular',
    fontSize: 18,
    lineHeight: 22,
  },
  midContainer: {
    flex: 1,
    marginTop: '3%',
    paddingHorizontal: 30,
  },
  paragraph: {
    fontSize: 12,
    lineHeight: 15,
    fontFamily: 'Signika-Regular',
    marginTop: '3%',
  },
  greyContainer: {
    flex: 1,
    marginTop: '5%',
    backgroundColor: '#FAFAFA',
    height: 66,
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  yellowText: {
    fontSize: 12,
    lineHeight: 15,
    color: '#FAC408',
    paddingHorizontal: 20,
    fontFamily: 'Signika-Regular',
  },
  line: {
    flex: 1,

    flexDirection: 'row',
    alignItems: 'center',
  },
  lineText: {
    marginLeft: 10,
    fontSize: 12,
    lineHeight: 15,
    fontFamily: 'Signika-Regular',
    paddingVertical: 5,
  },

  yellowDot: {
    fontSize: 16,
    color: '#FAC408',
  },
  btnContainer: {
    width: 80,
    height: 29,
    borderRadius: 20,
    justifyContent: 'center',
  },
});
