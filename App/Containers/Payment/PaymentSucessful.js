import React from 'react';
import { StyleSheet, Text, View, ScrollView } from 'react-native';
import { Image } from 'react-native-elements';

const PaymentSucessful = () => {
  return (
    <ScrollView contentContainerStyle={styles.container}>
      <Image
        source={require('../../Assets/Images/splash.png')}
        style={{
          width: 300,
          height: 200,
        }}
      />
      <Text style={styles.text}>Payment{'\n'}Successfull!</Text>
      <Text style={styles.yellow}>Continue Reading</Text>
    </ScrollView>
  );
};

export default PaymentSucessful;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: '3%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    marginTop: '5%',
    fontFamily: 'Signika-Regular',
    fontSize: 30,
    lineHeight: 37,
    textAlign: 'center',
  },
  yellow: {
    marginTop: '3%',
    fontFamily: 'Signika-Regular',
    fontSize: 20,
    lineHeight: 25,
    textAlign: 'center',
    color: '#FAC408',
  },
});
