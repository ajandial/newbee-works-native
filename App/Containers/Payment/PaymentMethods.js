import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import { Card, Image, Avatar, Button, Icon } from 'react-native-elements';

class PaymentMethods extends Component {
  constructor(props) {
    super(props);
    this.state = {
      paymentOptions: [
        { payementType: 'PayPal', selected: false },
        { payementType: 'Credit', selected: true },
        { payementType: 'Wallet', selected: false },
        { payementType: 'Gpay', selected: false },
      ],
    };
  }
  renderPaymentOptions = (item) => {
    return item.selected === true ? (
      <Card
        containerStyle={{
          marginHorizontal: 0,
          borderRadius: 5,
          borderWidth: 0,
          backgroundColor: '#FCB61A',
          elevation: 6,
          shadowColor: 'black',
          shadowOpacity: 0.26,
          shadowOffset: { width: 0, height: 2 },
          shadowRadius: 3,
          overflow: 'hidden',
        }}
      >
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}
        >
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <Image
              source={require('../../Assets/Images/in.png')}
              style={{ width: 30, height: 30 }}
            />
            <Text style={[styles.paymentTypeText, { color: 'white' }]}>
              {item.payementType}
            </Text>
          </View>
          <Avatar
            containerStyle={{
              backgroundColor: '#FAC408',
              elevation: 6,
              shadowColor: 'black',
              shadowOpacity: 0.26,
              shadowOffset: { width: 0, height: 2 },
              shadowRadius: 3,
              overflow: 'hidden',
            }}
            rounded
            icon={{ type: 'antdesign', name: 'check', color: 'white' }}
          />
        </View>
      </Card>
    ) : (
      <Card
        containerStyle={{
          marginHorizontal: 0,
          borderRadius: 5,
          backgroundColor: '#FAFAFA',
          borderWidth: 0,
        }}
      >
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}
        >
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <Image
              source={require('../../Assets/Images/in.png')}
              style={{ width: 30, height: 30 }}
            />
            <Text style={styles.paymentTypeText}>{item.payementType}</Text>
          </View>
          <Avatar
            containerStyle={{
              backgroundColor: 'white',
            }}
            rounded
            icon={{
              type: 'antdesign',
              name: 'check',
              color: 'rgba(153, 153, 153, 0.2)',
            }}
          />
        </View>
      </Card>
    );
  };
  render() {
    return (
      <View style={styles.screen}>
        <View
          style={{
            height: '5%',
            justifyContent: 'space-between',
            flexDirection: 'row',
            alignItems: 'center',
            padding: 10,
          }}
        >
          <TouchableOpacity onPress={() => this.props.navigation.openDrawer()}>
            <Icon name="menu" color="#FCB61A" size={30} />
          </TouchableOpacity>
          <Text
            style={{
              fontFamily: 'Signika-Regular',
              fontSize: 16,
              lineHeight: 20,
              textAlign: 'center',
            }}
          >
            Subscription
          </Text>
          <View style={{ flexDirection: 'row' }}>
            <TouchableOpacity>
              <Icon name="search" color="#FCB61A" size={20} />
            </TouchableOpacity>
          </View>
        </View>
        <ScrollView
          style={styles.container}
          contentContainerStyle={{ flex: 1 }}
        >
          <View style={{ marginTop: '10%', paddingHorizontal: 20 }}>
            <Card
              containerStyle={{
                marginHorizontal: 0,
                borderRadius: 13,
                elevation: 5,
                borderWidth: 0,
                shadowColor: 'black',
                shadowOpacity: 0.26,
                shadowOffset: { width: 0, height: 2 },
                shadowRadius: 3,
                marginBottom: '2%',
                height: 106,
                justifyContent: 'center',
                alignItems: 'center',
              }}
            >
              <Text style={styles.amountText}>Total Amount</Text>
              <Text style={styles.yellowText}>$ 100</Text>
            </Card>
            <View style={{ marginTop: '6%', paddingBottom: '3%' }}>
              <Text style={styles.heading}>Select your payment method</Text>
              {this.state.paymentOptions.map((item) =>
                this.renderPaymentOptions(item)
              )}
            </View>
          </View>
          <View
            style={{
              paddingHorizontal: 20,
              justifyContent: 'flex-end',
              flex: 1,
              marginBottom: 10,
            }}
          >
            <Button
              containerStyle={{
                paddingTop: 10,
                width: '100%',
              }}
              buttonStyle={{
                width: '100%',
                backgroundColor: 'black',
              }}
              titleStyle={{
                fontFamily: 'Signika-Regular',
                fontSize: 20,
                color: '#FCB61A',
              }}
              title="Proceed To Confirm"
              onPress={() => this.props.navigation.navigate('ConfirmPayment')}
            />
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
  },
  container: {
    flex: 1,
  },
  amountText: {
    fontSize: 14,
    lineHeight: 17,
    fontFamily: 'Signika-Regular',
    textAlign: 'center',
  },
  yellowText: {
    marginTop: '3%',
    fontFamily: 'Signika-Regular',
    fontSize: 24,
    lineHeight: 30,
    fontWeight: 'bold',
    color: '#FCB61A',
    textAlign: 'center',
  },
  heading: {
    fontSize: 14,
    lineHeight: 17,
    fontFamily: 'Signika-Regular',
  },
  paymentTypeText: {
    fontSize: 12,
    lineHeight: 18,
    color: '#999999',
    fontFamily: 'Signika-Regular',
    marginLeft: '10%',
  },
});

export default PaymentMethods;
