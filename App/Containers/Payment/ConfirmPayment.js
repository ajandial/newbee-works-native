import React, { useState, Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TextInput,
  Switch,
  TouchableOpacity,
} from 'react-native';
import { Card, Button, Icon } from 'react-native-elements';

class ConfirmPayment extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cardInput: '',
      month: 'Month',
      year: 'Year',
      cvv: '',
      cardHolderName: '',
      isEnabled: false,
      showDatePicker: false,
      paymentSuccessFull: true,
    };
  }

  showPicker = (value) => this.setState({ showDatePicker: value });

  toggleSwitch = () =>
    this.setState((previousState) => ({ isEnabled: !previousState.value }));
  render() {
    return (
      <ScrollView style={{ flex: 1 }} contentContainerStyle={{ flex: 1 }}>
        <View
          style={{
            height: '5%',
            justifyContent: 'space-between',
            flexDirection: 'row',
            alignItems: 'center',
            padding: 10,
          }}
        >
          <TouchableOpacity onPress={() => this.props.navigation.openDrawer()}>
            <Icon name="menu" color="#FCB61A" size={30} />
          </TouchableOpacity>
          <Text
            style={{
              fontFamily: 'Signika-Regular',
              fontSize: 16,
              lineHeight: 20,
              textAlign: 'center',
            }}
          >
            Confirm Payment
          </Text>
          <View style={{ flexDirection: 'row' }}>
            <TouchableOpacity>
              <Icon name="search" color="#FCB61A" size={20} />
            </TouchableOpacity>
          </View>
        </View>
        <View
          style={{
            flex: 1,
            padding: 20,
          }}
        >
          <Text style={styles.confirmtText}>Confirm Your Details</Text>
          <Card
            containerStyle={{
              borderRadius: 13,
              elevation: 6,
              shadowColor: 'black',
              shadowOpacity: 0.26,
              shadowOffset: { width: 0, height: 2 },
              shadowRadius: 3,
              overflow: 'hidden',
              marginBottom: '2%',
              alignSelf: 'center',
              width: '100%',
              height: 263,
              borderWidth: 0,
            }}
          >
            <Text style={styles.cardText}>Card number</Text>
            <TextInput
              value={this.state.cardInput}
              style={styles.input}
              onChangeText={(value) => this.setState({ cardInput: value })}
              placeholder="**** **** **** ****"
              placeholderTextColor="#000000"
            />
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
              }}
            >
              <View>
                <Text style={styles.cardText}>Valid until</Text>
                <TextInput
                  value={this.state.month}
                  style={styles.input}
                  onChangeText={(value) => this.setState({ month: value })}
                  placeholder="21/2021"
                  placeholderTextColor="#000000"
                />
              </View>
              <View>
                <Text style={styles.cardText}>CVV</Text>
                <TextInput
                  value={this.state.cvv}
                  style={styles.input}
                  onChangeText={(value) => this.setState({ cvv: value })}
                  placeholder="* * * *"
                  placeholderTextColor="#000000"
                />
              </View>
            </View>

            <View>
              <View style={styles.validContainer}>
                <Text style={styles.cardText}>Card Holder</Text>
                <TextInput
                  value={this.state.cardHolderName}
                  style={styles.input}
                  onChangeText={(value) =>
                    this.setState({ cardHolderName: value })
                  }
                  placeholder="Your name and surname"
                />
              </View>
            </View>
          </Card>
          <Card
            containerStyle={{
              borderRadius: 13,
              elevation: 6,
              borderWidth: 0,
              shadowColor: 'black',
              shadowOpacity: 0.26,
              shadowOffset: { width: 0, height: 2 },
              shadowRadius: 3,
              overflow: 'hidden',
              marginBottom: '2%',
              alignSelf: 'center',
              width: '100%',
              height: 60,
            }}
          >
            <View style={styles.saveCard}>
              <Text style={styles.saveCardText}>
                Save card data for future payments
              </Text>
              <Switch
                trackColor={{ false: '#E5E5E5', true: '#E5E5E5' }}
                thumbColor={this.state.isEnabled ? '#FCB61A' : '#f4f3f4'}
                ios_backgroundColor="#E5E5E5"
                onValueChange={this.toggleSwitch}
                value={this.state.isEnabled}
              />
            </View>
          </Card>
        </View>
        <View
          style={{
            flex: 1,
            justifyContent: 'flex-end',
            paddingHorizontal: 20,
          }}
        >
          <Text
            style={{
              fontSize: 20,
              lineHeight: 25,
              textAlign: 'center',
              color: '#FCB61A',
              fontFamily: 'Signika-Regular',
              paddingHorizontal: 50,
            }}
          >
            This subscription will expire after 1 month
          </Text>
          <Button
            containerStyle={{
              paddingTop: 10,
              width: '100%',
            }}
            buttonStyle={{
              width: '100%',
              backgroundColor: 'black',
            }}
            titleStyle={{
              fontFamily: 'Signika-Regular',
              fontSize: 20,
              color: '#FCB61A',
            }}
            title="Confirm"
            onPress={() =>
              this.state.paymentSuccessFull
                ? this.props.navigation.navigate('PaymentSucessful')
                : this.props.navigation.navigate('PaymentUnSucessful')
            }
          />
          <Text
            style={{
              fontFamily: 'Signika-Regular',
              fontSize: 16,
              alignSelf: 'center',
              paddingTop: 20,
            }}
          />
        </View>
      </ScrollView>
    );
  }
}

export default ConfirmPayment;

const styles = StyleSheet.create({
  screen: {
    flex: 1,
  },
  confirmtText: {
    fontSize: 18,
    lineHeight: 22,
    fontFamily: 'Signika-Regular',
  },
  cardText: {
    marginTop: 10,
    fontSize: 10,
    lineHeight: 12,
    color: '#555555',
    fontFamily: 'Signika-Regular',
  },
  input: {
    marginTop: '3%',
    fontFamily: 'Signika-Regular',
    fontSize: 18,
    lineHeight: 22,
    color: '#000000',
    backgroundColor: '#FFFFFF',
    borderRadius: 5,
    elevation: 2,
    padding: 10,
  },
  saveCard: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  saveCardText: {
    fontSize: 14,
    lineHeight: 17,
    fontFamily: 'Signika-Regular',
  },
});
