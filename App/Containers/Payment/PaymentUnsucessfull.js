import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView } from 'react-native';
import { Image } from 'react-native-elements';

class PaymentUnSucessful extends Component {
  render() {
    return (
      <ScrollView contentContainerStyle={styles.container}>
        <Image
          source={require('../../Assets/Images/splash.png')}
          style={{
            width: 300,
            height: 200,
          }}
        />
        <Text style={styles.text}>Sorry,{'\n'}Something went wrong</Text>
        <Text
          style={styles.yellow}
          onPress={() => this.props.navigation.goBack()}
        >
          Retry
        </Text>
      </ScrollView>
    );
  }
}

export default PaymentUnSucessful;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: '3%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    marginTop: '5%',
    fontFamily: 'Signika-Regular',
    fontSize: 30,
    lineHeight: 37,
    textAlign: 'center',
  },
  yellow: {
    marginTop: '5%',
    fontFamily: 'Signika-Regular',
    fontSize: 20,
    lineHeight: 25,
    textAlign: 'center',
    color: '#FAC408',
  },
});
