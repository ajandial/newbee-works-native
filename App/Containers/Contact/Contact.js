import React, { Component } from "react";
import { View, Text, StyleSheet, TouchableOpacity, Image } from "react-native";
import { Icon } from "react-native-elements";
import logo from "../../Assets/Images/logo.png";
import contents from "../contents.json";
import { ScrollView } from "react-native-gesture-handler";

class Dashboard extends Component {
  static navigationOptions = ({ navigation }) => ({
    drawerLabel: "Contact Us",
    drawerIcon: ({ tintColor }) => (
      <Icon
        name="call"
        type="material-icon"
        color={tintColor}
        style={{ fontSize: 24, opacity: 1 }}
      />
    )
  });
  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
          <Icon
            type="font-awesome"
            name="arrow-left"
            color="#FCB61A"
            style={{ alignSelf: "flex-start" }}
          />
        </TouchableOpacity>
        <ScrollView style={{ flex: 1}}>
          <View
            style={{
              flex: 1,
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <Image
              style={{
                flex: 1,
                justifyContent: "center",
                alignItems: "center",
                resizeMode: "contain",
                width: 200,
                height: 100
              }}
              source={logo}
            />
          </View>
          <View style={{ width: "90%", paddingLeft: "5%", paddingTop:'40%' }}>
            <Text
              style={{
                fontFamily: "Signika-Regular",
                fontWeight: "600",
                fontSize: 30,
                lineHeight: 37,
                color: "#000000"
              }}
            >
              {contents.Contact_us}
            </Text>
          </View>

          <View style={{ width: "90%", paddingTop: "20%", paddingLeft: "5%" }}>
            <Text
              style={{
                fontFamily: "Signika-Regular",
                fontSize: 20,
                lineHeight: 25,
                color: "#000000"
              }}
            >
              {contents.email}
              {"\n\n"}
              {contents.mobileno}
            </Text>
          </View>
        </ScrollView>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20
  }
});
export default Dashboard;
