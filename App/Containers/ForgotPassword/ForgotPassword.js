import React from "react";
import {
  StyleSheet,
  View,
  ScrollView,
  Text,
  Image,
  TouchableHighlight,
  ActivityIndicator
} from "react-native";
import logo from "../../Assets/Images/logo.png";
import fbLogo from "../../Assets/Images/fb.png";
import inLogo from "../../Assets/Images/in.png";
import gpLogo from "../../Assets/Images/gp.png";
import { Input, Button } from "react-native-elements";
import { Icon } from "react-native-elements";
import { Auth } from "aws-amplify";
import { TouchableOpacity } from "react-native-gesture-handler";

export default class ForgotPassword extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      password: "",
      username: "",
      isLoading: false,
      isError: false,
      errorMessage: ""
    };
  }

  validateEmail = () => {
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (!this.state.email) {
      this.setState({ emailError: "Email is Required" });
      return true;
    } else if (reg.test(this.state.email) === false) {
      this.setState({ emailError: "Email is Invalid" });
      return true;
    } else {
      this.setState({ emailError: null });
      return false;
    }
  };

  handleForgotPress = async () => {
    const { email } = this.state;
    if (this.validateEmail()) {
      return;
    }
    try {
      this.setState({
        isLoading: true
      });
      const user = await Auth.forgotPassword(email);
      if (user) {
        this.setState({
          isLoading: false,
          isError: false,
          errorMessage: ""
        });
        this.props.navigation.navigate("ResetPassword", {
          email: this.state.email
        });
      }
    } catch (err) {
      this.setState({
        isLoading: false,
        isError: true,
        errorMessage: err.message
      });
      console.log("error:", err.message);
    }
  };

  onChangeText = (key, val) => {
    this.setState({ [key]: val });
  };

  render() {
    const { isError, errorMessage, isLoading } = this.state;
    return isLoading ? (
      <ScrollView
        style={{
          flex: 1,
          paddingTop: "50%"
        }}
      >
        <ActivityIndicator
          style={{ justifyContent: "center", alignItems: "center" }}
          size="large"
          color="#0000ff"
        />
      </ScrollView>
    ) : (
      <ScrollView
        style={{
          flex: 1,
          paddingTop: "10%"
        }}
      >
        <View
          style={{
            flex: 1,
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <Image
            style={{
              flex: 1,
              justifyContent: "center",
              alignItems: "center",
              resizeMode: "contain",
              width: 200,
              height: 100
            }}
            source={logo}
          />
        </View>
        <View
          style={{
            paddingTop: "10%",
            flex: 1,
            width: "90%",
            justifyContent: "flex-start",
            alignItems: "flex-start",
            paddingLeft: "10%"
          }}
        >
          <Text
            style={{
              fontFamily: "Signika-Regular",
              fontWeight: "600",
              fontSize: 24,
              lineHeight: 27,
              color: "#FCB61A"
            }}
          >
            Forgot Password
          </Text>
          <View
            style={{
              paddingTop: 5,
              width: "10%",
              alignSelf: "stretch",
              borderBottomColor: "black",
              borderBottomWidth: 2
            }}
          />
          <Text
            style={{
              fontFamily: "Signika-Regular",
              paddingTop: 20,
              color: "#555555",
              fontSize: 14
            }}
          >
            Enter your email address
          </Text>
          <Input
            inputStyle={{
              fontSize: 14,
              lineHeight: 17,
              paddingLeft: 10
            }}
            containerStyle={{
              paddingTop: 5,
              backgroundColor: "#FFFFFF",
              borderRadius: 5
            }}
            onChangeText={val => this.onChangeText("email", val)}
            placeholder="John Doe"
            leftIcon={
              <Icon
                name="envelope"
                type="font-awesome"
                size={24}
                color="black"
              />
            }
            onEndEditing={() => this.validateEmail()}
          />
          {this.state.emailError ? (
            <Text style={styles.errorText}>{this.state.emailError}</Text>
          ) : null}
          {isError ? (
            <Text style={{ fontFamily: "Signika-Regular", color: "red" }}>
              {errorMessage}
            </Text>
          ) : (
            <Text />
          )}
          <Button
            containerStyle={{
              paddingTop: 10,
              width: "100%"
            }}
            buttonStyle={{
              width: "100%",
              backgroundColor: "black"
            }}
            titleStyle={{
              fontFamily: "Signika-Regular",
              fontSize: 20,
              color: "#FCB61A"
            }}
            onPress={this.handleForgotPress}
            title="Send Email"
          />
          <TouchableHighlight
            underlayColor="#FFFFFF"
            style={{ alignSelf: "center" }}
            onPress={() => this.props.navigation.navigate("LoginScreen")}
          >
            <Text
              style={{
                fontFamily: "Signika-Regular",
                fontSize: 16,

                paddingTop: 20
              }}
            >
              Password Remembered ?{" "}
              <Text style={{ color: "#FCB61A" }}>Log in </Text>
            </Text>
          </TouchableHighlight>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  image: {
    width: "100%",
    height: "100%"
  },
  text: {
    fontSize: 18,
    color: "black",
    textAlign: "center",
    paddingTop: "10%"
  },
  title: {
    fontSize: 25,
    color: "white",
    textAlign: "center"
  }
});
