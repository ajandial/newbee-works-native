import React from 'react'
import { StyleSheet, View, ScrollView, Text, Image, TouchableHighlight } from 'react-native'
import logo from '../../Assets/Images/logo.png'
import fbLogo from '../../Assets/Images/fb.png'
import inLogo from '../../Assets/Images/in.png'
import gpLogo from '../../Assets/Images/gp.png'
import { Input, Button } from 'react-native-elements'
import { Icon } from 'react-native-elements'
import { Auth } from 'aws-amplify'
import { TouchableOpacity } from 'react-native-gesture-handler'

export default class ResetPassword extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      code: '',
      password: '',
    }
  }

  onChangeText = (key, val) => {
    this.setState({ [key]: val })
  }

  ResendCode = async () => {
    const { navigation } = this.props
    const email = navigation.getParam('email')
    try {
      await await Auth.forgotPassword(email)
      console.log('successully sent code again !')
    } catch (err) {
      console.log('error confirming signing up: ', err)
    }
  }

  ResetPassword = async () => {
    const { navigation } = this.props
    const { code, password } = this.state
    const email = navigation.getParam('email')
    try {
      await Auth.forgotPasswordSubmit(email, code, password)
      console.log('successully resetted password up!')
      this.props.navigation.navigate('Customer')
    } catch (err) {
      console.log('error confirming signing up: ', err)
    }
  }

  render() {
    return (
      <ScrollView
        style={{
          flex: 1,
          paddingTop: '10%',
        }}
      >
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <Image
            style={{
              flex: 1,
              height: 100,
              width: '50%',
              resizeMode: 'contain',
            }}
            source={logo}
          />
        </View>
        <View
          style={{
            paddingTop: '10%',
            flex: 1,
            width: '90%',
            justifyContent: 'flex-start',
            alignItems: 'flex-start',
            paddingLeft: '10%',
          }}
        >
          <Text
            style={{
              fontFamily: 'Signika-Regular',
              fontWeight: '600',
              fontSize: 22,
              lineHeight: 27,
              color: '#FCB61A',
            }}
          >
            Change Password
          </Text>
          <View
            style={{
              paddingTop: 5,
              width: '10%',
              alignSelf: 'stretch',
              borderBottomColor: 'black',
              borderBottomWidth: 2,
            }}
          />
          <Text
            style={{
              fontFamily: 'Signika-Regular',
              paddingTop: 20,
            }}
          >
            Code has been sent to your mobile number. Please enter your code
          </Text>
          <Text
            style={{
              fontFamily: 'Signika-Regular',
              paddingTop: 20,
              color: '#555555',
              fontSize: 10,
            }}
          >
            Enter your code
          </Text>
          <Input
            inputStyle={{
              fontSize: 14,
              lineHeight: 17,
              paddingLeft: 10,
            }}
            onChangeText={(val) => this.onChangeText('code', val)}
            containerStyle={{
              paddingTop: 5,
              backgroundColor: '#FFFFFF',
              borderRadius: 5,
            }}
            placeholder="1234"
          />

          <Text
            style={{
              fontFamily: 'Signika-Regular',
              paddingTop: 20,
              color: '#555555',
              fontSize: 10,
            }}
          >
            Enter your email
          </Text>
          <Input
            inputStyle={{
              fontSize: 14,
              lineHeight: 17,
              paddingLeft: 10,
            }}
            disabled
            value={this.props.navigation.getParam('email')}
            containerStyle={{
              paddingTop: 5,
              backgroundColor: '#FFFFFF',
              borderRadius: 5,
            }}
            placeholder="1234"
          />

          <Text
            style={{
              fontFamily: 'Signika-Regular',
              paddingTop: 20,
              color: '#555555',
              fontSize: 10,
            }}
          >
            Enter New Password
          </Text>
          <Input
            inputStyle={{
              fontSize: 14,
              lineHeight: 17,
              paddingLeft: 10,
            }}
            onChangeText={(val) => this.onChangeText('password', val)}
            containerStyle={{
              paddingTop: 5,
              backgroundColor: '#FFFFFF',
              borderRadius: 5,
            }}
            placeholder="*******"
            secureTextEntry
          />

          <Button
            
            containerStyle={{
              paddingTop: 10,
              width: '100%',
            }}
            buttonStyle={{
              width: '100%',
              backgroundColor: 'black',
            }}
            titleStyle={{
              fontFamily: 'Signika-Regular',
              fontSize: 20,
              color: '#FCB61A',
            }} 
            onPress={this.ResetPassword}
            title="Submit"
          />
          <TouchableHighlight
            underlayColor="#FFFFFF"
            style={{ alignSelf: 'center' }}
            onPress={this.ResendCode}
          >
            <Text
              style={{
                fontFamily: 'Signika-Regular',
                fontSize: 14,

                paddingTop: 20,
              }}
            >
              Didn't received yet ? <Text style={{ color: '#FCB61A' }}>Resend</Text>
            </Text>
          </TouchableHighlight>
        </View>
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  image: {
    width: '100%',
    height: '100%',
  },
  text: {
    fontSize: 18,
    color: 'black',
    textAlign: 'center',
    paddingTop: '10%',
  },
  title: {
    fontSize: 25,
    color: 'white',
    textAlign: 'center',
  },
})
