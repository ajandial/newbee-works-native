import React from "react";
import {
  StyleSheet,
  View,
  ScrollView,
  ActivityIndicator,
  Text,
  Image,
  TouchableHighlight,
  TouchableWithoutFeedback
} from "react-native";
import logo from "../../Assets/Images/logo.png";
import fbLogo from "../../Assets/Images/fb.png";
import inLogo from "../../Assets/Images/in.png";
import gpLogo from "../../Assets/Images/gp.png";
import contents from "../contents.json";
import { Input, Button } from "react-native-elements";
import { Icon } from "react-native-elements";
import { Auth } from "aws-amplify";
import { TouchableOpacity } from "react-native-gesture-handler";

export default class LoginScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      password: "",
      username: "",
      isLoading: false,
      isError: false,
      errorMessage: ""
    };
  }

  validateEmail = () => {
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(this.state.username) === false || !this.state.username) {
      this.setState({ emailError: "Email is Invalid" });
      return true;
    } else {
      this.setState({ emailError: null });
      return false;
    }
  };

  validatePassword = () => {
    if (this.state.password.length < 6) {
      this.setState({ passwordError: "Password is Invalid" });
      return true;
    } else {
      this.setState({ passwordError: null });
      return false;
    }
  };

  handleLoginPress = async () => {
    if (this.validateEmail() || this.validatePassword()) {
      return;
    }
    const { username, password } = this.state;
    try {
      this.setState({
        isLoading: true
      });
      const user = await Auth.signIn(username, password);
      if (user) {
        this.setState({
          isLoading: false,
          isError: false,
          errorMessage: ""
        });
        const profile = user.attributes.profile;
        this.props.navigation.navigate(
          profile === "Customer" ? "Customer" : ""
        );
      }

      console.log("user successfully signed in!", user);
      console.log("user successfully signed in!", Auth.userAttributes(user));
    } catch (err) {
      this.setState({
        isLoading: false,
        isError: true,
        errorMessage: err.message
      });
      console.log("error:", err.message);
    }
  };

  onChangeText = (key, val) => {
    this.setState({ [key]: val });
  };

  render() {
    const { isError, errorMessage, isLoading } = this.state;
    return (
      <ScrollView
        style={{
          flex: 1,
          paddingTop: "10%"
        }}
      >
        <View
          style={{
            flex: 1,
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <Image
            style={{
              flex: 1,
              justifyContent: "center",
              alignItems: "center",
              resizeMode: "contain",
              width: 200,
              height: 100
            }}
            source={logo}
          />
        </View>
        <View
          style={{
            paddingTop: "10%",
            flex: 1,
            width: "90%",
            justifyContent: "flex-start",
            alignItems: "flex-start",
            paddingLeft: "10%"
          }}
        >
          <Text
            style={{
              fontFamily: "Signika-Regular",
              fontWeight: "600",
              fontSize: 22,
              lineHeight: 27,
              color: "#FCB61A"
            }}
          >
            Sign In
          </Text>
          <View
            style={{
              paddingTop: 5,
              width: "10%",
              alignSelf: "stretch",
              borderBottomColor: "black",
              borderBottomWidth: 2
            }}
          />
          <Text
            style={{
              fontFamily: "Signika-Regular",
              paddingTop: 20,
              color: "#555555",
              fontSize: 14
            }}
          >
            Email
          </Text>
          <Input
            inputStyle={{
              fontSize: 14,
              lineHeight: 17,
              paddingLeft: 10
            }}
            containerStyle={{
              paddingTop: 5,
              backgroundColor: "#FFFFFF",
              borderRadius: 5
            }}
            placeholder="John Doe"
            onChangeText={val => this.onChangeText("username", val)}
            leftIcon={
              <Icon name="user" type="font-awesome" size={24} color="black" />
            }
            onEndEditing={() => this.validateEmail()}
          />
          {this.state.emailError ? (
            <Text style={styles.errorText}>{this.state.emailError}</Text>
          ) : null}
          <Text
            style={{
              fontFamily: "Signika-Regular",
              paddingTop: 5,
              color: "#555555",
              fontSize: 14
            }}
          >
            Password
          </Text>
          <Input
            inputStyle={{
              fontSize: 14,
              lineHeight: 17,
              paddingLeft: 10
            }}
            containerStyle={{
              paddingTop: 5,
              backgroundColor: "#FFFFFF",
              borderRadius: 5
            }}
            placeholder="********"
            onChangeText={val => this.onChangeText("password", val)}
            secureTextEntry={true}
            leftIcon={
              <Icon name="lock" type="font-awesome" size={24} color="black" />
            }
            onEndEditing={() => this.validatePassword()}
          />

          {this.state.passwordError ? (
            <Text style={styles.errorText}>{this.state.passwordError}</Text>
          ) : null}

          <TouchableHighlight
            underlayColor="#FFFFFF"
            style={{
              alignSelf: "flex-end",
              justifyContent: "flex-end",
              color: "#FCB61A",

              paddingBottom: 10
            }}
            onPress={() => this.props.navigation.navigate("ForgotPassword")}
          >
            <Text
              style={{
                fontSize: 16,
                fontFamily: "Signika-Regular",
                color: "#FCB61A"
              }}
            >
              Forgot Password ?
            </Text>
          </TouchableHighlight>
          {isError ? (
            <Text style={{ fontFamily: "Signika-Regular", color: "red" }}>
              {errorMessage}
            </Text>
          ) : (
            <Text />
          )}
          <Button
            containerStyle={{
              paddingTop: 10,
              width: "100%"
            }}
            buttonStyle={{
              width: "100%",
              backgroundColor: "black"
            }}
            titleStyle={{
              fontFamily: "Signika-Regular",
              fontSize: 20,
              color: "#FCB61A"
            }}
            loading={this.state.isLoading}
            onPress={() => this.handleLoginPress()}
            title={contents.login}
          />
          {/* <Text
            style={{
              fontFamily: 'Signika-Regular',
              fontSize: 16,
              alignSelf: 'center',
              paddingTop: 20,
            }}
          >
            -OR-
          </Text>
          <View
            style={{
              flexDirection: 'row',
              alignSelf: 'center',
              marginTop: 10,
            }}
          >
            <Image style={{ marginRight: '5%' }} source={fbLogo} />
            <Image style={{ marginRight: '5%' }} source={fbLogo} />
            <Image source={fbLogo} />
          </View>
           */}
          <TouchableHighlight
            style={{ alignSelf: "center" }}
            underlayColor="#FFFFFF"
            onPress={() => this.props.navigation.navigate("SignUp")}
          >
            <Text
              style={{
                fontFamily: "Signika-Regular",
                fontSize: 14,

                paddingTop: 20
              }}
            >
              Don't have an account yet ?{" "}
              <Text style={{ color: "#FCB61A" }}>Create an account </Text>
            </Text>
          </TouchableHighlight>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  image: {
    width: "100%",
    height: "100%"
  },
  text: {
    fontSize: 18,
    color: "black",
    textAlign: "center",
    paddingTop: "10%"
  },
  title: {
    fontSize: 25,
    color: "white",
    textAlign: "center"
  },
  errorText: {
    fontSize: 12,

    fontFamily: "Signika-Regular",
    textTransform: "capitalize",
    marginBottom: 10,
    marginLeft: 10,
    color: "red"
  }
});
