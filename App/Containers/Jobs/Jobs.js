import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  StyleSheet,
  ScrollView,
  TouchableOpacity
} from "react-native";
import { Card, ListItem, Button, Icon, Header } from "react-native-elements";
import empty from "../../Assets/Images/empty.png";

class Notification extends Component {
  static navigationOptions = ({ navigation }) => ({
    drawerLabel: "Jobs",
    drawerIcon: ({ tintColor }) => (
      <Icon
        name="notifications"
        color={tintColor}
        type="material-icons"
        style={{ fontSize: 24, opacity: 1 }}
      />
    )
  });
  render() {
    return (
      <View
        style={{
          flex: 1
        }}
      >
        <View
          style={{
            height: "5%",
            justifyContent: "space-between",
            flexDirection: "row",
            alignItems: "center",
            padding: 10
          }}
        >
          <TouchableOpacity onPress={() => this.props.navigation.openDrawer()}>
            <Icon name="menu" color="#FCB61A" size={30} />
          </TouchableOpacity>
          <Text
            style={{
              fontFamily: "Signika-Regular",
              fontSize: 16,
              lineHeight: 20,
              textAlign: "center"
            }}
          >
            Jobs
          </Text>
          <View style={{ flexDirection: "row" }}>
            <TouchableOpacity>
              <Icon name="search" color="#FCB61A" size={20} />
            </TouchableOpacity>
          </View>
        </View>
        <ScrollView style={{ flex: 1, paddingTop: '40%' }}>
          <View
            style={{
              flex: 1,
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <Image
              style={{
                flex: 1,
                justifyContent: "center",
                alignItems: "center",
                resizeMode: "contain",
                width: 300,
                height: 200
              }}
              source={empty}
            />
          </View>
          <Text
              style={{
                fontFamily: "Signika-Regular",
                fontWeight: "600",
                fontSize: 24,
                padding:'3%',
                textAlign: 'center',
                lineHeight: 37,
                color: "#000000"
              }}
            >
              We are baking something awesome and will be back soon
            </Text>
        </ScrollView>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  notification: {
    borderRadius: 10
  }
});
export default Notification;
