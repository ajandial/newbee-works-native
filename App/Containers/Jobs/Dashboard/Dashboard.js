import React, { Component } from 'react'
import { View, Text, StyleSheet } from 'react-native'
import { Header, Icon } from 'react-native-elements'

class Dashboard extends Component {
  static navigationOptions = ({ navigation }) => ({
    drawerLabel: 'Dashboard',
    drawerIcon: ({ tintColor }) => (
      <Icon name="tachometer" color={tintColor} type="font-awesome" style={{ fontSize: 24, opacity: 1 }} />
    ),
  })
  render() {
    return (
      <View style={styles.container}>
        <Header
          leftComponent={<Icon name="menu" onPress={() => this.props.navigation.openDrawer()} />}
        />
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', color: 'black' }}>
          <Text>Dashboard</Text>
        </View>
      </View>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
})
export default Dashboard
