import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  SafeAreaView,
} from 'react-native';
import { Header, Icon } from 'react-native-elements';

class Dashboard extends Component {
  static navigationOptions = ({ navigation }) => ({
    drawerLabel: 'Jobs',
    drawerIcon: ({ tintColor }) => (
      <Icon
        name="briefcase"
        color={tintColor}
        type="font-awesome"
        style={{ fontSize: 24, opacity: 1 }}
      />
    ),
  });
  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <View style={styles.container}>
          <View
            style={{
              height: '5%',
              justifyContent: 'space-between',
              flexDirection: 'row',
              alignItems: 'center',
              padding: 10,
            }}
          >
            <TouchableOpacity
              onPress={() => this.props.navigation.openDrawer()}
            >
              <Icon name="menu" color="#FCB61A" size={30} />
            </TouchableOpacity>
            <Text
              style={{
                fontFamily: 'Signika-Regular',
                fontSize: 16,
                lineHeight: 20,
                textAlign: 'center',
              }}
            >
              Privacy Policy
            </Text>
            <View style={{}} />
          </View>
          <ScrollView
            style={{
              flex: 1,
              marginTop: 20,
              marginHorizontal: 15,
            }}
          >
            <Text
              style={{
                fontSize: 17,
                lineHeight: 21,
                fontFamily: 'Signika-Regular',
                textAlign: 'center',
                marginBottom: 15,
                textDecorationLine: 'underline',
              }}
            >
              Website or Mobile App Privacy & Refund Policy
            </Text>
            <Text
              style={{
                fontSize: 20,
                lineHeight: 23,
                fontFamily: 'Signika-Bold',

                marginTop: 10,
              }}
            >
              Terms & Conditions
            </Text>
            <Text
              style={{
                fontSize: 15,
                lineHeight: 21,
                fontFamily: 'Signika-Regular',
                marginVertical: 10,
              }}
            >
              Welcome to newbee.works. Newbee.works is owned and managed by
              Newbee Works Private Limited (herein after referred to as
              ‘’company’’). If you continue to browse and use this website you
              are agreeing to comply with and be bound by the following terms
              and conditions of use, which together with our privacy policy
              govern relationship with you in relation to this website.
            </Text>

            <Text
              style={{
                fontSize: 15,
                lineHeight: 21,
                fontFamily: 'Signika-Regular',
                marginVertical: 10,
              }}
            >
              The term ‘Newbee Works’ or 'us' or 'we' refers to the owner of the
              website whose registered office is  KATTITHARA HOUSE SY115/4
              Door#39/2475B1 SUITE#95 LR TOWERS SJRRA104 S JANATHA RD
              PALARIVATTOM KOCHI Ernakulam KL 682025 IN Our company registration
              number is U72200KL2020PTC062012, India. The term 'you' refers to
              the user or viewer of our website.
            </Text>
            <Text
              style={{
                fontSize: 15,
                lineHeight: 21,
                fontFamily: 'Signika-Regular',
                marginVertical: 10,
              }}
            >
              The use of this website/ Mobile App is subject to the following
              terms of use:
            </Text>
            <Text
              style={{
                fontSize: 15,
                lineHeight: 21,
                fontFamily: 'Signika-Regular',
                marginVertical: 10,
              }}
            >
              • The content of the pages of this website is for your general
              information and use only. It is subject to change without notice.
            </Text>
            <Text
              style={{
                fontSize: 15,
                lineHeight: 21,
                fontFamily: 'Signika-Regular',
                marginVertical: 10,
              }}
            >
              • Neither we nor any third parties provide any warranty or
              guarantee as to the accuracy, timeliness, performance,
              completeness or suitability of the information and materials found
              or offered on this website for any particular purpose. You
              acknowledge that such information and materials may contain
              inaccuracies or errors and we expressly exclude liability for any
              such inaccuracies or errors to the fullest extent permitted by
              law.
            </Text>
            <Text
              style={{
                fontSize: 15,
                lineHeight: 21,
                fontFamily: 'Signika-Regular',
                marginVertical: 10,
              }}
            >
              • Your use of any information or materials on this website is
              entirely at your own risk, for which we shall not be liable. It
              shall be your own responsibility to ensure that any products,
              services or information available through this website meet your
              specific requirements.
            </Text>
            <Text
              style={{
                fontSize: 15,
                lineHeight: 21,
                fontFamily: 'Signika-Regular',
                marginVertical: 10,
              }}
            >
              • This website contains material which is owned by or licensed to
              us. This material includes, but is not limited to, the design,
              layout, look, appearance and graphics. Reproduction is prohibited
              other than in accordance with the copyright notice, which forms
              part of these terms and conditions. All trademarks reproduced in
              this website which are not the property of, or licensed to Newbee
              Works are acknowledged on the website.
            </Text>
            <Text
              style={{
                fontSize: 15,
                lineHeight: 21,
                fontFamily: 'Signika-Regular',
                marginVertical: 10,
              }}
            >
              • Unauthorised use of this website may give rise to a claim for
              damages and/or be a criminal offence. From time to time this
              website may also include links to other websites. These links are
              provided for your convenience to provide further information. They
              do not signify that we endorse the website(s). We have no
              responsibility for the content of the linked website(s).
            </Text>
            <Text
              style={{
                fontSize: 15,
                lineHeight: 21,
                fontFamily: 'Signika-Regular',
                marginVertical: 10,
              }}
            >
              • You may not create a link to this website from another website
              or document without Newbee's prior written consent. Your use of
              this website and any dispute arising out of such use of the
              website is subject to the laws of India or regulatory authority
              within the country of India.
            </Text>
            <Text
              style={{
                fontSize: 18,
                lineHeight: 23,
                fontFamily: 'Signika-Bold',

                marginTop: 10,
              }}
            >
              Refund Policy
            </Text>
            <Text
              style={{
                fontSize: 15,
                lineHeight: 21,
                fontFamily: 'Signika-Regular',
                marginTop: 5,
              }}
            >
              When you buy our services, if you are, for any reason, not
              entirely happy with your purchase, you have the option to make a
              change in the choice of course within a period of 24 hours. To
              request a change under this guarantee, you must contact us within
              the 24 hours of your payment. Just send an email to
              contact@newbee.works. We will gladly process your request within
              24-72 hours of your change request. Please note that the course
              can only be switched once for each purchase.
            </Text>
            <Text
              style={{
                fontSize: 18,
                lineHeight: 23,
                fontFamily: 'Signika-Bold',

                marginTop: 10,
              }}
            >
              Privacy Policy
            </Text>
            <Text
              style={{
                fontSize: 15,
                lineHeight: 21,
                fontFamily: 'Signika-Regular',
                marginTop: 5,
              }}
            >
              This privacy policy sets out how Newbee Works uses and protects
              any information that you provide to Newbee Works when you use this
              website.
            </Text>
            <Text
              style={{
                fontSize: 15,
                lineHeight: 21,
                fontFamily: 'Signika-Regular',
                marginTop: 10,
              }}
            >
              Newbee Works is committed to ensuring that your privacy is
              protected. Should we ask you to provide certain information by
              which you can be identified when using this website, then you can
              be assured that it will only be used in accordance with this
              privacy statement.
            </Text>
            <Text
              style={{
                fontSize: 15,
                lineHeight: 21,
                fontFamily: 'Signika-Regular',
                marginTop: 10,
              }}
            >
              Newbee Works may change this policy from time to time by updating
              this page. You should check this page from time to time to ensure
              that you are happy with any changes. This policy is revised and
              effective from 28/04/2020.
            </Text>
            <Text
              style={{
                fontSize: 15,
                lineHeight: 21,
                fontFamily: 'Signika-Regular',
                marginTop: 5,
              }}
            >
              <Text
                style={{
                  fontSize: 15,
                  lineHeight: 21,
                  fontFamily: 'Signika-Bold',
                  marginTop: 5,
                }}
              >
                What we collect:
              </Text>
              We may collect the following information:
            </Text>
            <Text
              style={{
                fontSize: 15,
                lineHeight: 21,
                fontFamily: 'Signika-Regular',
                marginTop: 5,
                marginBottom: 10,
              }}
            >
              • Name and Job title
            </Text>
            <Text
              style={{
                fontSize: 15,
                lineHeight: 21,
                fontFamily: 'Signika-Regular',
                marginVertical: 6,
              }}
            >
              • Contact Information including email address & phone number
            </Text>
            <Text
              style={{
                fontSize: 15,
                lineHeight: 21,
                fontFamily: 'Signika-Regular',
                marginVertical: 6,
              }}
            >
              • Demographic Information such as City, postcode, preferences and
              interests
            </Text>
            <Text
              style={{
                fontSize: 15,
                lineHeight: 21,
                fontFamily: 'Signika-Regular',
                marginVertical: 6,
              }}
            >
              • Other Information relevant to service enquiry, customer surveys
              and/or offers
            </Text>
            <Text
              style={{
                fontSize: 15,
                lineHeight: 21,
                fontFamily: 'Signika-Bold',
                marginVertical: 10,
              }}
            >
              What we do with the information we gather
            </Text>
            <Text
              style={{
                fontSize: 15,
                lineHeight: 21,
                fontFamily: 'Signika-Regular',
                marginTop: 5,
              }}
            >
              We require this information to understand your needs and provide
              you with a best service, and in particular for the following
              reasons:
            </Text>
            <Text
              style={{
                fontSize: 15,
                lineHeight: 21,
                fontFamily: 'Signika-Regular',
                marginVertical: 6,
              }}
            >
              • Internal record keeping.
            </Text>
            <Text
              style={{
                fontSize: 15,
                lineHeight: 21,
                fontFamily: 'Signika-Regular',
                marginVertical: 6,
              }}
            >
              • We may use the information to improve our products and services.
            </Text>
            <Text
              style={{
                fontSize: 15,
                lineHeight: 21,
                fontFamily: 'Signika-Regular',
                marginVertical: 6,
              }}
            >
              • We may periodically send promotional emails about new products,
              special offers or other information which we think you may find
              interesting using the email address which you have provided.
            </Text>
            <Text
              style={{
                fontSize: 15,
                lineHeight: 21,
                fontFamily: 'Signika-Regular',
                marginVertical: 6,
              }}
            >
              • From time to time, we may also use your information to contact
              you for feedback, market research purposes. We may contact you by
              email, phone, fax or mail. We may use the information to customise
              the website according to your interests.
            </Text>

            <Text
              style={{
                fontSize: 20,
                lineHeight: 23,
                fontFamily: 'Signika-Bold',

                marginTop: 10,
              }}
            >
              Security
            </Text>

            <Text
              style={{
                fontSize: 15,
                lineHeight: 21,
                fontFamily: 'Signika-Regular',
                marginTop: 10,
              }}
            >
              We are committed to ensuring that your information is secure. In
              order to prevent unauthorised access or disclosure we have put in
              place suitable physical, electronic and managerial procedures to
              safeguard and secure the information we collect online.
            </Text>
            <Text
              style={{
                fontSize: 20,
                lineHeight: 23,
                fontFamily: 'Signika-Bold',

                marginTop: 10,
              }}
            >
              How we use cookies
            </Text>

            <Text
              style={{
                fontSize: 15,
                lineHeight: 21,
                fontFamily: 'Signika-Regular',
                marginBottom: 10,
              }}
            >
              A cookie is a small file which asks permission to be placed on
              your computer's hard drive. Once you agree, the file is added and
              the cookie helps analyse web traffic or lets you know when you
              visit a particular site. Cookies allow web applications to respond
              to you as an individual. The web application can tailor its
              operations to your needs, likes and dislikes by gathering and
              remembering information about your preferences. We use traffic log
              cookies to identify which pages are being used. This helps us
              analyse data about webpage traffic and improve our website in
              order to tailor it to customer needs. We only use this information
              for statistical analysis purposes and then the data is removed
              from the system. Overall, cookies help us provide you with a
              better website, by enabling us to monitor which pages you find
              useful and which you do not. A cookie in no way gives us access to
              your computer or any information about you, other than the data
              you choose to share with us. You can choose to accept or decline
              cookies. Most web browsers automatically accept cookies, but you
              can usually modify your browser setting to decline cookies if you
              prefer. This may prevent you from taking full advantage of the
              website.
            </Text>
            <Text
              style={{
                fontSize: 20,
                lineHeight: 23,
                fontFamily: 'Signika-Bold',

                marginTop: 10,
              }}
            >
              Links to other websites
            </Text>

            <Text
              style={{
                fontSize: 15,
                lineHeight: 21,
                fontFamily: 'Signika-Regular',
                marginBottom: 10,
              }}
            >
              Our website may contain links to other websites of interest.
              However, once you have used these links to leave our site, you
              should note that we do not have any control over that other
              website. Therefore, we cannot be responsible for the protection
              and privacy of any information which you provide whilst visiting
              such sites and such sites are not governed by this privacy
              statement. You should exercise caution and look at the privacy
              statement applicable to the website in question.
            </Text>
            <Text
              style={{
                fontSize: 20,
                lineHeight: 23,
                fontFamily: 'Signika-Bold',

                marginTop: 10,
              }}
            >
              Controlling your Personal Information
            </Text>

            <Text
              style={{
                fontSize: 15,
                lineHeight: 21,
                fontFamily: 'Signika-Regular',
                marginBottom: 10,
              }}
            >
              You may choose to restrict the collection or use of your personal
              information in the following ways:
            </Text>
            <Text
              style={{
                fontSize: 15,
                lineHeight: 21,
                fontFamily: 'Signika-Regular',
                marginTop: 5,
                marginBottom: 10,
              }}
            >
              • whenever you are asked to fill in a form on the website, look
              for the box that you can click to indicate that you do not want
              the information to be used by anybody for promotional purposes. If
              such box is not available, you may choose not to fill such form.
              However, by submitting the filled enquiry form, you will be
              construed to have foregone your right and Company may choose to
              send promotional emails and materials from time to time.
            </Text>
            <Text
              style={{
                fontSize: 15,
                lineHeight: 21,
                fontFamily: 'Signika-Regular',
                marginTop: 5,
                marginBottom: 10,
              }}
            >
              • if you have previously agreed to us using your personal
              information for promotional purposes, you may change your mind at
              any time by writing to or emailing us at contact@newbee.works
            </Text>
            <Text
              style={{
                fontSize: 15,
                lineHeight: 21,
                fontFamily: 'Signika-Regular',
                marginVertical: 10,
              }}
            >
              We will not sell, distribute or lease your personal information to
              third parties unless we have your permission or are required by
              law to do so. We may use your personal information to send you
              promotional information about third parties which we think you may
              find interesting.
            </Text>
            <Text
              style={{
                fontSize: 20,
                lineHeight: 23,
                fontFamily: 'Signika-Bold',

                marginTop: 10,
              }}
            >
              Contacting Us
            </Text>
            <Text
              style={{
                fontSize: 15,
                lineHeight: 21,
                fontFamily: 'Signika-Regular',
                marginVertical: 10,
              }}
            >
              If there are any questions regarding this privacy policy you may
              contact us using the information below: Newbee Works pvt limited,
            </Text>
            <Text
              style={{
                fontSize: 15,
                lineHeight: 21,
                fontFamily: 'Signika-Regular',
                marginVertical: 10,
                textDecorationLine: 'underline',
                color: 'blue',
              }}
            >
              contact@newbee.works
            </Text>
            <Text
              style={{
                fontSize: 15,
                lineHeight: 21,
                fontFamily: 'Signika-Regular',
                marginVertical: 10,
              }}
            >
              +91 9663545377
            </Text>
          </ScrollView>
        </View>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
export default Dashboard;
