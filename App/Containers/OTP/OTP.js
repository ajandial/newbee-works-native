import React from "react";
import {
  StyleSheet,
  View,
  ScrollView,
  Text,
  Image,
  TouchableHighlight
} from "react-native";
import logo from "../../Assets/Images/logo.png";
import { Input, Button } from "react-native-elements";
import OTPTextInput from 'react-native-otp-textinput';
import { Auth } from "aws-amplify";
import axios from "axios";

export default class OTP extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      authenticationCode: ""
    };
  }

  onChangeText = (key, val) => {
    this.setState({ [key]: val });
  };

  confirmSignUp = async () => {
    if (!this.state.authenticationCode) {
      return;
    }
    const { navigation } = this.props;
    const { authenticationCode } = this.state;
    const data = navigation.getParam("username");
    try {
      await Auth.confirmSignUp(data.username, authenticationCode);
      const user = await Auth.signIn(data.username, data.password);
      if (user) {
        let token = user
          .getSignInUserSession()
          .getAccessToken()
          .getJwtToken();

        axios
          .post(
            "http://newbee-prod-1105238445.ap-south-1.elb.amazonaws.com/api/user-api/user",
            {
              email: data.username,
              role: "Customer",
              firstName: data.name,
              lastName: data.name
            },
            {
              headers: {
                Authorization: `Bearer ${token}`
              }
            }
          )
          .then(data => {
            this.props.navigation.navigate("Customer");
          });
      }
      console.log("successully signed up!");
    } catch (err) {
      console.log("error confirming signing up: ", err);
    }
  };

  render() {
    return (
      <ScrollView
        style={{
          flex: 1,
          paddingTop: "10%"
        }}
      >
        <View
          style={{
            flex: 1,
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <Image
            style={{
              flex: 1,
              justifyContent: "center",
              alignItems: "center",
              resizeMode: "contain",
              width: 200,
              height: 100
            }}
            source={logo}
          />
        </View>
        <View
          style={{
            paddingTop: "10%",
            flex: 1,
            width: "90%",
            justifyContent: "flex-start",
            alignItems: "flex-start",
            paddingLeft: "10%"
          }}
        >
          <Text
            style={{
              fontFamily: "Signika-Regular",
              fontWeight: "600",
              fontSize: 24,
              lineHeight: 27,
              color: "#FCB61A"
            }}
          >
            One Time Password
          </Text>
          <View
            style={{
              paddingTop: 5,
              width: "10%",
              alignSelf: "stretch",
              borderBottomColor: "black",
              borderBottomWidth: 2
            }}
          />
          <Text
            style={{
              fontFamily: "Signika-Regular",
              paddingTop: 20
            }}
          >
            Code has been sent to your email. Please enter your code
          </Text>
          <Text
            style={{
              fontFamily: "Signika-Regular",
              paddingTop: 20,
              color: "#555555",
              fontSize: 12
            }}
          >
            Enter your code
          </Text>
          <OTPTextInput
            ref={(e) => (this.otpInput = e)}
            inputCount={6}
            containerStyle={{
              paddingTop: 5,
              backgroundColor: '#FFFFFF',
              borderRadius: 5,
              marginBottom: 20,
              width: '100%',
            }}
            textInputStyle={{
              fontSize: 14,
              lineHeight: 17,
              paddingLeft: 10,
              width: 40,
            }}
            tintColor="#FCB61A"
            handleTextChange={(val) =>
              this.onChangeText('authenticationCode', val)
            }
          />
        
          <Button
            containerStyle={{
              paddingTop: 10,
              width: "100%"
            }}
            buttonStyle={{
              width: "100%",
              backgroundColor: "black"
            }}
            titleStyle={{
              fontFamily: "Signika-Regular",
              fontSize: 20,
              color: "#FCB61A"
            }}
            onPress={this.confirmSignUp}
            title="Submit"
          />
          <TouchableHighlight
            underlayColor="#FFFFFF"
            style={{ alignSelf: "center" }}
            onPress={() => this.props.navigation.navigate("LoginScreen")}
          >
            <Text
              style={{
                fontFamily: "Signika-Regular",
                fontSize: 16,

                paddingTop: 20
              }}
            >
              Didn't receive yet ?{" "}
              <Text style={{ color: "#FCB61A" }}>Resend</Text>
            </Text>
          </TouchableHighlight>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  image: {
    width: "100%",
    height: "100%"
  },
  text: {
    fontSize: 20,
    color: "black",
    textAlign: "center",
    paddingTop: "10%"
  },
  title: {
    fontSize: 26,
    color: "white",
    textAlign: "center"
  }
});
