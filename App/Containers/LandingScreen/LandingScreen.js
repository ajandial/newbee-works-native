import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  TouchableWithoutFeedback,
  ImageBackground,
  ScrollView
} from "react-native";
import Video from "react-native-video";
// import logo from "../../Assets/Images/logo.png";
import splash from "../../Assets/Videos/Splash.mp4";
import logo from "../../Assets/Images/logo.png";
import splash1 from "../../Assets/Images/splash.png";

class Dashboard extends Component {
  render() {
    return (
      <View
        style={{
          flex: 1,
          justifyContent: "center",
          alignItems: "center"
        }}
      >
        <Video
          source={splash}
          style={{
            position: "absolute",
            backgroundColor: "white",
            top: 0,
            left: 0,
            right: 0,
            bottom: 0
          }}
          // onEnd={() =><RootScreen />}
          muted={true}
          repeat={false}
          resizeMode="contain"
        />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});
export default Dashboard;
