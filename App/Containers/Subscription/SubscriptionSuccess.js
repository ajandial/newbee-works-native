import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  SafeAreaView
} from "react-native";
import success from "../../Assets/Images/success.png";
import { Header, Icon } from "react-native-elements";
import NavigationService from "App/Services/NavigationService";

class Dashboard extends Component {
  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <View style={styles.container}>
          <View
            style={{
              height: "5%",
              justifyContent: "space-between",
              flexDirection: "row",
              alignItems: "center",
              padding: 10
            }}
          >
            <TouchableOpacity
              onPress={() => this.props.navigation.openDrawer()}
            >
              <Icon name="menu" color="#FCB61A" size={30} />
            </TouchableOpacity>
            <Text
              style={{
                fontFamily: "Signika-Regular",
                fontSize: 16,
                lineHeight: 20,
                textAlign: "center"
              }}
            >
              Success
            </Text>
            <View style={{ flexDirection: "row" }} />
          </View>
          <View
            style={{
              flex: 1,
              alignItems: "center",
              justifyContent: "center",
              color: "black"
            }}
          >
            <Image
              style={{
                flex: 1,
                justifyContent: "center",
                alignItems: "center",
                resizeMode: "contain",
                width: 200,
                height: 100
              }}
              source={success}
            />
            <Text style={styles.text}>Payment{"\n"}Successfull!</Text>
            <View
              style={{
                flex: 1,
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <TouchableOpacity
                style={{
                  flex: 1
                }}
                onPress={() => {
                  this.props.navigation.navigate("Dashboard");
                }}
              >
                <Text
                  style={{
                    fontSize: 18,
                    justifyContent: "center",
                    alignItems: "center",
                    color: "#FAC408"
                  }}
                >
                  Continue Reading
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  text: {
    marginTop: "5%",
    fontFamily: "Signika-Regular",
    fontSize: 30,
    lineHeight: 37,
    textAlign: "center"
  }
});
export default Dashboard;
