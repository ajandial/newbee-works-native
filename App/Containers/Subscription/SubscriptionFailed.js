import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  SafeAreaView
} from "react-native";
import failure from "../../Assets/Images/failure.png";
import { Header, Icon } from "react-native-elements";

class Dashboard extends Component {
  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <View style={styles.container}>
          <View
            style={{
              height: "5%",
              justifyContent: "space-between",
              flexDirection: "row",
              alignItems: "center",
              padding: 10
            }}
          >
            <TouchableOpacity
              onPress={() => this.props.navigation.openDrawer()}
            >
              <Icon name="menu" color="#FCB61A" size={30} />
            </TouchableOpacity>
            <Text
              style={{
                fontFamily: "Signika-Regular",
                fontSize: 16,
                lineHeight: 20,
                textAlign: "center"
              }}
            >
              Payment Unsuccessful
            </Text>
            <View style={{ flexDirection: "row" }} />
          </View>
          <View
            style={{
              flex: 1,
              alignItems: "center",
              justifyContent: "center",
              color: "black"
            }}
          >
            <Image
              style={{
                flex: 1,
                justifyContent: "center",
                alignItems: "center",
                resizeMode: "contain",
                width: 200,
                height: 100
              }}
              source={failure}
            />
            <View
              style={{
                flex: 1,
                justifyContent: "center",
                alignItems: "center"
              }}
            >
            <Text
              style={{
                
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              Sorry, Something went wrong
            </Text>
              <TouchableOpacity
                style={{
                  flex: 1
                }}
              onPress={() => this.props.navigation.push('Subscription', this.props.navigation.state.params)}
              >
                <Text
                  style={{
                    marginTop: 20,
                    justifyContent: "center",
                    alignItems: "center",
                    color: "#FAC408"
                  }}
                >
                  Retry
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});
export default Dashboard;
