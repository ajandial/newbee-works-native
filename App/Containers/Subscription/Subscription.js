import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  SafeAreaView
} from "react-native";
import PayuMoney from "react-native-payumoney";
import axios from "axios";
import NavigationService from 'App/Services/NavigationService'
import { Header, Icon } from "react-native-elements";
import { Auth } from "aws-amplify";

class Subscription extends Component {
  constructor(props) {
    super(props);
    this.state = {
      paymentData: []
    };
  }
  componentDidMount() {
    Auth.currentAuthenticatedUser().then(dt => {
      let token = dt
        .getSignInUserSession()
        .getAccessToken()
        .getJwtToken();
      axios
        .post(
          "http://newbee-prod-1105238445.ap-south-1.elb.amazonaws.com/api/payment-api/payment/paymentDetails",
          {
            email: dt.attributes.email,
            firstName: dt.attributes.email,
            phone: "9639999999",
            productName: this.props.navigation.state.params.courseItem
              .courseDescription,
            amount:
              this.props.navigation.state.params.courseItem.discountedPrice !==
              0
                ? this.props.navigation.state.params.courseItem.discountedPrice
                : this.props.navigation.state.params.courseItem.coursePrice
          },
          {
            headers: {
              Authorization: `Bearer ${token}`
            }
          }
        )
        .then(response => {
          const payData = {
            amount: response.data.amount,
            productName: response.data.productName,
            txnId: response.data.txnId,
            firstName: response.data.firstName,
            email: response.data.email,
            phone: response.data.phone,
            isDebug: true,
            merchantId: response.data.merchantId,
            key: response.data.key,
            surl: response.data.sUrl,
            furl: response.data.fUrl,
            successUrl: response.data.sUrl,
            failedUrl: response.data.sUrl,
            hash: response.data.hash
          };

          PayuMoney(payData)
            .then(data => {
              if (data.success === true) {
                axios
                  .post(
                    "http://newbee-prod-1105238445.ap-south-1.elb.amazonaws.com/api/course-api/subscription",
                    {
                      userId: dt.attributes.email,
                      courseId:this.props.navigation.state.params.courseItem.id,
                      subscriptionStatus:'SUCCESS',
                      subscriptionStartDate:new Date(),
                      subscriptionValidityDays: this.props.navigation.state.params.courseItem.subscriptionValidityDays,
                      freeCourse:this.props.navigation.state.params.courseItem.freeCourse
                    },
                    {
                      headers: {
                        Authorization: `Bearer ${token}`,
                        Accept: "application/json",
                        "Content-Type": "application/json"
                      }
                    }
                  )
                  .then(data => {
                    this.props.navigation.navigate("SubscriptionSuccess", this.props.navigation.state.params);
                  });
              }
            })
            .catch(e => {
              this.props.navigation.navigate("SubscriptionFailed", this.props.navigation.state.params);
              console.log(e);
            });
        });
    });
  }
  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <View style={styles.container}>
          <View
            style={{
              height: "5%",
              justifyContent: "space-between",
              flexDirection: "row",
              alignItems: "center",
              padding: 10
            }}
          >
            <TouchableOpacity
              onPress={() => this.props.navigation.openDrawer()}
            >
              <Icon name="menu" color="#FCB61A" size={30} />
            </TouchableOpacity>
            <Text
              style={{
                fontFamily: "Signika-Regular",
                fontSize: 16,
                lineHeight: 20,
                textAlign: "center"
              }}
            >
              Subscription
            </Text>
            <View style={{ flexDirection: "row" }}>
              <TouchableOpacity>
                <Icon name="search" color="#FCB61A" size={20} />
              </TouchableOpacity>
            </View>
          </View>
          <View
            style={{
              flex: 1,
              alignItems: "center",
              justifyContent: "center",
              color: "black"
            }}
          >
            <Text>Subscription</Text>
          </View>
        </View>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});
export default Subscription;
