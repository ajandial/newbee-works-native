import React, { Component } from "react";
import {
  StyleSheet,
  View,
  ScrollView,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  ActivityIndicator,
  FlatList,
  SafeAreaView
} from "react-native";
import { Card, Avatar, Icon, Button } from "react-native-elements";
import GestureRecognizer from "react-native-swipe-gestures";
import axios from "axios";
import { Auth } from "aws-amplify";

class QuizStart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentQuestion: 1,
      questions: [],
      questionAnswers: [],
      selectedAnswers: 0,
      submitLoading: false,
      markForReview: []
    };
  }

  componentDidMount() {
    this.setState({
      questions: this.props.navigation.state.params.info.questions
    });
  }

  renderOptions = (options, questionNumber) => {
    const optionArr = Object.entries(options);
    return optionArr.map((item, index) =>
      this.state.questionAnswers.find(question => {
        if (question.questionNumber === questionNumber) {
          return question.answers.find(item => {
            if (item === index + 1) {
              return true;
            } else {
              return false;
            }
          });
        }
      }) ? (
        <TouchableWithoutFeedback
          onPress={() => {
            this.setState(prevState => {
              const newQuestions = prevState.questionAnswers.map(question => {
                if (question.questionNumber === questionNumber) {
                  const newAnswers = question.answers.map(answer => {
                    if (answer === index + 1) {
                      return;
                    }
                    return answer;
                  });
                  question.answers = newAnswers;
                }
                return question;
              });
              return {
                ...prevState,
                selectedItem: [],
                questionAnswers: newQuestions
              };
            });
          }}
        >
          <Card
            containerStyle={{
              marginHorizontal: 0,
              borderRadius: 5,
              borderWidth: 0,
              backgroundColor: "#FCB61A",
              elevation: 6,
              shadowColor: "black",
              shadowOpacity: 0.26,
              shadowOffset: { width: 0, height: 2 },
              shadowRadius: 3,
              overflow: "hidden"
            }}
          >
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between"
              }}
            >
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <Text style={[styles.paymentTypeText, { color: "white" }]}>
                  {item[1]}
                </Text>
              </View>
              <Avatar
                containerStyle={{
                  backgroundColor: "#FAC408",
                  elevation: 6,
                  shadowColor: "black",
                  shadowOpacity: 0.26,
                  shadowOffset: { width: 0, height: 2 },
                  shadowRadius: 3,
                  overflow: "hidden"
                }}
                rounded
                icon={{ type: "antdesign", name: "check", color: "white" }}
              />
            </View>
          </Card>
        </TouchableWithoutFeedback>
      ) : (
        <TouchableWithoutFeedback
          onPress={() => {
            const newMarkForReview = this.state.markForReview.filter(
              mark => mark !== this.state.currentQuestion
            );

            this.setState(prevState => {
              const found = prevState.questionAnswers.find(
                question => question.questionNumber === questionNumber
              );

              if (!found) {
                prevState.questionAnswers.push({
                  questionNumber: questionNumber,
                  answers: [index + 1]
                });
              } else {
                found.answers.push(index + 1);
              }

              return {
                ...prevState,
                questionAnswers: prevState.questionAnswers,
                markForReview: newMarkForReview
              };
            });
          }}
        >
          <Card
            containerStyle={{
              marginHorizontal: 0,
              borderRadius: 5,
              backgroundColor: "#FAFAFA",
              borderWidth: 0
            }}
          >
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between"
              }}
            >
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <Text style={styles.paymentTypeText}>{item[1]}</Text>
              </View>
              <Avatar
                containerStyle={{
                  backgroundColor: "white"
                }}
                rounded
                icon={{
                  type: "antdesign",
                  name: "check",
                  color: "rgba(153, 153, 153, 0.2)"
                }}
              />
            </View>
          </Card>
        </TouchableWithoutFeedback>
      )
    );
  };

  renderQuestion = (item, index) => {
    return (
      this.state.currentQuestion === index + 1 && (
        <>
          <Text style={styles.heading}>Question: {item.questionNumber}</Text>
          <Text style={styles.paragraph}>{item.description}</Text>
          <View style={{ marginTop: 40, marginBottom: 5 }}>
            {this.renderOptions(item.options, item.questionNumber)}
          </View>
        </>
      )
    );
  };

  submitData = async () => {
    this.setState({ submitLoading: true });

    const newQuestionAndAnswers = this.state.questionAnswers.map(question => {
      const newAnswers = question.answers.filter(
        answer => answer !== undefined
      );
      question.answers = newAnswers;
      return question;
    });

    const data = {
      userId: this.props.navigation.state.params.uid,
      courseId: this.props.navigation.state.params.info.courseId,
      level: this.props.navigation.state.params.info.level,
      questionAnswers: newQuestionAndAnswers
    };
    try {
      Auth.currentAuthenticatedUser().then(dt => {
        let token = dt
          .getSignInUserSession()
          .getAccessToken()
          .getJwtToken();
        axios
          .post("http://newbee-prod-1105238445.ap-south-1.elb.amazonaws.com/api/course-api/quiz/score/calculate", data, {
            headers: {
              Authorization: `Bearer ${token}`
            }
          })
          .then(response => {
            this.setState({ submitLoading: false });
            this.props.navigation.navigate("QuizResult", {
              score: response.data,
              courseId: this.props.navigation.state.params.info.courseId,
              level: this.props.navigation.state.params.info.level,
              uid: this.props.navigation.state.params.info.uid
            });
          });
      });
    } catch (error) {
      this.setState({ submitLoading: false });
      console.log(error);
    }
  };

  onSwipeLeft = gestureState => {
    if (this.state.currentQuestion !== this.state.questions.length) {
      this.setState(prevState => {
        return {
          ...prevState,
          currentQuestion: prevState.currentQuestion + 1
        };
      });
    }
  };

  onSwipeRight = gestureState => {
    this.setState(prevState => ({
      currentQuestion:
        prevState.currentQuestion === 1
          ? prevState.currentQuestion
          : prevState.currentQuestion - 1
    }));
  };
  isDoneDisabled = () => {
    const newQuestionAndAnswers = this.state.questionAnswers.map(question => {
      const newAnswers = question.answers.filter(
        answer => answer !== undefined
      );
      question.answers = newAnswers;
      return question;
    });
    const value = newQuestionAndAnswers.find(
      question => question.answers.length === 0
    );

    if (value) {
      return true;
    } else {
      return false;
    }
  };

  isMarkForReview = () => {
    const markForReview = this.state.markForReview.find(
      item => item === this.state.currentQuestion
    );
    return markForReview ? true : false;
  }
  onMarkForReviewPress = () => {
    const newQuestionsAndAnswers = this.state.questionAnswers.map(question => {
      if (question.questionNumber === this.state.currentQuestion) {
        question.answers = [];
      }
      return question;
    });
    this.setState(prevState => {
      return {
        markForReview: [...prevState.markForReview, this.state.currentQuestion],
        questionAnswers: newQuestionsAndAnswers
      };
    });
  };
  render() {
    const config = {
      velocityThreshold: 0.3,
      directionalOffsetThreshold: 80
    };

    return (
      <SafeAreaView style={{ flex: 1 }}>
        <GestureRecognizer
          onSwipeLeft={state => this.onSwipeLeft(state)}
          onSwipeRight={state => this.onSwipeRight(state)}
          config={config}
          style={{
            flex: 1
          }}
        >
          <ScrollView
            style={{
              flex: 1
            }}
            contentContainerStyle={{ flexGrow: 1 }}
          >
            <View
              style={{
                marginBottom: 10,
                padding: 20,
                alignItems: "center",
                flexDirection: "row"
              }}
            >
              <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                <Icon
                  type="font-awesome"
                  name="arrow-left"
                  color="#FCB61A"
                  style={{ alignSelf: "flex-start" }}
                />
              </TouchableOpacity>
              <View style={{ flex: 1 }}>
                <Text
                  style={{
                    fontSize: 14,
                    lineHeight: 17,
                    fontFamily: "Signika-Regular",
                    textAlign: "center"
                  }}
                >
                  {this.props.navigation.state.params.info.level}
                </Text>
              </View>
            </View>

            <View
              style={{
                flex: 1,
                padding: 20
              }}
            >
              {this.state.questions.map((item, index) =>
                this.renderQuestion(item, index)
              )}

              <View
                style={{
                  flex: 1,

                  marginTop: 15,
                  paddingBottom: 10
                }}
              >
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-between",
                    marginTop: "7%"
                  }}
                >
                  <Button
                    containerStyle={{
                      justifyContent: "center"
                    }}
                    disabled={this.isMarkForReview()}
                    buttonStyle={{
                      width: 135,
                      height: 50,
                      borderRadius: 50,
                      backgroundColor: "#FCB61A"
                    }}
                    title="Mark for review"
                    titleStyle={{
                      fontSize: 14,
                      lineHeight: 17,
                      textAlign: "center",
                      fontFamily: "Signika-Regular"
                    }}
                    onPress={this.onMarkForReviewPress}
                  />
                  {this.props.navigation.state.params.info.questions.length ===
                  this.state.currentQuestion ? (
                    !this.state.submitLoading ? (
                      <Button
                        containerStyle={{
                          justifyContent: "center"
                        }}
                        buttonStyle={{
                          width: 135,
                          height: 50,
                          borderRadius: 50,
                          backgroundColor: "#000000"
                        }}
                        disabled={
                          this.state.markForReview.length > 0 ||
                          this.state.questionAnswers.length !==
                            this.state.questions.length ||
                          this.isDoneDisabled()
                        }
                        title="Done"
                        titleStyle={{
                          fontSize: 14,
                          lineHeight: 17,
                          color: "#FAC408",
                          textAlign: "center",
                          fontFamily: "Signika-Regular"
                        }}
                        onPress={this.submitData}
                      />
                    ) : (
                      <ActivityIndicator size={30} color="#FAC408" />
                    )
                  ) : (
                    <Button
                      title="Next"
                      containerStyle={{
                        justifyContent: "center"
                      }}
                      buttonStyle={{
                        width: 135,
                        height: 50,
                        borderRadius: 50,
                        backgroundColor: "#000000"
                      }}
                      titleStyle={{
                        fontSize: 14,
                        lineHeight: 17,
                        color: "#FAC408",
                        textAlign: "center",
                        fontFamily: "Signika-Regular"
                      }}
                      onPress={() => {
                        this.setState(prevState => {
                          return {
                            ...prevState,
                            currentQuestion: prevState.currentQuestion + 1,
                            questionAnswers: prevState.questionAnswers
                          };
                        });
                      }}
                    />
                  )}
                </View>
              </View>
              <View
                style={{
                  flexDirection: "row",
                  paddingHorizontal: 5,
                  alignSelf: "center"
                }}
              >
                <FlatList
                  data={this.props.navigation.state.params.info.questions}
                  horizontal
                  showsHorizontalScrollIndicator={true}
                  keyExtractor={(item, index) => index.toString()}
                  renderItem={({ item, index }) =>
                    this.state.currentQuestion === index + 1 ? (
                      this.state.markForReview.find(
                        item => item === index + 1
                      ) ? (
                        <Button
                          title={`${index + 1}`}
                          buttonStyle={{
                            backgroundColor: "blue"
                          }}
                          containerStyle={{
                            borderRadius: 25,
                            width: 25,
                            height: 25,
                            marginHorizontal: 5,
                            justifyContent: "center"
                          }}
                          titleStyle={{ fontSize: 7 }}
                          onPress={() =>
                            this.setState({ currentQuestion: index + 1 })
                          }
                        />
                      ) : (
                        <Button
                          title={`${index + 1}`}
                          buttonStyle={{
                            backgroundColor: "#FAC408"
                          }}
                          containerStyle={{
                            borderRadius: 25,
                            width: 25,
                            height: 25,
                            marginHorizontal: 5,
                            justifyContent: "center"
                          }}
                          titleStyle={{ fontSize: 7 }}
                          onPress={() =>
                            this.setState({ currentQuestion: index + 1 })
                          }
                        />
                      )
                    ) : this.state.markForReview.find(
                        item => item === index + 1
                      ) ? (
                      <Button
                        title={`${index + 1}`}
                        buttonStyle={{
                          backgroundColor: "blue"
                        }}
                        containerStyle={{
                          borderRadius: 25,
                          width: 25,
                          height: 25,
                          marginHorizontal: 5,
                          justifyContent: "center"
                        }}
                        titleStyle={{ fontSize: 7 }}
                        onPress={() =>
                          this.setState({ currentQuestion: index + 1 })
                        }
                      />
                    ) : (
                      <Button
                        title={`${index + 1}`}
                        buttonStyle={{
                          backgroundColor: "#000000"
                        }}
                        containerStyle={{
                          borderRadius: 25,
                          width: 25,
                          height: 25,
                          marginHorizontal: 5,
                          justifyContent: "center",
                          alignSelf: "center"
                        }}
                        titleStyle={{ color: "white", fontSize: 7 }}
                        onPress={() =>
                          this.setState({ currentQuestion: index + 1 })
                        }
                      />
                    )
                  }
                />
              </View>
            </View>
          </ScrollView>
        </GestureRecognizer>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  heading: {
    fontSize: 16,
    lineHeight: 20,
    fontFamily: "Signika-Regular"
  },
  paragraph: {
    marginTop: "4%",
    fontSize: 12,
    lineHeight: 15,
    fontFamily: "Signika-Regular"
  },
  heading: {
    fontSize: 14,
    lineHeight: 17,
    fontFamily: "Signika-Regular"
  },
  paymentTypeText: {
    fontSize: 12,
    lineHeight: 18,
    color: "#999999",
    fontFamily: "Signika-Regular",
    marginLeft: "10%"
  }
});

export default QuizStart;
