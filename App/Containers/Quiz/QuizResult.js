import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  Image,
  ActivityIndicator
} from "react-native";
import { Icon, Card, Badge, Avatar, Button } from "react-native-elements";
import axios from "axios";
import { StackActions, NavigationActions } from "react-navigation";
import { Auth } from "aws-amplify";

class QuizResult extends Component {
  static navigationOptions = ({ navigation }) => ({
    drawerLabel: "Take a Quiz",
    drawerIcon: ({ tintColor }) => (
      <Icon
        name="user"
        type="font-awesome"
        color={tintColor}
        style={{ fontSize: 24, opacity: 1 }}
      />
    )
  });
  state = {
    showSubscribeCard: true,
    cardInput: "",
    loading: false,
    attempts: []
  };

  componentDidMount = async () => {
    this.setState({ loading: true });

    try {
      Auth.currentAuthenticatedUser().then(dt => {
        let token = dt
          .getSignInUserSession()
          .getAccessToken()
          .getJwtToken();
        fetch(
          `http://newbee-prod-1105238445.ap-south-1.elb.amazonaws.com/api/user-api/user/search/findByEmail?email=${
            dt.attributes.email
          }`,
          {
            method: "GET",
            headers: {
              Accept: "application/json",
              Authorization: `Bearer ${token}`,
              "Content-Type": "application/json"
            }
          }
        )
          .then(response => response.json())
          .then(data => {
            axios
              .get(
                `http://newbee-prod-1105238445.ap-south-1.elb.amazonaws.com/api/course-api/score/search/findByUserIdAndCourseIdAndLevel?userId=${
                  data._embedded.users[0].id
                }&courseId=${
                  this.props.navigation.state.params.courseId
                }&level=${this.props.navigation.state.params.level}`,
                {
                  headers: {
                    Authorization: `Bearer ${token}`
                  }
                }
              )
              .then(response => {
                this.setState({
                  loading: false,
                  attempts: response.data._embedded.scores[0].attempts
                });
              });
          });
      });
    } catch (error) {
      console.log(error);
      this.setState({ loading: false });
    }
  };

  toggleSubscribeCard = () =>
    this.setState(state => ({
      showSubscribeCard: !state.showSubscribeCard
    }));
  renderLevels = item => {
    return item.selected === true ? (
      <Card
        containerStyle={{
          marginHorizontal: 0,
          borderRadius: 5,
          borderWidth: 0,
          backgroundColor: "#FCB61A",
          elevation: 6,
          shadowColor: "black",
          shadowOpacity: 0.26,
          shadowOffset: { width: 0, height: 2 },
          shadowRadius: 3,
          overflow: "hidden",
          height: 90,
          width: 100,
          marginHorizontal: "5%"
        }}
      >
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between"
          }}
        >
          <Badge
            value={
              <Avatar
                containerStyle={{
                  backgroundColor: "#FAC408",
                  elevation: 6,
                  shadowColor: "black",
                  shadowOpacity: 0.26,
                  shadowOffset: { width: 0, height: 2 },
                  shadowRadius: 3,
                  overflow: "hidden"
                }}
                rounded
                icon={{ type: "antdesign", name: "check", color: "white" }}
              />
            }
            containerStyle={{
              position: "absolute",
              top: -20,
              right: -20,

              backgroundColor: "red"
            }}
          />
          <View style={{ flexDirection: "row", alignItems: "center" }}>
            <Text style={[styles.paymentTypeText, { color: "white" }]}>
              {item.levelType}
            </Text>
          </View>
        </View>
      </Card>
    ) : (
      <Card
        containerStyle={{
          marginHorizontal: "2%",
          borderRadius: 5,
          backgroundColor: "#FAFAFA",
          borderWidth: 0,
          height: 90,
          width: 100
        }}
      >
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between"
          }}
        >
          <View style={{ flexDirection: "row", alignItems: "center" }}>
            <Text style={styles.paymentTypeText}>{item.levelType}</Text>
          </View>
          <Avatar
            containerStyle={{
              backgroundColor: "white"
            }}
            rounded
            icon={{
              type: "antdesign",
              name: "check",
              color: "rgba(153, 153, 153, 0.2)"
            }}
          />
        </View>
      </Card>
    );
  };
  render() {
    return (
      <View style={styles.container}>
        <ScrollView style={{ flex: 1 }} contentContainerStyle={{ flexGrow: 1 }}>
          <View
            style={{
              alignItems: "center",
              justifyContent: "center",
              color: "black"
            }}
          />
          <Card
            containerStyle={{
              backgroundColor: "#FCB61A",
              flex: 2,
              margin: 0,
              padding: 0,
              justifyContent: "flex-end",
              alignSelf: "flex-start",
              width: "100%",
              borderBottomStartRadius: 65,
              borderBottomEndRadius: 65
            }}
          >
            {this.props.navigation.state.params.score < 60 ? (
              <Image
                source={require("App/Assets/Images/quizFail.png")}
                resizeMode="contain"
                style={{
                  height: "100%",
                  width: "100%",
                  alignSelf: "center"
                }}
              />
            ) : (
              <Image
                source={require("App/Assets/Images/QuizResult.png")}
                resizeMode="cover"
                style={{
                  height: "100%",
                  width: "100%"
                }}
              />
            )}
          </Card>
          <View
            style={{
              flex: 1,
              justifyContent: "center",
              alignItems: "center",
              marginHorizontal: 20
            }}
          >
            <Text
              style={{
                fontSize: 24,
                lineHeight: 30,
                fontFamily: "Signika-Regular"
              }}
            >
              {this.props.navigation.state.params.score < 60
                ? "Sorry!"
                : "Congratulations!"}
            </Text>
            <Text
              style={{
                fontFamily: "Signika-Regular",
                fontSize: 12,
                lineHeight: 15,
                textAlign: "center",
                marginTop: "5%"
              }}
            >
              It is a long established fact that a reader will be distracted by
              the readable. Lorem Ipsum is simply dummy text of the printing and
              typesetting industry.
            </Text>

            <Card
              containerStyle={{
                marginHorizontal: 0,
                borderRadius: 5,
                borderWidth: 0,
                width: "100%",
                elevation: 6,
                shadowColor: "black",
                shadowOpacity: 0.26,
                shadowOffset: { width: 0, height: 2 },
                shadowRadius: 3,
                overflow: "hidden",
                paddingHorizontal: 30,
                borderRightWidth: 2,
                borderLeftWidth: 2,
                borderColor: "#FCB61A"
              }}
            >
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between"
                }}
              >
                <Text
                  style={{
                    fontFamily: "Signika-Regular",
                    fontSize: 16,
                    lineHeight: 20,
                    textAlign: "center"
                  }}
                >
                  Points earned
                </Text>
                <Text
                  style={{
                    fontFamily: "Signika-Regular",
                    fontSize: 16,
                    lineHeight: 20,
                    textAlign: "center"
                  }}
                >
                  Attempts left
                </Text>
              </View>
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                  marginTop: "5%"
                }}
              >
                <Text
                  style={{
                    fontFamily: "Signika-Regular",
                    fontSize: 16,
                    lineHeight: 20,
                    textAlign: "center"
                  }}
                >
                  {this.props.navigation.state.params.score} / 100
                </Text>
                {this.state.loading ? (
                  <ActivityIndicator size={20} color="#FCB61A" />
                ) : (
                  <Text
                    style={{
                      fontFamily: "Signika-Regular",
                      fontSize: 16,
                      lineHeight: 20,
                      textAlign: "center"
                    }}
                  >
                    0{parseInt(5) - parseInt(this.state.attempts.length)} / 05
                  </Text>
                )}
              </View>
            </Card>
          </View>
          <View
            style={{
              width: "100%",
              flexDirection: "row",
              justifyContent: "space-between",
              alignSelf: "flex-end",
              marginBottom: 10,
              paddingHorizontal: 20
            }}
          >
            <Button
              containerStyle={{
                borderRadius: 25
              }}
              buttonStyle={{
                width: 135,
                height: 50,

                backgroundColor: "#FCB61A"
              }}
              title="Go to home"
              titleStyle={{
                fontSize: 14,
                lineHeight: 17,
                color: "white",
                textAlign: "center",
                fontFamily: "Signika-Regular"
              }}
              onPress={() => {
                const resetAction = StackActions.reset({
                  index: 0,
                  actions: [NavigationActions.navigate({ routeName: "Quiz" })]
                });
                this.props.navigation.dispatch(resetAction);
                this.props.navigation.navigate("Dashboard");
              }}
            />
            <Button
              containerStyle={{
                borderRadius: 25
              }}
              buttonStyle={{
                width: 135,
                height: 50,

                backgroundColor: "#000000"
              }}
              title="Retake a quiz"
              titleStyle={{
                fontSize: 14,
                lineHeight: 17,
                color: "#FAC408",
                textAlign: "center",
                fontFamily: "Signika-Regular"
              }}
              onPress={() => {
                const resetAction = StackActions.reset({
                  index: 0,
                  actions: [NavigationActions.navigate({ routeName: "Quiz" })]
                });
                this.props.navigation.dispatch(resetAction);
              }}
            />
          </View>
        </ScrollView>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  selectText: {
    fontSize: 10,
    lineHeight: 12,
    color: "#555555",
    fontFamily: "Signika-Regular"
  },
  input: {
    marginTop: "3%",
    fontFamily: "Signika-Regular",
    fontSize: 10,
    lineHeight: 22,
    color: "#000000",
    backgroundColor: "#FFFFFF",
    borderRadius: 5,
    elevation: 2,
    padding: 10
  },
  selectLevel: {
    fontSize: 10,
    lineHeight: 12,
    color: "#555555",
    fontFamily: "Signika-Regular",
    marginTop: "5%"
  }
});

export default QuizResult;
