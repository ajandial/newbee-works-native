import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableWithoutFeedback,
  ScrollView,
  TouchableHighlight,
  Image,
  FlatList,
  ActivityIndicator
} from "react-native";
import contents from "../contents.json";
import { Icon, Card, Button, SearchBar } from "react-native-elements";
import { TouchableOpacity } from "react-native-gesture-handler";
import { Dropdown } from "react-native-material-dropdown";
import axios from "axios";
import { Auth } from "aws-amplify";

class Dashboard extends Component {
  static navigationOptions = ({ navigation }) => ({
    drawerLabel: "Take a Quiz",
    drawerIcon: ({ tintColor }) => (
      <Icon
        name="user"
        type="font-awesome"
        color={tintColor}
        style={{ fontSize: 24, opacity: 1 }}
      />
    )
  });
  constructor(props) {
    super(props);
    this.state = {
      showScoreGrid: false,
      showInstructionsCard: false,
      cardInput: "",
      totalAttempts: 0,
      courses: [],
      loading: false,
      uid: "",
      error: "",
      quizData: null,
      pickerValue: "Select",
      currentLevel: null,
      scores: [],
      loadingScores: false
    };
  }

  componentDidMount = async () => {
    this.setState({ loading: true });
    await this.load();
  };

  load = async () => {
    try {
      await Auth.currentAuthenticatedUser().then(dt => {
        let token = dt
          .getSignInUserSession()
          .getAccessToken()
          .getJwtToken();
        fetch(
          `http://newbee-prod-1105238445.ap-south-1.elb.amazonaws.com/api/user-api/user/search/findByEmail?email=${
            dt.attributes.email
          }`,
          {
            method: "GET",
            headers: {
              Accept: "application/json",
              Authorization: `Bearer ${token}`,
              "Content-Type": "application/json"
            }
          }
        )
          .then(response => response.json())
          .then(data => {
            axios
              .get("http://newbee-prod-1105238445.ap-south-1.elb.amazonaws.com/api/course-api/course", {
                headers: {
                  Authorization: `Bearer ${token}`
                }
              })
              .then(response => {
                this.setState({
                  courses: response.data._embedded.courses,
                  loading: false,
                  uid: data._embedded.users[0].id
                });
              });
          });
      });
    } catch (error) {
      console.log(error);
      this.setState({ error: "Something went wrong", loading: false });
    }
  }

  getScoreGrid = async () => {
    this.setState({ loadingScores: true });
    try {
      await Auth.currentAuthenticatedUser().then(dt => {
        let token = dt
          .getSignInUserSession()
          .getAccessToken()
          .getJwtToken();
        axios
          .get(
            `http://newbee-prod-1105238445.ap-south-1.elb.amazonaws.com/api/course-api/score/search/findByUserIdAndCourseIdAndLevel?userId=${
              this.state.uid
            }&courseId=${this.state.currentLevel.courseId}&level=${
              this.state.currentLevel.level
            }`,
            {
              headers: {
                Authorization: `Bearer ${token}`
              }
            }
          )
          .then(response => {
            this.setState({
              scores: response.data._embedded.scores,
              loadingScores: false,
              totalAttempts: response.data._embedded.scores[0].attempts.length
            });
          });
      });
    } catch (error) {
      this.setState({ loadingScores: true });
      console.log(error);
    }
  };

  renderScoreGrid = () => {
    return (
      <Card
        containerStyle={{
          borderRadius: 10,
          elevation: 6,
          shadowColor: "black",
          shadowOpacity: 0.26,
          shadowOffset: { width: 0, height: 2 },
          shadowRadius: 3,
          overflow: "hidden",
          marginBottom: "2%",
          height: "50%",
          position: "absolute",
          alignSelf: "center",
          width: "90%",
          bottom: "10%"
        }}
      >
        {this.state.scores[0] &&
        this.state.loadingScore &&
        this.state.scores[0].attempts !== 0 ? (
          <View
            style={{
              justifyContent: "center",

              alignItems: "center"
            }}
          >
            <ActivityIndicator color="black" size={40} />
          </View>
        ) : (
          <>
            <View
              style={{
                justifyContent: "space-between",
                flexDirection: "row",
                alignItems: "center"
              }}
            >
              <Text
                style={{
                  fontSize: 16,
                  lineHeight: 20,
                  textAlign: "center",
                  fontFamily: "Signika-Regular"
                }}
              >
                Score grid
              </Text>
              <TouchableHighlight
                underlayColor="white"
                disabled={!this.state.pickerValue}
                onPress={() => {
                  this.setState({ showScoreGrid: false });
                }}
              >
                <Icon name="close" style={{ alignSelf: "flex-end" }} />
              </TouchableHighlight>
            </View>

            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                marginVertical: "3%"
              }}
            >
              <Text
                style={{
                  fontSize: 10,
                  lineHeight: 12,
                  color: "#555555",
                  fontFamily: "Signika-Regular"
                }}
              >
                Attempts
              </Text>
              <Text
                style={{
                  fontSize: 10,
                  lineHeight: 12,
                  color: "#555555",

                  fontFamily: "Signika-Regular"
                }}
              >
                Right Answers
              </Text>
              <Text
                style={{
                  fontSize: 10,
                  lineHeight: 12,
                  color: "#555555",
                  fontFamily: "Signika-Regular"
                }}
              >
                Multiplier
              </Text>
              <Text
                style={{
                  fontSize: 10,
                  lineHeight: 12,
                  color: "#555555",
                  fontFamily: "Signika-Regular"
                }}
              >
                Final points
              </Text>
            </View>
            <ScrollView>
              {this.state.scores[0] ? (
                this.state.scores[0].attempts.map(attempt => (
                  <View
                    style={{
                      flexDirection: "row",
                      marginVertical: "1%",
                      justifyContent: "space-between",
                      paddingVertical: 10
                    }}
                  >
                    <Text
                      style={{
                        fontSize: 10,
                        lineHeight: 12,
                        color: "#555555"
                      }}
                    >
                      {attempt.questionAttended}
                    </Text>
                    <Text
                      style={{
                        fontSize: 10,
                        lineHeight: 12,
                        color: "#555555"
                      }}
                    >
                      {attempt.rightAnswers}
                    </Text>
                    <Text
                      style={{
                        fontSize: 10,
                        lineHeight: 12,
                        color: "#555555"
                      }}
                    >
                      {attempt.multiplier}
                    </Text>
                    <Text
                      style={{
                        fontSize: 10,
                        lineHeight: 12,
                        color: "#555555"
                      }}
                    >
                      {attempt.finalScore}
                    </Text>
                  </View>
                ))
              ) : (
                <Text
                  style={{
                    fontSize: 14,
                    textAlign: "center",
                    color: "#555555"
                  }}
                >
                  No Attempts Taken Yet
                </Text>
              )}
            </ScrollView>
          </>
        )}
      </Card>
    );
  };

  renderInstructionsCard = () => (
    <Card
      containerStyle={{
        borderRadius: 10,
        elevation: 6,
        shadowColor: "black",
        shadowOpacity: 0.26,
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 3,
        overflow: "hidden",
        marginBottom: "2%",
        height: "30%",
        position: "absolute",
        alignSelf: "center",
        width: "90%",
        bottom: "15%"
      }}
    >
      <View
        style={{
          justifyContent: "space-between",
          flexDirection: "row",
          alignItems: "center"
        }}
      >
        <Text
          style={{
            fontSize: 16,
            lineHeight: 20,
            textAlign: "center",
            fontFamily: "Signika-Regular"
          }}
        >
          Instructions
        </Text>
        <TouchableHighlight
          underlayColor="white"
          onPress={() => this.setState({ showInstructionsCard: false })}
        >
          <Icon name="close" style={{ alignSelf: "flex-end" }} />
        </TouchableHighlight>
      </View>

      <View
        style={{
          flex: 1,
          justifyContent: "space-between",
          marginVertical: "3%"
        }}
      >
        <Text
          style={{
            fontSize: 14,
            height: 20,
            color: "#555555",
            fontFamily: "Signika-Regular",
            marginVertical: "2%"
          }}
        >
          <Text style={{ color: "#FAC408" }}>•</Text>{" "}
          {contents.quizintruction_1}
        </Text>
        <Text
          style={{
            fontSize: 14,
            height: 20,
            color: "#555555",
            fontFamily: "Signika-Regular",
            marginVertical: "2%"
          }}
        >
          <Text style={{ color: "#FAC408" }}>•</Text>{" "}
          {contents.quizintruction_2}
        </Text>
        <Text
          style={{
            fontSize: 14,
            height: 20,
            color: "#555555",
            fontFamily: "Signika-Regular",
            marginVertical: "2%"
          }}
        >
          <Text style={{ color: "#FAC408" }}>•</Text>{" "}
          {contents.quizintruction_3}
        </Text>
        <Text
          style={{
            fontSize: 14,
            height: 40,
            width: "100%",
            color: "#555555",
            fontFamily: "Signika-Regular",
            marginVertical: "2%"
          }}
        >
          <Text style={{ color: "#FAC408" }}>•</Text>{" "}
          {contents.quizintruction_4} {"\n"}
          <Text
            style={{
              fontSize: 14,
              height: 40,
              width: "100%",
              color: "#555555",
              fontFamily: "Signika-Regular",
              marginVertical: "2%"
            }}
          >
            {" "}
            {contents.quizintruction_5}{" "}
          </Text>
        </Text>
      </View>
    </Card>
  );

  renderLevels = (item, index) => {
    return item.selected === true ? (
      <TouchableWithoutFeedback
        onPress={() => {
          this.getScoreGrid();
          this.setState(prevState => ({
            quizData: prevState.quizData.map(level => {
              if (level.levelType === item.levelType) {
                level.selected = false;
              }
              return level;
            })
          }));
          this.setState({ currentLevel: null });
        }}
      >
        <Card
          containerStyle={{
            borderRadius: 5,
            borderWidth: 0,
            backgroundColor: "#FCB61A",
            elevation: 6,
            shadowColor: "black",
            shadowOpacity: 0.26,
            shadowOffset: { width: 0, height: 2 },
            shadowRadius: 3,
            height: 90,
            width: 100,
            marginHorizontal: 7,

            marginBottom: 10
          }}
        >
          <Icon
            type="antdesign"
            name="check"
            containerStyle={{
              backgroundColor: "#FAC408",
              width: 25,
              height: 25,
              borderRadius: 25,
              justifyContent: "center",
              position: "absolute",
              top: -25,
              right: -20,
              elevation: 6,
              shadowColor: "black",
              shadowOpacity: 0.26,
              shadowOffset: { width: 0, height: 2 },
              shadowRadius: 3
            }}
            color="white"
            size={15}
          />
          <View
            style={{
              height: "100%",
              width: "100%",
              justifyContent: "center"
            }}
          >
            <Text
              style={[
                styles.paymentTypeText,
                {
                  textAlign: "center",
                  fontWeight: "bold",
                  fontSize: 12,
                  lineHeight: 18,
                  fontFamily: "Signika-Regular",
                  color: "white"
                }
              ]}
            >
              {item.level}
            </Text>
          </View>
        </Card>
      </TouchableWithoutFeedback>
    ) : (
      <TouchableWithoutFeedback
        onPress={() => {
          this.getScoreGrid();
          let currentLevel = null;
          this.setState(prevState => ({
            quizData: prevState.quizData.map(level => {
              if (level.level === item.level) {
                this.setState({ currentLevel: level });
                level.selected = true;

                return level;
              }

              level.selected = false;
              return level;
            })
          }));
        }}
      >
        <Card
          containerStyle={{
            marginHorizontal: 7,
            borderRadius: 5,
            backgroundColor: "#FAFAFA",
            borderWidth: 0,
            height: 90,
            width: 100,
            alignItems: "center",
            marginBottom: 10
          }}
        >
          <Icon
            type="antdesign"
            name="check"
            containerStyle={{
              width: 25,
              height: 25,
              borderRadius: 25,
              justifyContent: "center",
              position: "absolute",
              top: -25,
              right: -30,

              backgroundColor: "white"
            }}
            color="rgba(153, 153, 153, 0.2)"
            size={15}
          />
          <View
            style={{
              height: "100%",
              width: "100%",

              justifyContent: "center"
            }}
          >
            <Text
              style={{
                color: "#999999",
                fontFamily: "Signika-Regular",
                fontSize: 12,
                lineHeight: 18
              }}
            >
              {item.level}
            </Text>
          </View>
        </Card>
      </TouchableWithoutFeedback>
    );
  };
  getLevels = async courseId => {
    try {
      Auth.currentAuthenticatedUser().then(dt => {
        let token = dt
          .getSignInUserSession()
          .getAccessToken()
          .getJwtToken();
        axios
          .get(
            `http://newbee-prod-1105238445.ap-south-1.elb.amazonaws.com/api/course-api/quiz/search/findByCourseId?courseId=${courseId}`,
            {
              headers: {
                Authorization: `Bearer ${token}`
              }
            }
          )
          .then(response => {
            this.setState({
              quizData: response.data._embedded.quizs
            });
          });
      });
    } catch (error) {
      console.log(error);
    }
  };

  render() {
    return (
      <>
        <View
          style={{
            color: "black",
            height: "50%"
          }}
        >
          <Card
            containerStyle={{
              backgroundColor: "#FCB61A",
              flex: 1,
              margin: 0,
              padding: 0,
              justifyContent: "flex-start",
              alignSelf: "flex-start",
              width: "100%",
              borderBottomStartRadius: 60,
              borderBottomEndRadius: 60
            }}
          >
            <View
              style={{
                justifyContent: "space-between",
                flexDirection: "row",
                alignItems: "center",
                padding: 10
              }}
            >
              <TouchableOpacity
                onPress={() => this.props.navigation.openDrawer()}
              >
                <Icon name="menu" color="white" size={30} />
              </TouchableOpacity>

              <Text
                style={{
                  fontFamily: "Signika-Regular",
                  color: "white",
                  fontSize: 16,
                  lineHeight: 20,
                  textAlign: "center"
                }}
              >
                Take A Quiz
              </Text>
              <TouchableOpacity>
                <Icon name="search" color="#FCB61A" size={20} />
              </TouchableOpacity>
            </View>
            <Image
              resizeMode="contain"
              source={require("../../Assets/Images/quiz.png")}
              style={{ width: "100%", height: "90%" }}
            />
          </Card>
        </View>
        <ScrollView style={{ flex: 1 }} contentContainerStyle={{ flexGrow: 1 }}>
          <View
            style={{
              flex: 1,
              padding: 30,
              marginTop: 15
            }}
          >
            <Text style={styles.selectText}>Select a module</Text>
            {this.state.loading ? (
              <ActivityIndicator
                size={25}
                color="#FCB61A"
                style={{ alignSelf: "center" }}
              />
            ) : (
              <Dropdown
                data={this.state.courses.map(course => ({
                  value: course.courseDescription,
                  courseId: course.id
                }))}
                value={this.state.pickerValue}
                labelTextStyle={{ fontFamily: "Signika-Regular" }}
                onChangeText={(value, index, data) => {
                  this.setState({ pickerValue: value });
                  let courseId = "";
                  data.map((innerValue, innerIndex) => {
                    if (value === innerValue.value && index === innerIndex) {
                      courseId = innerValue.courseId;
                    }
                  });
                  this.getLevels(courseId);
                }}
                containerStyle={{ marginBottom: 10 }}
              />
            )}

            {this.state.quizData ? (
              this.state.quizData.length > 0 ? (
                <>
                  <Text style={styles.selectLevel}>Select Level</Text>

                  <FlatList
                    horizontal
                    contentContainerStyle={{
                      paddingBottom: 10
                    }}
                    showsHorizontalScrollIndicator={false}
                    data={this.state.quizData}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={({ item, index }) =>
                      this.renderLevels(item, index)
                    }
                    contentContainerTyle={{ paddingBottom: 20 }}
                  />
                </>
              ) : (
                <Text
                  style={{
                    marginVertical: 10,
                    alignSelf: "center",
                    fontFamily: "Signika-Regular",
                    fontSize: 16,
                    lineHeight: 21
                  }}
                >
                  No Quiz
                </Text>
              )
            ) : null}
            <View style={{ flexDirection: "row", marginVertical: 10 }}>
              <Button
                containerStyle={{
                  flex: 1,
                  width: "40%",
                  marginHorizontal: 10,
                  borderRadius: 25,
                  shadowColor: "#000",
                  shadowOffset: {
                    width: 0,
                    height: 1
                  },
                  shadowOpacity: 0.2,
                  shadowRadius: 1.41,
                  elevation: 2
                }}
                buttonStyle={{
                  backgroundColor: "#FAFAFA"
                }}
                titleStyle={{
                  color: "black",
                  fontSize: 16,
                  lineHeight: 20,
                  textAlign: "center",
                  fontFamily: "Signika-Regular"
                }}
                onPress={() => this.setState({ showInstructionsCard: true })}
                title="Instructions"
              />
              <Button
                containerStyle={{
                  flex: 1,
                  width: "40%",
                  marginHorizontal: 10,
                  borderRadius: 25,
                  shadowColor: "#000",
                  shadowOffset: {
                    width: 0,
                    height: 1
                  },
                  shadowOpacity: 0.2,
                  shadowRadius: 1.41,
                  elevation: 2
                }}
                buttonStyle={{
                  backgroundColor: "#FAFAFA"
                }}
                disabled={!this.state.currentLevel}
                titleStyle={{
                  color: "black",
                  fontSize: 16,
                  lineHeight: 20,
                  textAlign: "center",
                  fontFamily: "Signika-Regular"
                }}
                title="Score grid"
                onPress={() => {
                  this.setState({ showScoreGrid: true });
                }}
              />
            </View>
            {this.state.totalAttempts >= 5 ? (
              <Text style={{ color: "red" }}>
                Sorry, you have exhausted all limits for this quiz
              </Text>
            ) : null
          }
            <Button
              containerStyle={{
                marginTop: 10,
                paddingTop: 10,
                width: "100%"
              }}
              buttonStyle={{
                width: "100%",
                backgroundColor: "black"
              }}
              titleStyle={{
                fontFamily: "Signika-Regular",
                fontSize: 20,
                color: "#FCB61A"
              }}
              disabled={
                !this.state.currentLevel || this.state.totalAttempts === 5
              }
              title="Let's start the quiz!"
              onPress={() =>
                this.props.navigation.navigate("QuizStart", {
                  info: this.state.currentLevel,
                  uid: this.state.uid
                })
              }
            />
          </View>
        </ScrollView>
        {this.state.showScoreGrid && this.renderScoreGrid()}
        {this.state.showInstructionsCard && this.renderInstructionsCard()}
      </>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  selectText: {
    fontSize: 10,
    lineHeight: 12,
    color: "#555555",
    fontFamily: "Signika-Regular"
  },
  input: {
    marginTop: 10,
    fontFamily: "Signika-Regular",
    fontSize: 10,
    lineHeight: 22,
    color: "#000000",
    backgroundColor: "#FFFFFF",
    borderRadius: 5,

    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 2
  },
  selectLevel: {
    fontSize: 10,
    lineHeight: 12,
    color: "#555555",
    fontFamily: "Signika-Regular",
    marginTop: "5%"
  }
});

export default Dashboard;
