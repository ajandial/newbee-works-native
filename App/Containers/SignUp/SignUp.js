import React from "react";
import {
  StyleSheet,
  View,
  ActivityIndicator,
  Text,
  ScrollView,
  Image,
  TouchableHighlight,
  TouchableWithoutFeedback
} from "react-native";
import logo from "../../Assets/Images/logo.png";
import fbLogo from "../../Assets/Images/fb.png";
import inLogo from "../../Assets/Images/in.png";
import gpLogo from "../../Assets/Images/gp.png";
import { Input, Button } from "react-native-elements";
import { Icon } from "react-native-elements";
import { Auth } from "aws-amplify";
import { TouchableOpacity } from "react-native-gesture-handler";

export default class SignUp extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      password: "",
      email: "",
      errorMessage: "",
      errors: {},
      phone_number: "",
      authenticationCode: "",
      showConfirmationForm: false,
      isLoading: false,
      isError: false
    };
  }
  onChangeText = (key, val) => {
    this.setState({ [key]: val });
  };

  validateEmail = () => {
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (!this.state.username) {
      this.setState({ emailError: "This Field is Required" });
      return true;
    } else if (reg.test(this.state.username) === false) {
      this.setState({ emailError: "Email is Invalid" });
      return true;
    } else {
      this.setState({ emailError: null });
      return false;
    }
  };

  isEmpty = () => {
    const newArr = [
      this.state.name,
      this.state.password,
      this.state.phone_number
    ].map((item, index) => ({ name: index, isEmpty: !item }));
    this.setState({ errors: newArr });
    return this.state.errors.length === 0;
  };
  signUp = async () => {
    if (this.isEmpty() || this.validateEmail()) {
      return;
    }
    const { username, password, email, phone_number, name } = this.state;
    try {
      this.setState({
        isLoading: true
      });
      const success = await Auth.signUp({
        username,
        password,
        attributes: { profile: "Customer" }
      });
      console.log("user successfully signed up!: ", success);
      this.setState({
        showConfirmationForm: true,
        isError: false,
        errorMessage: "",
        isLoading: false
      });
      this.props.navigation.navigate("OTP", {
        username: this.state
      });
    } catch (err) {
      const { username, password, phone_number } = this.state;
      this.setState({
        isError: true,
        username,
        password,
        phone_number,
        isLoading: false,
        errorMessage: err.message
      });
      console.log("error signing up: ", err);
    }
  };

  render() {
    const { isError, errorMessage, isLoading } = this.state;
    return (
      <ScrollView
        style={{
          flex: 1,
          paddingTop: "10%"
        }}
      >
        <View
          style={{
            flex: 1,
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <Image
            style={{
              flex: 1,
              justifyContent: "center",
              alignItems: "center",
              resizeMode: "contain",
              width: 200,
              height: 100
            }}
            source={logo}
          />
        </View>
        <View
          style={{
            paddingTop: "10%",
            flex: 1,
            width: "90%",
            justifyContent: "flex-start",
            alignItems: "flex-start",
            paddingLeft: "10%",
            paddingBottom: "20%"
          }}
        >
          <Text
            style={{
              fontFamily: "Signika-Regular",
              fontWeight: "600",
              fontSize: 22,
              lineHeight: 27,
              color: "#FCB61A"
            }}
          >
            Sign Up
          </Text>
          <View
            style={{
              paddingTop: 5,
              width: "10%",
              alignSelf: "stretch",
              borderBottomColor: "black",
              borderBottomWidth: 2
            }}
          />
          <Text
            style={{
              fontFamily: "Signika-Regular",
              paddingTop: 20,
              color: "#555555",
              fontSize: 10
            }}
          >
            Your first and last name
          </Text>
          <Input
            inputStyle={{
              fontSize: 14,
              lineHeight: 17,
              paddingLeft: 10
            }}
            containerStyle={{
              backgroundColor: "#FFFFFF",
              borderRadius: 5
            }}
            onChangeText={val => this.onChangeText("name", val)}
            placeholder="John Doe"
            leftIcon={
              <Icon name="user" type="font-awesome" size={24} color="black" />
            }
            onEndEditing={() => this.isEmpty()}
          />
          {this.state.errors.length > 0 && this.state.errors[0].isEmpty && (
            <Text style={styles.errorText}>This Field is Required</Text>
          )}
          <Text
            style={{
              fontFamily: "Signika-Regular",
              color: "#555555",
              fontSize: 10
            }}
          >
            Enter your email address
          </Text>
          <Input
            inputStyle={{
              fontSize: 14,
              lineHeight: 17,
              paddingLeft: 10
            }}
            containerStyle={{
              backgroundColor: "#FFFFFF",
              borderRadius: 5
            }}
            onChangeText={val => this.onChangeText("username", val)}
            placeholder="e.g.john.doe@gmail.com"
            leftIcon={
              <Icon
                name="envelope"
                type="font-awesome"
                size={24}
                color="black"
              />
            }
            onEndEditing={() => this.isEmpty()}
          />
          {this.state.errors.length > 0 && this.state.errors[0].isEmpty && (
            <Text style={styles.errorText}>This Field is Required</Text>
          )}
          <Text
            style={{
              fontFamily: "Signika-Regular",
              color: "#555555",
              fontSize: 10
            }}
          >
            Enter your mobile number
          </Text>
          <Input
            inputStyle={{
              fontSize: 14,
              lineHeight: 17,
              paddingLeft: 10
            }}
            containerStyle={{
              backgroundColor: "#FFFFFF",
              borderRadius: 5
            }}
            onChangeText={val => this.onChangeText("phone_number", val)}
            placeholder="12345678"
            leftIcon={
              <Icon name="phone" type="font-awesome" size={24} color="black" />
            }
            onEndEditing={() => this.isEmpty()}
          />
          {this.state.errors.length > 0 && this.state.errors[0].isEmpty && (
            <Text style={styles.errorText}>This Field is Required</Text>
          )}
          <Text
            style={{
              fontFamily: "Signika-Regular",
              color: "#555555",
              fontSize: 10
            }}
          >
            Password
          </Text>
          <Input
            inputStyle={{
              fontSize: 14,
              lineHeight: 17,
              paddingLeft: 10
            }}
            containerStyle={{
              backgroundColor: "#FFFFFF",
              borderRadius: 5
            }}
            onChangeText={val => this.onChangeText("password", val)}
            placeholder="********"
            secureTextEntry={true}
            leftIcon={
              <Icon name="lock" type="font-awesome" size={24} color="black" />
            }
            onEndEditing={() => this.isEmpty()}
          />
          {this.state.errors.length > 0 && this.state.errors[0].isEmpty && (
            <Text style={styles.errorText}>This Field is Required</Text>
          )}

          <TouchableHighlight
            underlayColor="#FFFFFF"
            style={{
              fontSize: 12,

              alignSelf: "flex-end",
              justifyContent: "flex-end",
              color: "#FCB61A",

              paddingBottom: 10
            }}
            onPress={() => this.props.navigation.navigate("ForgotPassword")}
          >
            <Text style={{ fontFamily: "Signika-Regular", color: "#FCB61A" }}>
              Forgot Password ?
            </Text>
          </TouchableHighlight>
          {isError ? (
            <Text style={{ fontFamily: "Signika-Regular", color: "red" }}>
              {errorMessage}
            </Text>
          ) : (
            <Text />
          )}
          <Button
            onPress={this.signUp}
            containerStyle={{
              paddingTop: 10,
              width: "100%"
            }}
            buttonStyle={{
              width: "100%",
              backgroundColor: "black"
            }}
            titleStyle={{
              fontFamily: "Signika-Regular",
              fontSize: 20,
              color: "#FCB61A"
            }}
            isLoading={this.state.isLoading}
            title="Create an Account"
          />

          <TouchableHighlight
            style={{ alignSelf: "center" }}
            underlayColor="#FFFFFF"
            onPress={() => this.props.navigation.navigate("LoginScreen")}
          >
            <Text
              style={{
                fontFamily: "Signika-Regular",
                fontSize: 14,

                paddingTop: 20
              }}
            >
              Already Registered ?{" "}
              <Text style={{ color: "#FCB61A" }}>Log In </Text>
            </Text>
          </TouchableHighlight>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  image: {
    width: "100%",
    height: "100%"
  },
  text: {
    fontSize: 18,
    color: "black",
    textAlign: "center",
    paddingTop: "10%"
  },
  title: {
    fontSize: 25,
    color: "white",
    textAlign: "center"
  },
  errorText: {
    fontSize: 12,

    fontFamily: "Signika-Regular",
    textTransform: "capitalize",
    marginBottom: 15,
    marginLeft: 10,
    color: "red"
  }
});
