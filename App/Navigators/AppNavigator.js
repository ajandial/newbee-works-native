import { createSwitchNavigator } from "react-navigation";
import { createDrawerNavigator, DrawerItems } from "react-navigation-drawer";
import { createAppContainer } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";
import React, { Component } from "react";
import {
  StyleSheet,
  SafeAreaView,
  ScrollView,
  Dimensions,
  View,
  TouchableOpacity,
  Text,
  TouchableHighlight
} from "react-native";
import ExampleScreen from "App/Containers/Example/ExampleScreen";
import LandingScreen from "App/Containers/LandingScreen/LandingScreen";
import LoginScreen from "App/Containers/Login/LoginScreen";
import SplashScreen from "App/Containers/SplashScreen/SplashScreen";
import Subscription from "App/Containers/Subscription/Subscription";
import SubscriptionFailed from "App/Containers/Subscription/SubscriptionFailed";
import SubscriptionSuccess from "App/Containers/Subscription/SubscriptionSuccess";
import Privacy from "App/Containers/Privacy/Privacy";
import { NavigationEvents } from "react-navigation";
import { Auth } from "aws-amplify";
import ForgotPassword from "App/Containers/ForgotPassword/ForgotPassword";
import ResetPassword from "App/Containers/ForgotPassword/ResetPassword";
import Logout from "App/Containers/Logout/Logout";
import SignUp from "App/Containers/SignUp/SignUp";
import OTP from "App/Containers/OTP/OTP";
import Dashboard from "App/Containers/Dashboard/Dashboard";
import MyAccount from "App/Containers/MyAccount/MyAccount";
import Jobs from "App/Containers/Jobs/Jobs";
import Tnc from "App/Containers/Tnc/Tnc";
import ActiveCourses from "App/Containers/Courses/ActiveCourses";
import Quiz from "App/Containers/Quiz/Quiz";
import Offers from "App/Containers/Offers/Offers";
import Notification from "App/Containers/Notification/Notification";
import Certification from "App/Containers/Certification/Certification";
import Contact from "App/Containers/Contact/Contact";
import AllCourses from "App/Containers/Courses/AllCourses";
import CourseDetails from "../Containers/Courses/CourseDetails";
import CourseDetailsInner from "../Containers/Courses/CourseDetailsInner";
import CourseDetailsDescription from "../Containers/Courses/CourseDetailsDescription";
import PaymentMethods from "../Containers/Payment/PaymentMethods";
import ConfirmPayment from "../Containers/Payment/ConfirmPayment";
import PaymentSucessful from "../Containers/Payment/PaymentSucessful";
import PaymentUnSucessful from "../Containers/Payment/PaymentUnsucessfull";
import QuizStart from "../Containers/Quiz/QuizStart";
import QuizResult from "../Containers/Quiz/QuizResult";
import { Icon, Avatar } from "react-native-elements";
import axios from "axios";

/**
 * The root screen contains the application's navigation.
 *
 * @see https://reactnavigation.org/docs/en/hello-react-navigation.html#creating-a-stack-navigator
 */

const { width } = Dimensions.get("window");

class CustomerDrawerComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      userName: {}
    };
  }
  componentDidMount() {
    this.loadData();
  }

  componentWillReceiveProps() {
    this.loadData();
  }

  loadData = () => {
    this.setState({ loading: true });
    try {
      Auth.currentAuthenticatedUser().then(dt => {
        let token = dt
          .getSignInUserSession()
          .getAccessToken()
          .getJwtToken();

        fetch(
          `http://newbee-prod-1105238445.ap-south-1.elb.amazonaws.com/api/user-api/user/search/findByEmail?email=${
            dt.attributes.email
          }`,
          {
            method: "GET",
            headers: {
              Accept: "application/json",
              Authorization: `Bearer ${token}`,
              "Content-Type": "application/json"
            }
          }
        )
          .then(profile => profile.json())
          .then(data1 => {
            this.setState({
              userName: data1._embedded.users[0]
            });
          });
      });
    } catch (error) {
      console.log(error);
    }
  };
  render() {
    return (
      <SafeAreaView style={{ flex: 1, height: 100 }}>
        <View style={{ padding: 10, height: 200, backgroundColor: "#FCB61A" }}>
          <TouchableOpacity onPress={() => this.props.navigation.closeDrawer()}>
            <Icon
              type="font-awesome"
              name="close"
              style={{ alignSelf: "flex-end" }}
            />
          </TouchableOpacity>
          <View
            style={{
              flex: 1,
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <Avatar
              rounded
              source={{
                uri: this.state.userName.profileImage
                  ? this.state.userName.profileImage
                  : "https://icon-library.com/images/no-profile-pic-icon/no-profile-pic-icon-27.jpg"
              }}
              size={80}
              containerStyle={{
                borderWidth: 5,
                borderColor: "#FCB61A",
                shadowColor: "#000",
                shadowOffset: {
                  width: 0,
                  height: 3
                },
                shadowOpacity: 0.27,
                shadowRadius: 4.65,
                marginBottom: 10,
                elevation: 6
              }}
            />
            <Text
              style={{
                marginTop: 10,
                fontSize: 16,
                lineHeight: 25,
                fontWeight: "bold",
                fontFamily: "Signika-Regular"
              }}
            >
              Hello {this.state.userName.firstName}
            </Text>
            <View style={{ flexDirection: "row", alignItems: "center" }}>
              <Icon type="materialicons" name="mail" size={20} />
              <Text
                style={{
                  fontSize: 13,
                  marginLeft: 5,
                  fontFamily: "Signika-Regular"
                }}
              >
                {this.state.userName.email}
              </Text>
            </View>
          </View>
        </View>
        <ScrollView>
          <DrawerItems {...this.props} />
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            <TouchableHighlight
              underlayColor="#FFFFFF"
              style={{ alignSelf: 'center' }}
              onPress={() => this.props.navigation.navigate('Privacy')}
            >
              <Text
                style={{
                  paddingTop: 50,
                  fontFamily: 'Signika-Regular',
                  alignSelf: 'center',
                }}
              >
                Privacy Policy
              </Text>
            </TouchableHighlight>
            <TouchableHighlight
              underlayColor="#FFFFFF"
              style={{ alignSelf: 'center' }}
              onPress={() => this.props.navigation.navigate('Tnc')}
            >
              <Text
                style={{
                  paddingTop: 50,
                  fontFamily: 'Signika-Regular',
                  alignSelf: 'center',
                  marginLeft: 10,
                }}
              >
                | Terms & Conditions
              </Text>
            </TouchableHighlight>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const StackNavigator = createStackNavigator(
  {
    // Create the application routes here (the key is the route name, the value is the target screen)
    // See https://reactnavigation.org/docs/en/stack-navigator.html#routeconfigs
    SplashScreen: SplashScreen,
    LoginScreen: LoginScreen,
    // The main application screen is our "ExampleScreen". Feel free to replace it with your
    // own screen and remove the example.
    MainScreen: ExampleScreen,
    LandingScreen: LandingScreen,
    ForgotPassword: ForgotPassword,
    ResetPassword: ResetPassword,
    SignUp: SignUp,
    OTP: OTP
  },
  {
    // By default the application will show the splash screen
    initialRouteName: "LoginScreen",
    // See https://reactnavigation.org/docs/en/stack-navigator.html#stacknavigatorconfig
    headerMode: "none"
  }
);

const AfterLoginCourseStack = createStackNavigator(
  {
    // Create the application routes here (the key is the route name, the value is the target screen)
    // See https://reactnavigation.org/docs/en/stack-navigator.html#routeconfigs
    AllCourses: AllCourses,
    ActiveCourses: ActiveCourses,
    CourseDetails,
    CourseDetailsInner,
    CourseDetailsDescription,
    Subscription,
    PaymentMethods,
    ConfirmPayment,
    PaymentSucessful,
    PaymentUnSucessful
  },
  {
    // By default the application will show the splash screen
    initialRouteName: "AllCourses",
    // See https://reactnavigation.org/docs/en/stack-navigator.html#stacknavigatorconfig
    headerMode: "none"
  }
);

const AfterLoginActiveCourseStack = createStackNavigator(
  {
    // Create the application routes here (the key is the route name, the value is the target screen)
    // See https://reactnavigation.org/docs/en/stack-navigator.html#routeconfigs
    AllCourses,
    ActiveCourses,
    CourseDetails,
    CourseDetailsInner,
    CourseDetailsDescription,
    Subscription,
    PaymentSucessful,
    PaymentUnSucessful
  },
  {
    // By default the application will show the splash screen
    initialRouteName: "ActiveCourses",
    // See https://reactnavigation.org/docs/en/stack-navigator.html#stacknavigatorconfig
    headerMode: "none"
  }
);

const QuizStack = createStackNavigator(
  {
    Quiz,
    QuizStart,
    QuizResult
  },
  {
    // By default the application will show the splash screen
    initialRouteName: "Quiz",
    // See https://reactnavigation.org/docs/en/stack-navigator.html#stacknavigatorconfig
    headerMode: "none"
  }
);

const AfterLoginStack = createStackNavigator(
  {
    // Create the application routes here (the key is the route name, the value is the target screen)
    // See https://reactnavigation.org/docs/en/stack-navigator.html#routeconfigs
    Dashboard: Dashboard,
    Privacy,
    Tnc,
    SubscriptionFailed,
    SubscriptionSuccess
  },
  {
    // By default the application will show the splash screen
    initialRouteName: "Dashboard",
    // See https://reactnavigation.org/docs/en/stack-navigator.html#stacknavigatorconfig
    headerMode: "none"
  }
);

const appNavigatorStack = createDrawerNavigator(
  {
    "My Account": {
      screen: MyAccount,
      navigationOptions: {
        drawerIcon: ({ tintColor }) => (
          <Icon
            name="user"
            color={tintColor}
            type="font-awesome"
            style={{ fontSize: 24, opacity: 1 }}
          />
        )
      }
    },
    Dashboard: {
      screen: AfterLoginStack,
      navigationOptions: {
        drawerIcon: ({ tintColor }) => (
          <Icon
            name="tachometer"
            color={tintColor}
            type="font-awesome"
            style={{ fontSize: 24, opacity: 1 }}
          />
        )
      }
    },
    Courses: {
      screen: AfterLoginCourseStack,
      navigationOptions: {
        drawerIcon: ({ tintColor }) => (
          <Icon
            name="book"
            color={tintColor}
            type="font-awesome"
            style={{ fontSize: 24, opacity: 1 }}
          />
        )
      }
    },
    "Active Courses": {
      screen: AfterLoginActiveCourseStack,
      navigationOptions: {
        drawerIcon: ({ tintColor }) => (
          <Icon
            name="school"
            color={tintColor}
            style={{ fontSize: 24, opacity: 1 }}
          />
        )
      }
    },
    Jobs: {
      screen: Jobs
    },
    Certification: {
      screen: Certification
    },
    "Take a Quiz": {
      screen: QuizStack,
      navigationOptions: {
        drawerIcon: ({ tintColor }) => (
          <Icon
            type="materialicons"
            name="question-answer"
            color={tintColor}
            style={{ fontSize: 24, opacity: 1 }}
          />
        )
      }
    },
    Offers: {
      screen: Offers
    },
    Notification: {
      screen: Notification
    },
    Contact: {
      screen: Contact
    },
    Logout: {
      screen: Logout
    }
  },
  {
    initialRouteName: "Dashboard",
    drawerPosition: "left",
    drawerWidth: width * 0.9,
    contentComponent: CustomerDrawerComponent,
    headerMode: "float",
    contentOptions: {
      activeTintColor: "#FCB61A",
      inactiveTintColor: "#FAC408",
      activeBackgroundColor: "#171616",
      inactiveLabelStyle: { color: "#000000", width: "100%" },
      activeLabelStyle: { color: "#FCB61A", width: "100%" },
      itemStyle: {
        fontFamily: "Signika-Regular",
        fontSize: 16,
        lineHeight: 20,
        width: "90%",
        borderTopRightRadius: 50,
        borderBottomRightRadius: 50
      }
    }
  }
);

export default createAppContainer(
  createSwitchNavigator(
    {
      App: StackNavigator,
      Customer: appNavigatorStack,
    },
    {
      initialRouteName: "App"
    }
  )
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  drawerHeader: {
    height: 100,
    backgroundColor: "white"
  },
  drawerImage: {
    width: "30%",
    resizeMode: "contain"
  }
});
