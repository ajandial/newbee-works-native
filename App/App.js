import React, { Component } from "react";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/lib/integration/react";
import { SafeAreaView } from "react-native";
import SplashScreen from "App/Containers/SplashScreen/SplashScreen";
import createStore from "App/Stores";
import LandingScreen from "App/Containers/LandingScreen/LandingScreen";
import RootScreen from "./Containers/Root/RootScreen";

const { store, persistor } = createStore();
console.disableYellowBox = true;
export default class App extends Component {
  constructor() {
    super();
    this.state = {
      showScreen:false
    }
  }

  render() {
    return (
      /**
       * @see https://github.com/reduxjs/react-redux/blob/master/docs/api/Provider.md
       */

      <Provider store={store}>
        {/**
         * PersistGate delays the rendering of the app's UI until the persisted state has been retrieved
         * and saved to redux.
         * The `loading` prop can be `null` or any react instance to show during loading (e.g. a splash screen),
         * for example `loading={<SplashScreen />}`.
         * @see https://github.com/rt2zz/redux-persist/blob/master/docs/PersistGate.md
         */}
        <PersistGate loading={<LandingScreen />} persistor={persistor} onBeforeLift={() => new Promise(resolve => setTimeout(resolve, 5000))}>
          <SafeAreaView style={{ flex: 1 }}>
            <RootScreen />
          </SafeAreaView>
        </PersistGate>
      </Provider>
    );
  }
}
