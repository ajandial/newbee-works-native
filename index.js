/**
 * @format
 */

import { AppRegistry } from 'react-native'
import App from './App/App'
import { name as appName } from './app.json'
import Amplify from 'aws-amplify'
import config from './App/aws-exports'
console.ignoredYellowBox = ['Warning: Each', 'Warning: Failed']

//crashlytics().crash()

Amplify.configure({
  Auth: {
    mandatorySignIn: true,
    region: config.cognito.REGION,
    userPoolId: config.cognito.USER_POOL_ID,
    identityPoolId: config.cognito.IDENTITY_POOL_ID,
    userPoolWebClientId: config.cognito.APP_CLIENT_ID,
  },
  // },
  API: {
    endpoints: [
      {
        name: 'NewbeeWorks',
        endpoint: config.apiGateway.URL,
        region: config.apiGateway.REGION,
      },
    ],
  },
})



AppRegistry.registerComponent(appName, () => App)
